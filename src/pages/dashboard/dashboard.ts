import { Component } from '@angular/core';
import { NavController, NavParams,  LoadingController } from 'ionic-angular';
import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
import { Http } from '@angular/http';
import { SigninPage } from '../../pages/signin/signin';

/**
 * Generated class for the DashboardPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
    public foto: any;
    loading: any;
    lists_pegawai_detail: any;
    constructor(
        public navCtrl: NavController,
        public http: Http, 
        public navParams: NavParams,
        private dataService: DataServiceRiwayatProvider, 
        public loadingCtrl: LoadingController ) {
        this.foto = localStorage.getItem('bsw-foto');
        this.getListPegawaiDetail();
    }

    doRefresh(refresh){
       this.getListPegawaiDetail();
       refresh.complete();
    }
    ionViewDidLoad() {
      console.log('ionViewDidLoad DashboardPage');
      console.log('Your Token : '+ localStorage.getItem('bsw-token'));
      if (localStorage.getItem('bsw-token')){
      }else{
        this.navCtrl.setRoot(SigninPage);
        console.log('You need to login');
      }
   
    }

    getListPegawaiDetail(){
        this.dataService.getListPegawaiDetail().subscribe(data => {
          this.lists_pegawai_detail = data.results;
      });
    }

    GoToDashboard(){
    	this.navCtrl.setRoot(DashboardPage);
    }

    ShowLoadingPage() {
      let loading = this.loadingCtrl.create({
      content:  `<ion-spinner name="bubbles"></ion-spinner>`
    });

    loading.present();

    setTimeout(() => {
      loading.dismiss();
    }, 1000);
  }
}
