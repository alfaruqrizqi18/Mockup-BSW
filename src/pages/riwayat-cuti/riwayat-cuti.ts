import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { DetailRiwayatCutiPage } from '../../pages/detail-riwayat-cuti/detail-riwayat-cuti';

import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the RiwayatCutiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-riwayat-cuti',
  templateUrl: 'riwayat-cuti.html',
})
export class RiwayatCutiPage {
public foto: any;
loading: any;
lists_riwayat_cuti: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private dataService: DataServiceRiwayatProvider, 
    public loadingCtrl: LoadingController) {
    this.foto = localStorage.getItem('bsw-foto');
    this.getDataRiwayatCuti();
  }
  doRefresh(refresh){
    this.getDataRiwayatCuti();
     refresh.complete();
  }
  getDataRiwayatCuti(){
      this.showLoader();
      this.dataService.getListRiwayatCuti().subscribe(data => {
        this.loading.dismiss();
        //this.lists_riwayat_jabatan = data;
        this.lists_riwayat_cuti = data.results;
    });
  }
  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Mengambil data dari server...'
    });

    this.loading.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RiwayatCutiPage');
  }
  GoToDashboard(){
  	this.navCtrl.setRoot(DashboardPage);
  }
  GoToDetailRiwayatCuti(id){
    this.navCtrl.push(DetailRiwayatCutiPage, {id:id});
  }
}
