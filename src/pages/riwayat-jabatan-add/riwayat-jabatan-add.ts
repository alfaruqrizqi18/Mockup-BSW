import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RiwayatJabatanAddPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-riwayat-jabatan-add',
  templateUrl: 'riwayat-jabatan-add.html',
})
export class RiwayatJabatanAddPage {
jabatan_data = {pegawai: '', nama_pegawai: '', unit_kerja: '', value_unit_kerja:'', 
   jabatan: '', jenis_jabatan: '', eselon_jabatan: '', plt: '', angka_kredit_utama: '', tmt: '', keterangan: '', surat: '', nama_lampiran: '',lampiran: '', nama_pejabat_penandatangan: '',
   jabatan_pejabat_penandatangan:'', tanggal_surat:''};
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RiwayatJabatanAddPage');
  }

}
