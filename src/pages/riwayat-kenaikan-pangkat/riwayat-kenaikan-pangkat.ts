import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { DetailRiwayatKenaikanPangkatPage } from '../../pages/detail-riwayat-kenaikan-pangkat/detail-riwayat-kenaikan-pangkat';


import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the RiwayatKenaikanPangkatPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-riwayat-kenaikan-pangkat',
  templateUrl: 'riwayat-kenaikan-pangkat.html',
})
export class RiwayatKenaikanPangkatPage {
public foto: any;
form_operator = {operator_cari_pegawai: ''};
form_verifikator = {verifikator_cari_pegawai: ''};
loading: any;
lists_riwayat_kenaikan_pangkat: any;
groups : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public dataService: DataServiceRiwayatProvider,
    public loadingCtrl: LoadingController) {
    this.foto = localStorage.getItem('bsw-foto');
    this.groups = localStorage.getItem('bsw-groups');
    this.getDataRiwayatKenaikanPangkat();
  }
  doRefresh(refresh){
    this.getDataRiwayatKenaikanPangkat();
     refresh.complete();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RiwayatKenaikanPangkatPage');
  }
  getDataRiwayatKenaikanPangkat(){
      this.showLoader();
      this.dataService.getListRiwayatKenaikanPangkat().subscribe(data => {
        this.loading.dismiss();
        //this.lists_riwayat_kenaikan_pangkat = data;
        this.lists_riwayat_kenaikan_pangkat = data.results;
    });
  }
    operator_getDataRiwayatUnitKerja(){
      let param;
      if (this.form_operator.operator_cari_pegawai == "" ||  this.form_operator.operator_cari_pegawai == null) {
        this.getDataRiwayatKenaikanPangkat();
      }else {
        param = this.form_operator.operator_cari_pegawai;
          this.showLoader();
          this.dataService.non_pegawai_getDetailRiwayatKenaikanPangkat(param).subscribe(data => {
          this.loading.dismiss();
            //this.lists_riwayat_unit_kerja = data;
            this.lists_riwayat_kenaikan_pangkat = data.results;
            console.log(data.results)
        });
      }
  }

  verifikator_getDataRiwayatUnitKerja(){
      let param;
      if (this.form_verifikator.verifikator_cari_pegawai == "" ||  this.form_verifikator.verifikator_cari_pegawai == null) {
        this.getDataRiwayatKenaikanPangkat();
      }else {
        param = this.form_verifikator.verifikator_cari_pegawai;
          this.showLoader();
          this.dataService.non_pegawai_getDetailRiwayatKenaikanPangkat(param).subscribe(data => {
          this.loading.dismiss();
            //this.lists_riwayat_unit_kerja = data;
            this.lists_riwayat_kenaikan_pangkat = data.results;
            console.log(data.results)
        });
      }
  }
  GoToDashboard(){
  	this.navCtrl.setRoot(DashboardPage);
  }
  GoToDetailRiwayatKenaikanPangkat(id){
    this.navCtrl.push(DetailRiwayatKenaikanPangkatPage, {id: id});
  }
  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Mengambil data dari server...'
    });

    this.loading.present();
  }
}
