import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { DetailRiwayatJabatanPegawaiPage } from '../../pages/detail-riwayat-jabatan-pegawai/detail-riwayat-jabatan-pegawai';

import { RiwayatJabatanAddPage } from '../../pages/riwayat-jabatan-add/riwayat-jabatan-add';

import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the RiwayatJabatanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-riwayat-jabatan',
  templateUrl: 'riwayat-jabatan.html',
})
export class RiwayatJabatanPage {
public foto: any;
loading: any;
form_operator = {operator_cari_pegawai: ''};
form_verifikator = {verifikator_cari_pegawai: ''};
lists_riwayat_jabatan: any;
groups : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private dataService: DataServiceRiwayatProvider, 
    public loadingCtrl: LoadingController ) {
    this.foto = localStorage.getItem('bsw-foto');
    this.getDataRiwayatJabatan();
    this.groups = localStorage.getItem('bsw-groups');
  }
  doRefresh(refresh){
    this.getDataRiwayatJabatan();
     refresh.complete();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RiwayatJabatanPage');
  }
  
  getDataRiwayatJabatan(){
      this.showLoader();
      this.dataService.getListRiwayatJabatan().subscribe(data => {
        this.loading.dismiss();
        //this.lists_riwayat_jabatan = data;
        this.lists_riwayat_jabatan = data.results;
    });
  }

    operator_getDataRiwayatUnitKerja(){
      let param;
      if (this.form_operator.operator_cari_pegawai == "" ||  this.form_operator.operator_cari_pegawai == null) {
      this.getDataRiwayatJabatan();
      }else {
        param = this.form_operator.operator_cari_pegawai;
          this.showLoader();
          this.dataService.non_pegawai_getDetailRiwayatJabatan(param).subscribe(data => {
          this.loading.dismiss();
            //this.lists_riwayat_unit_kerja = data;
            this.lists_riwayat_jabatan = data.results;
            console.log(data.results)
        });
      }
  }

  verifikator_getDataRiwayatUnitKerja(){
      let param;
      if (this.form_verifikator.verifikator_cari_pegawai == "" ||  this.form_verifikator.verifikator_cari_pegawai == null) {
      this.getDataRiwayatJabatan();
      }else {
        param = this.form_verifikator.verifikator_cari_pegawai;
          this.showLoader();
          this.dataService.non_pegawai_getDetailRiwayatJabatan(param).subscribe(data => {
          this.loading.dismiss();
            //this.lists_riwayat_unit_kerja = data;
            this.lists_riwayat_jabatan = data.results;
            console.log(data.results)
        });
      }
  }

  GoToDashboard(){
  	this.navCtrl.setRoot(DashboardPage);
  }

  GoToDetailRiwayatJabatanPegawai(id){
    this.navCtrl.push(DetailRiwayatJabatanPegawaiPage, {id: id});
    console.log("ID Detail Riwayat : "+id);
  }

  GoToAddRiwayatJabatan(){
    this.navCtrl.push(RiwayatJabatanAddPage);    
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Mengambil data dari server...'
    });

    this.loading.present();
  }

}
