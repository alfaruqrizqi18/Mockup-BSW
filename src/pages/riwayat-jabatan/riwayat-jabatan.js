var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { DetailRiwayatJabatanPegawaiPage } from '../../pages/detail-riwayat-jabatan-pegawai/detail-riwayat-jabatan-pegawai';
import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the RiwayatJabatanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RiwayatJabatanPage = (function () {
    function RiwayatJabatanPage(navCtrl, navParams, dataService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.loadingCtrl = loadingCtrl;
        this.foto = localStorage.getItem('bsw-foto');
        this.getDataRiwayatJabatan();
    }
    RiwayatJabatanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatJabatanPage');
    };
    RiwayatJabatanPage.prototype.getDataRiwayatJabatan = function () {
        var _this = this;
        this.showLoader();
        this.dataService.getListRiwayatJabatan().subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_riwayat_jabatan = data;
            _this.lists_riwayat_jabatan = data.results;
        });
    };
    RiwayatJabatanPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(DashboardPage);
    };
    RiwayatJabatanPage.prototype.GoToDetailRiwayatJabatanPegawai = function (id) {
        this.navCtrl.push(DetailRiwayatJabatanPegawaiPage, { id: id });
        console.log("ID Detail Riwayat : " + id);
    };
    RiwayatJabatanPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Mengambil data dari server...'
        });
        this.loading.present();
    };
    return RiwayatJabatanPage;
}());
RiwayatJabatanPage = __decorate([
    Component({
        selector: 'page-riwayat-jabatan',
        templateUrl: 'riwayat-jabatan.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, DataServiceRiwayatProvider,
        LoadingController])
], RiwayatJabatanPage);
export { RiwayatJabatanPage };
//# sourceMappingURL=riwayat-jabatan.js.map