import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DashboardPage} from '../../pages/dashboard/dashboard'

import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the DetailRiwayatHukumanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-detail-riwayat-hukuman',
  templateUrl: 'detail-riwayat-hukuman.html',
})
export class DetailRiwayatHukumanPage {
public foto: any;
loading: any;
lists_detail_riwayat_hukuman: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private dataService: DataServiceRiwayatProvider, public loadingCtrl: LoadingController) {
    this.foto = localStorage.getItem('bsw-foto');
    this.getDetailRiwayatHukuman(navParams.get('id'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailRiwayatHukumanPage');
  }
  getDetailRiwayatHukuman(id){
    this.showLoader("Mengambil data dari server...")
     this.dataService.getDetailRiwayatHukuman(id).subscribe(data => {
      this.loading.dismiss();
        //this.lists_detail_riwayat_jabatan = data;
        this.lists_detail_riwayat_hukuman = data.results;
    });
  }
  showLoader(msg){
    this.loading = this.loadingCtrl.create({
        content: msg
    });
    this.loading.present();
  }
  GoToDashboard(){
  	this.navCtrl.setRoot(DashboardPage);
  }
}
