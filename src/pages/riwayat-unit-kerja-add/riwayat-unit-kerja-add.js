var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { RiwayatUnitKerjaModalCariPegawaiPage } from '../../pages/riwayat-unit-kerja-modal-cari-pegawai/riwayat-unit-kerja-modal-cari-pegawai';
import { RiwayatUnitKerjaModalCariUnitKerjaPage } from '../../pages/riwayat-unit-kerja-modal-cari-unit-kerja/riwayat-unit-kerja-modal-cari-unit-kerja';
/**
 * Generated class for the RiwayatUnitKerjaAddPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//let base_url = "https://bsw.kedirikab.go.id/api/";
var base_url = "http://192.168.200.88:8899/api-mobile/";
var RiwayatUnitKerjaAddPage = (function () {
    function RiwayatUnitKerjaAddPage(navCtrl, navParams, modalCtrl, viewCtrl, http, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.unit_kerja_data = { pegawai: '', unit_kerja: '', value_unit_kerja: '', bidang_struktural: '', tamat: '', wilayah_kerja: '', keterangan: '' };
    }
    RiwayatUnitKerjaAddPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatUnitKerjaAddPage');
    };
    RiwayatUnitKerjaAddPage.prototype.openModalCariPegawai = function () {
        var _this = this;
        var modal_cari_pegawai = this.modalCtrl.create(RiwayatUnitKerjaModalCariPegawaiPage);
        modal_cari_pegawai.onDidDismiss(function (data) {
            _this.unit_kerja_data.pegawai = data;
        });
        modal_cari_pegawai.present();
    };
    RiwayatUnitKerjaAddPage.prototype.openModalCariUnitKerja = function () {
        var _this = this;
        var modal_cari_unit_kerja = this.modalCtrl.create(RiwayatUnitKerjaModalCariUnitKerjaPage);
        modal_cari_unit_kerja.onDidDismiss(function (data) {
            _this.unit_kerja_data.unit_kerja = data.nama_unit_kerja;
            _this.unit_kerja_data.value_unit_kerja = data.id;
            console.log(data);
            _this.getListDataBidangStruktural();
        });
        modal_cari_unit_kerja.present();
    };
    RiwayatUnitKerjaAddPage.prototype.getListDataBidangStruktural = function () {
        var _this = this;
        this.showLoader();
        this.http.get(base_url + 'bidang-struktural/?q=' + this.unit_kerja_data.value_unit_kerja).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.loading.dismiss();
            //this.items = data;
            _this.items = data.results;
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    RiwayatUnitKerjaAddPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Fetching Data...'
        });
        this.loading.present();
    };
    RiwayatUnitKerjaAddPage.prototype.doSubmitUnitKerja = function () {
        if (this.unit_kerja_data.pegawai == "" || this.unit_kerja_data.unit_kerja == "" || this.unit_kerja_data.bidang_struktural == "" ||
            this.unit_kerja_data.tamat == "" || this.unit_kerja_data.wilayah_kerja == "" || this.unit_kerja_data.keterangan == "") {
            console.log("Silahkan isi semua form-fill");
        }
        else {
            console.log("Submit Data: " + this.unit_kerja_data.pegawai);
        }
    };
    return RiwayatUnitKerjaAddPage;
}());
RiwayatUnitKerjaAddPage = __decorate([
    Component({
        selector: 'page-riwayat-unit-kerja-add',
        templateUrl: 'riwayat-unit-kerja-add.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, ModalController, ViewController, Http,
        LoadingController])
], RiwayatUnitKerjaAddPage);
export { RiwayatUnitKerjaAddPage };
//# sourceMappingURL=riwayat-unit-kerja-add.js.map