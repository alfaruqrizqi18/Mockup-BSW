import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController, LoadingController, ToastController  } from 'ionic-angular';
import { Http } from '@angular/http';
import { RiwayatUnitKerjaPage } from '../../pages/riwayat-unit-kerja/riwayat-unit-kerja';
import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
import { RiwayatUnitKerjaModalCariPegawaiPage } from '../../pages/riwayat-unit-kerja-modal-cari-pegawai/riwayat-unit-kerja-modal-cari-pegawai';
import { RiwayatUnitKerjaModalCariUnitKerjaPage } from '../../pages/riwayat-unit-kerja-modal-cari-unit-kerja/riwayat-unit-kerja-modal-cari-unit-kerja';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { Base64 } from '@ionic-native/base64';

/**
 * Generated class for the RiwayatUnitKerjaAddPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
let base_url = "http://192.168.200.88:8899/api-mobile/";
 @Component({
   selector: 'page-riwayat-unit-kerja-add',
   templateUrl: 'riwayat-unit-kerja-add.html',
 })
 export class RiwayatUnitKerjaAddPage {
   loading: any;
   items: string[];
   data_result_post: any;
   unit_kerja_data = {pegawai: '', nama_pegawai: '', unit_kerja: '', value_unit_kerja:'', 
   bidang_struktural: '', tmt: '', wilayah_kerja: '', keterangan: '', surat: '', nama_lampiran: '',lampiran: '', nama_pejabat_penandatangan: '',
   jabatan_pejabat_penandatangan:'', tanggal_surat:''};
   imageFilePath: any;
   files;
   constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public viewCtrl: ViewController, public http: Http,
    public loadingCtrl: LoadingController, public dataService: DataServiceRiwayatProvider, private toastCtrl: ToastController, private fileChooser: FileChooser,
    private filePath: FilePath, private base64: Base64) {
   }


   ionViewDidLoad() {
     console.log('ionViewDidLoad RiwayatUnitKerjaAddPage');
   }

  pilihfile(){
    this.fileChooser.open().then(uri => 
      this.filePath.resolveNativePath(uri).then(filePath => this.encryptedFile(filePath)))
    .catch(e => this.presentToast(e));
  }
  resetImage(){
     this.imageFilePath = null;
     this.encryptedFile("");
  }
  encryptedFile(filePath){
    this.base64.encodeFile(filePath).then((base64File: string) => {
       this.imageFilePath = filePath;
      this.unit_kerja_data.nama_lampiran = base64File
    }, (err) => {
      console.log(err);
    });
  }

   openModalCariPegawai(){
     let modal_cari_pegawai = this.modalCtrl.create(RiwayatUnitKerjaModalCariPegawaiPage);
     modal_cari_pegawai.onDidDismiss(data => {
       this.unit_kerja_data.pegawai = data.id;
       this.unit_kerja_data.nama_pegawai = data.username;
     });
     modal_cari_pegawai.present();
   }

   openModalCariUnitKerja(){
     let modal_cari_unit_kerja = this.modalCtrl.create(RiwayatUnitKerjaModalCariUnitKerjaPage);
     modal_cari_unit_kerja.onDidDismiss(data => {
       this.unit_kerja_data.unit_kerja = data.nama_unit_kerja;
       this.unit_kerja_data.value_unit_kerja = data.id;
       console.log(data);
       this.getListDataBidangStruktural();
     });
     modal_cari_unit_kerja.present();
   }

   getListDataBidangStruktural(){
      this.showLoader();
      this.http.get(base_url+'bidang-struktural/?q='+this.unit_kerja_data.value_unit_kerja).map(res => res.json()).subscribe(data => {
         this.loading.dismiss();
          //this.items = data;
          this.items = data.results;
      }, (err) => {
        this.loading.dismiss();
      });
   }

    showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Fetching Data...'
    });
    this.loading.present();
  }

   doSubmitUnitKerja(){
     if (this.unit_kerja_data.pegawai == null || this.unit_kerja_data.pegawai == "" || this.unit_kerja_data.value_unit_kerja == null || this.unit_kerja_data.value_unit_kerja == "" ||
      this.unit_kerja_data.tmt == null || this.unit_kerja_data.tmt == "" || this.unit_kerja_data.wilayah_kerja == null || this.unit_kerja_data.wilayah_kerja == "" ||
      this.unit_kerja_data.keterangan == null || this.unit_kerja_data.keterangan == "") {
       console.log("Silahkan isi semua form-fill");
     }else{
       let in_surat = {
         no_surat: this.unit_kerja_data.surat,
         tanggal: this.unit_kerja_data.tanggal_surat,
         nama_pejabat_penandatangan : this.unit_kerja_data.nama_pejabat_penandatangan,
         jabatan_pejabat_penandatangan: this.unit_kerja_data.jabatan_pejabat_penandatangan,
         lampiran: this.unit_kerja_data.nama_lampiran
       };
       let data = {
         pegawai : this.unit_kerja_data.pegawai,
         unit_kerja : this.unit_kerja_data.value_unit_kerja,
         bidang_struktural : this.unit_kerja_data.bidang_struktural,
         tmt : this.unit_kerja_data.tmt,
         wilayah_kerja : this.unit_kerja_data.wilayah_kerja,
         keterangan : this.unit_kerja_data.keterangan,
         surat: in_surat
       };
       this.postData(data);
       console.log(data);
     }
   }

   postData(data){
     this.dataService.postRiwayatUnitKerjaBaru(data).then((result) => {
        this.loading.dismiss();
        this.data_result_post = result; {
          let toast = this.toastCtrl.create({
            message: "Berhasil menambahkan data...",
            duration: 3000,
            position: 'top',
            dismissOnPageChange: true
          });
          toast.onDidDismiss(() => {
            console.log('Berhasil');
          });
          toast.present();
          this.navCtrl.setRoot(RiwayatUnitKerjaPage);          
        }
      }, (err) => {
        this.loading.dismiss();
        this.presentToast(err);
      });
   }

   presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

 }
