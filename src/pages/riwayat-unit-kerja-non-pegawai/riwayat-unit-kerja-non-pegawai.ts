import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';

/**
 * Generated class for the RiwayatUnitKerjaNonPegawaiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-riwayat-unit-kerja-non-pegawai',
  templateUrl: 'riwayat-unit-kerja-non-pegawai.html',
})
export class RiwayatUnitKerjaNonPegawaiPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RiwayatUnitKerjaNonPegawaiPage');
  }

  GoToDashboard(){
  	this.navCtrl.setRoot(DashboardPage);
  }

}
