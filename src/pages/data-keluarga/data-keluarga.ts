import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController  } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';

import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the DataKeluargaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-data-keluarga',
  templateUrl: 'data-keluarga.html',
})
export class DataKeluargaPage {
public foto: any;
loading: any;
lists_data_keluarga: any;
lists_data_anak: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private dataService: DataServiceRiwayatProvider, 
    public loadingCtrl: LoadingController) {
    this.foto = localStorage.getItem('bsw-foto');
    this.getDataKeluarga();
  }
  doRefresh(refresh){
    this.getDataKeluarga();
     refresh.complete();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DataKeluargaPage');
  }
  getDataKeluarga(){
      this.showLoader();
      this.dataService.getListDataPerkawinan().subscribe(data => {
        this.loading.dismiss();
        //this.lists_riwayat_jabatan = data;
        this.lists_data_keluarga = data.results;
        this.lists_data_anak = data.results[0].data_perkawinan;
    });
  }
  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Mengambil data dari server...'
    });

    this.loading.present();
  }
  GoToDashboard(){
  	this.navCtrl.setRoot(DashboardPage);
  }
}
