import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { DetailRiwayatHukumanPage } from '../../pages/detail-riwayat-hukuman/detail-riwayat-hukuman';

import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the RiwayatHukumanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-riwayat-hukuman',
  templateUrl: 'riwayat-hukuman.html',
})
export class RiwayatHukumanPage {
public foto: any;
loading: any;
lists_riwayat_hukuman: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private dataService: DataServiceRiwayatProvider, 
    public loadingCtrl: LoadingController) {
    this.foto = localStorage.getItem('bsw-foto');
    this.getDataRiwayatHukuman();
  }
  doRefresh(refresh){
    this.getDataRiwayatHukuman();
     refresh.complete();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RiwayatHukumanPage');
  }
  getDataRiwayatHukuman(){
      this.showLoader();
      this.dataService.getListRiwayatHukuman().subscribe(data => {
        this.loading.dismiss();
        //this.lists_riwayat_jabatan = data;
        this.lists_riwayat_hukuman = data.results;
    });
  }
  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Mengambil data dari server...'
    });

    this.loading.present();
  }
  GoToDashboard(){
  	this.navCtrl.setRoot(DashboardPage);
  }
  GoToDetailRiwayatHukuman(id){
    this.navCtrl.push(DetailRiwayatHukumanPage, {id: id});
  }
}
