var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
/**
 * Generated class for the RiwayatUnitKerjaModalCariPegawaiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//let base_url = "https://bsw.kedirikab.go.id/api/";
var base_url = "http://192.168.200.88:8899/api-mobile/";
var RiwayatUnitKerjaModalCariPegawaiPage = (function () {
    function RiwayatUnitKerjaModalCariPegawaiPage(navCtrl, navParams, viewCtrl, http, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
    }
    RiwayatUnitKerjaModalCariPegawaiPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatUnitKerjaModalCariPegawaiPage');
    };
    RiwayatUnitKerjaModalCariPegawaiPage.prototype.doSubmitUnitKerja = function () {
    };
    RiwayatUnitKerjaModalCariPegawaiPage.prototype.checkDigit = function () {
        if (this.cari_pegawai.length <= 3) {
            this.buttonClicked = "false";
        }
        else if (this.cari_pegawai.length > 3) {
            this.buttonClicked = "true";
        }
    };
    RiwayatUnitKerjaModalCariPegawaiPage.prototype.closeModal = function (nip) {
        var data = { cari_pegawai: nip };
        this.viewCtrl.dismiss(data.cari_pegawai);
        console.log('Text Modal :' + data.cari_pegawai);
    };
    RiwayatUnitKerjaModalCariPegawaiPage.prototype.getListDataPegawaiAktif = function () {
        var _this = this;
        this.showLoader();
        this.http.get(base_url + 'pegawai-aktif/?q=' + this.cari_pegawai).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.loading.dismiss();
            //this.items = data;
            _this.items = data.results;
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    RiwayatUnitKerjaModalCariPegawaiPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Fetching Data...'
        });
        this.loading.present();
    };
    return RiwayatUnitKerjaModalCariPegawaiPage;
}());
RiwayatUnitKerjaModalCariPegawaiPage = __decorate([
    Component({
        selector: 'page-riwayat-unit-kerja-modal-cari-pegawai',
        templateUrl: 'riwayat-unit-kerja-modal-cari-pegawai.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, ViewController, Http,
        LoadingController])
], RiwayatUnitKerjaModalCariPegawaiPage);
export { RiwayatUnitKerjaModalCariPegawaiPage };
//# sourceMappingURL=riwayat-unit-kerja-modal-cari-pegawai.js.map