import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';

import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the RiwayatUnitKerjaModalCariPegawaiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


//let base_url = "http://192.168.200.88:8899/api-mobile/";
@Component({
  selector: 'page-riwayat-unit-kerja-modal-cari-pegawai',
  templateUrl: 'riwayat-unit-kerja-modal-cari-pegawai.html',
})
export class RiwayatUnitKerjaModalCariPegawaiPage {
public base_url;
loading: any;
cari_pegawai: string;
items: string[];
public buttonClicked: any; 
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: Http,
    public loadingCtrl: LoadingController, public dataService: DataServiceRiwayatProvider) {
    this.base_url = this.dataService.base_url.toString();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RiwayatUnitKerjaModalCariPegawaiPage');

  }
  
  doSubmitUnitKerja(){

  }

  checkDigit(){
    if (this.cari_pegawai.length <= 3) {
        this.buttonClicked = "false";
    }else if (this.cari_pegawai.length > 3){
        this.buttonClicked = "true";
    }
  }

  closeModal(id, username) {
    let data = {id: id, username: username};
    this.viewCtrl.dismiss(data);
    console.log('Text Modal :'+data);
  }

  getListDataPegawaiAktif(){
    this.showLoader();
    this.http.get(this.base_url+'pegawai-aktif/?q='+this.cari_pegawai).map(res => res.json()).subscribe(data => {
       this.loading.dismiss();
        //this.items = data;
        this.items = data.results;
    }, (err) => {
      this.loading.dismiss();
    });
  }
  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Fetching Data...'
    });

    this.loading.present();
  }
  
}
