var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { DetailRiwayatUnitKerjaPage } from '../../pages/detail-riwayat-unit-kerja/detail-riwayat-unit-kerja';
import { RiwayatUnitKerjaAddPage } from '../../pages/riwayat-unit-kerja-add/riwayat-unit-kerja-add';
import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the RiwayatUnitKerjaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RiwayatUnitKerjaPage = (function () {
    function RiwayatUnitKerjaPage(navCtrl, navParams, dataService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.loadingCtrl = loadingCtrl;
        this.foto = localStorage.getItem('bsw-foto');
        this.getDataRiwayatUnitKerja();
    }
    RiwayatUnitKerjaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatUnitKerjaPage');
    };
    RiwayatUnitKerjaPage.prototype.getDataRiwayatUnitKerja = function () {
        var _this = this;
        this.showLoader();
        this.dataService.getListRiwayatUnitKerja().subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_riwayat_unit_kerja = data;
            _this.lists_riwayat_unit_kerja = data.results;
        });
    };
    RiwayatUnitKerjaPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(DashboardPage);
    };
    RiwayatUnitKerjaPage.prototype.GoToDetailRiwayatUnitKerja = function (id) {
        this.navCtrl.push(DetailRiwayatUnitKerjaPage, { id: id });
    };
    RiwayatUnitKerjaPage.prototype.GoToAddRiwayatUnitKerja = function () {
        this.navCtrl.push(RiwayatUnitKerjaAddPage);
    };
    RiwayatUnitKerjaPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Mengambil data dari server...'
        });
        this.loading.present();
    };
    return RiwayatUnitKerjaPage;
}());
RiwayatUnitKerjaPage = __decorate([
    Component({
        selector: 'page-riwayat-unit-kerja',
        templateUrl: 'riwayat-unit-kerja.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, DataServiceRiwayatProvider,
        LoadingController])
], RiwayatUnitKerjaPage);
export { RiwayatUnitKerjaPage };
//# sourceMappingURL=riwayat-unit-kerja.js.map