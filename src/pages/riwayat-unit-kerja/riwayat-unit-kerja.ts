import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { DetailRiwayatUnitKerjaPage } from '../../pages/detail-riwayat-unit-kerja/detail-riwayat-unit-kerja';
import { RiwayatUnitKerjaAddPage } from '../../pages/riwayat-unit-kerja-add/riwayat-unit-kerja-add';

import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the RiwayatUnitKerjaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-riwayat-unit-kerja',
  templateUrl: 'riwayat-unit-kerja.html',
})
export class RiwayatUnitKerjaPage {
public foto: any;
loading: any;
form_operator = {operator_cari_pegawai: ''};
form_verifikator = {verifikator_cari_pegawai: ''};
lists_riwayat_unit_kerja: any;
groups : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private dataService: DataServiceRiwayatProvider,
    public loadingCtrl: LoadingController) {
    this.foto = localStorage.getItem('bsw-foto');
    this.getDataRiwayatUnitKerja();
    this.groups = localStorage.getItem('bsw-groups');
    
  }
  doRefresh(refresh){
    this.getDataRiwayatUnitKerja();
     refresh.complete();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RiwayatUnitKerjaPage');
  }
  
  getDataRiwayatUnitKerja(){
      this.showLoader();
      this.dataService.getListRiwayatUnitKerja().subscribe(data => {
      this.loading.dismiss();
        //this.lists_riwayat_unit_kerja = data;
        this.lists_riwayat_unit_kerja = data.results;
    });
  }

  operator_getDataRiwayatUnitKerja(){
      let param;
      if (this.form_operator.operator_cari_pegawai == "" ||  this.form_operator.operator_cari_pegawai == null) {
        this.getDataRiwayatUnitKerja();
      }else {
        param = this.form_operator.operator_cari_pegawai;
          this.showLoader();
          this.dataService.non_pegawai_getDataRiwayatUnitKerja(param).subscribe(data => {
          this.loading.dismiss();
            //this.lists_riwayat_unit_kerja = data;
            this.lists_riwayat_unit_kerja = data.results;
            console.log(data.results)
        });
      }
  }

  verifikator_getDataRiwayatUnitKerja(){
      let param;
      if (this.form_verifikator.verifikator_cari_pegawai == "" ||  this.form_verifikator.verifikator_cari_pegawai == null) {
        this.getDataRiwayatUnitKerja();
      }else {
        param = this.form_verifikator.verifikator_cari_pegawai;
          this.showLoader();
          this.dataService.non_pegawai_getDataRiwayatUnitKerja(param).subscribe(data => {
          this.loading.dismiss();
            //this.lists_riwayat_unit_kerja = data;
            this.lists_riwayat_unit_kerja = data.results;
            console.log(data.results)
        });
      }
  }

  GoToDashboard(){
  	this.navCtrl.setRoot(DashboardPage);
  }

  GoToDetailRiwayatUnitKerja(id){
    this.navCtrl.push(DetailRiwayatUnitKerjaPage, {id: id});
  }

  GoToAddRiwayatUnitKerja(){
    this.navCtrl.push(RiwayatUnitKerjaAddPage);
  }
  
  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Mengambil data dari server...'
    });

    this.loading.present();
  }
}
