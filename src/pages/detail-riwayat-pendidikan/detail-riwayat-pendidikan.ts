import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';

import { RiwayatPendidikanPage } from '../../pages/riwayat-pendidikan/riwayat-pendidikan';
import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the DetailRiwayatPendidikanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-detail-riwayat-pendidikan',
  templateUrl: 'detail-riwayat-pendidikan.html',
})
export class DetailRiwayatPendidikanPage {
public foto: any;
lists_detail_riwayat_pendidikan: any;
error = {data: ''};
loading: any;
data_result_post: any;
groups : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController, public loadingCtrl: LoadingController, 
   private dataService: DataServiceRiwayatProvider) {
    this.foto = localStorage.getItem('bsw-foto');
    this.getDetailRiwayatPendidikan(navParams.get('id'));
    this.groups = localStorage.getItem('bsw-groups');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailRiwayatPendidikanPage');
  }
   getDetailRiwayatPendidikan(id){
    this.showLoader("Mengambil data dari server...");
     this.dataService.getDetailRiwayatPendidikan(id).subscribe(data => {
       this.loading.dismiss();
        //this.lists_detail_riwayat_jabatan = data;
        this.lists_detail_riwayat_pendidikan = data.results;
    });
  }
  showLoader(msg){
    this.loading = this.loadingCtrl.create({
        content: msg
    });
    this.loading.present();
  }
  doSubmitVerifikasi(id){
      let data = {
        status : 1
      }
       this.postData(id,data);
       console.log(data);
  }
  
  doSubmitToVerifikator(id){
      let data = {
        status : 2
      }
       this.postData(id,data);
       console.log(data);
  }

  postData(id,data){
    this.showLoader("Mohon tunggu sebentar...");
    this.dataService.postStatusDetailPendidikan(id,data).then((result) => {
        this.loading.dismiss();
        this.data_result_post = result; {
          let toast = this.toastCtrl.create({
            message: "Berhasil mengirim data...",
            duration: 3000,
            position: 'top',
            dismissOnPageChange: true
          });
          toast.onDidDismiss(() => {
            console.log('Berhasil');
          });
          toast.present();
          this.navCtrl.setRoot(RiwayatPendidikanPage);
        }
      }, (err) => {
        this.loading.dismiss();
        this.presentToast(err);
      });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  GoToDashboard(){
  	this.navCtrl.setRoot(DashboardPage);
  }

}
