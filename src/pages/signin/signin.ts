import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, LoadingController, ToastController, Platform  } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';

import { App } from 'ionic-angular';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the SigninPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {
  loading: any;
  loginData = { username:'', password:'' };
  data: any;
  public site_url;
  public img_base64: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public menu: MenuController, public authService: AuthServiceProvider, public loadingCtrl: LoadingController, 
    private toastCtrl: ToastController, public platform : Platform, public app: App, public dataService: DataServiceRiwayatProvider ) {
    this.menu.swipeEnable(false, 'myMenu'); 
    this.site_url = this.dataService.site_url.toString();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
    if (localStorage.getItem('bsw-token')){
      this.ShowLoadingPage();
    }else{
      this.img_base64 = 
      "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAA7CAYAAACZg3PSAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4QkNFSE5J8EkWwAAHL5JREFUeNrdnXmUVdWV/z/n3PvGejVPVEFRgEAxTwIyCDiBKIMCtnYSY0zHNlGTtKuT1ctfOjGd/vXqX3fbGbrbxO60SWuMiUYbMQ4EB0RQggjIWMwUQ0HN86s333t+f5waoerVe1UPgn5ZbwH3nrvPued+7z777LPPvgIImpjO6VzLLOYKE1PQB2zs19povdvACD3H01wBCOAp4KuAFaecBGqBW4CDPY5/GXgaUP1WIAW3fXmGuPGeKaLuXGtzY7X/VmmIj3/+2DsADwNPAvaVuNkBYAC/Be5Nsj1FwNvA5H76UABtwB3A+6lssOkjXc5ijpzIFAzMfgtK5HVp+CYCn1zWLuzGSODGjn8bA5Tt67xAk65fzFgyips/P5X0bA8ZOR6zaEy2NAzR83qRQN1XCnIQ18wGronTRwCZwM2kmFjyNlbLKcyIS6oOFDTRsPI2ViMQicgeKm4Bxl4u4WOmFnLXo/PIyPagbKV/Sv8+IxDASsCdQNlbgZxUVi7zKIj7JggEMWLsYzcb+f3KtSzNVVz2zvcAawGJIWBEGswtAO+A5E8IBSUZ3PPt+RSMzMC2PzNE6kI6kAUjRbfGHwhT0NotZTAVql8NJBC00sJOtnOco9hY04CFwO8vc9/MxG3MpzQdZubCuExIM6HADZsqIRrfzJj/JRAS6a+HhgpoqYZgC1hRSM92c9ej8xkztRDb+uyR6gYgC8iHG9bDmIbELvMCtwNvpaodcVVAJWf5I9uopbrzkButSd4gvkE9OHxzCvz7Qbij9E7yPdmU+sBlgFLaBJ9fCP4obK2GOJrm/v+BkB/DdEGoRROr4TSc3gWtR4qYsnDkZ1JTASwFDHB4YOXHYCRILNCmRxFQlYp2XEIsAUSJUc4B9vARAQK9zufnipuXLpZl7QHKX92UYm5lOeF7M0fgNVdiSE2engQwBdxYrMm1q75/OR0K2HRAej5k5MPIqTDlVnj9u4001bSTX5KOukLkGscsShjPUEwIG9toplY4cLErjmLxAQrGeWFBKbA3mWbCAuB/U3HPvYglgDav4OOsCxy7sBWrh1LKSIdVSw0e/pI5YtJ4sTItTZTv3mcxf1UkFe2Ar07Uw13YvhnE+D41kgLcBiwvAX8MjjTHFaku+tt0QvGsVioOVVEwMmNIDzoZFDGaKSzEHrSSF8SIymPswUdmv6UeQA+DbXCzgOJRgAOIJlaJE23sv0IKXCyys+ECxXl3E5uWT+BwVmMXqbweuONWg5f/28UvfuxkwTyJL03cWVevskeVDGYG3A+ynFAbdGOKtRjC6HfiqYB0J6waCSN9SVWhgNHzFBcqzxAJxlLX9gHrVdhY2IP+Y6GwUR2S+sNYoBK8BqxQwDCIQ8M+sRgYlYp7NgUCiwiHXRXsvnMF7WPL4K19OB2waJ7kkftNbr3BwOtDW1UWSIMZGenMd7nEmynr/V+fgJg9hRLfArJdMDUbCj19uzeVgnwP3FEKL5yEulBCVSggZyQ48qqpOdNCSVnOZ8rWSgNsmCxhtkKTqgioT1zEKDS5Tg21LbKVZj5w7uTD1YtoX3o/HH4fM9DA97/l4JVfuliz0sDrpZepLgUeh0OsffPdmDz+oSs1vXK+HWqCq9hVl8fblbClCmJxHrqtoMQHq0oh05lwNYaEkdcFOFV+HiEFQlwRn9xlx7fQJDJhmYBc0GPbqOTESPRwmHiH9ifoD6537PJVK7CWfR1C7bBvEyOK4M/XGKRnoAnVx/M1DW5ZdJ0xNj83ZQ+mAFjV9b/DTXCmDWQc+baC8ZmwuhSuyRBMyBqwEgWUXgsXKis4ua+W5jo/kVAUpRSi09d+FaO/V60QOAmZEm7reXwUDOz67o0FaEN+SDAb7v624vovg8MFJ3fAmf3MvlVSUiTiOhSkpNTjFss9+eLY759xsvr+IRvxi9BrWiDAkAb2ngbUyHT6Wb7sxqQsgxFp2i77x71xiwqgvsJg7LiFeI0pVB2pJtDehGUHlW1HVA8N9gnwBCRs4d8KTE/0ZhWq3sb6HRBkYDpLi9jebr/LpUgDLJgpYVp3HVqLZQJJuB2K0Es8hxK/5FKYLP6KgeEA24YDb0G0lZuud+BwA3HsWyHANFlTecx+ZvZ02TqURqAnL+sMn9PpLcslc+FI3Ndkc+7fdhCuaIWyrLh+KxRgiPjaDa3nq45C845lfP6r3yQzJ4OYHSMcDNHa1EpjbaMwDINfPL4RYHvHL1FkkwSxQFVFCH5fQf0zfH9InfcdNKs3we1CO967uiULzZQkiAWwAr2AH0jusm50a0l/PRx8h4IcWDhHJvSeGpLZmenMcTrFu4NtwPBH5oCUE0Hd4JsxDO+4XIwMF6GzLditEfioFkang9nPDFQI3YVhO+4yrQCaq+H0m7NZu/YbZOT4sIghJLjT3Lg9buHxejDMQa85D2IQ7Wj7EJELbIACn/aP9kKnnXUwOZGz0cs8OwfbJk0sYcDpPVBZzvSFkrGjZUKeDCnxOZ1i7Z89EN58aItLTb4hnFTlE55dQ/YtYwhVNK2WHkeRkGinpa2w/RFUxIJjLXCqDSZk9dZanY+xOgA7ay1OtVmdQ6bs8etEqA0OvTyGZTd+i+LSYdgX3aDdNZ3/9M0SvUAM5gqY0Nf5UegHnYSDJQetBIdILNuC/X8AK8gNCx14OwbsASHB6WLZC//lHCWEqEi2cmeRj/YDNXlmtns1SqGsbrlWIIodtSBid2stRwdVpIDWCOyqgx210ByBHq9++WaIBFCZRZA9HFzpcOCVPK6b9NdMmFF2Cak+zfg74FkQ98FK0UckQ6edlQE0Jid6OfAfQPNg2mUipF5MK3+PjDRYMm+A8aQjyikagsoLiu277DEbN1s3L5gtk4r+G//fqzAzXaiItVBIMfXiiqz2CCrWQYDjLXCyFSZlQyAKh5pgey1mbYi0dCeZE/KEN90plIKjuy7w5EpQNjGnFzIKwZcHK9auY+4XF3ymSAXaML8bRsh+Ihl62llJEmsaMAvYPJh2mQgDTu2EquNMnCGYVCZ6D4M9wuWC7YqTpxUffmzz7gc2H39iU1mtZCzGmufXW8+jZzgJwZmfRvuBWjNtcv46hOj9pgmw/BFUpx8rYiM/qiWzyEfu2SCFtpviu6YybEw2eSPS8WW6ZTRsGQh47PbniXa0IhaGQJP+99kJtdjK+sz4rQBmomeDYVgkYEx/5VxAKUlP83xo18UgiWVFYP9GUBEWzTPJzhIdsyw9uW1psjl01GLbTtj2scnuvRHqGhR2J/mEwMxyzfdckzMz1hreHjye2PzDzHZjZrjGY8ibLjmpwGqL6AZ0YPrwHNYsLCNruRtXmgNpdLBdKayYjb85hIgzK9y5eSdVZ6oYMXrEZ0ZrrQVOglmqh8G47qpR6BDSJFcrl6JdZDXJts2krgKObsPjhJuXSBCK6iqbvYdsPthlsu9kPn45noJx1+IYH6b+vaew7SiGz0nmwhJ8s4pIm1yQ7ShIW+vO8m73n26k/O6X4lZa9vRqXMMziNT4bxdSDL+kQIfG6sTIsjzWPjKHYcM7AvMU2FY3ORIJ+qw6XcXuLbspGV2SbB9dtcgA0mGMhOvjlVNAcUf5puSqmABcxyDi70wOvQt1p8kpgOPHLd7e5uFEbSkqYypF42dy3Q1l5BTm407zcnLvXjY948SKRXGXZjHy/yzCzHKDUthR+/Zwa/AnjlxP5UCVukoyCB5vyDJzPHdqu617aUXFbKzWMJFqv+68XC9r/+o6ho3KGlJgnm3bbH1tK8u/sByH0zFoOVcLvoh2WAXgJgkj4pVVaCfbMJImlgvt03qNJP0iJh+/jNtlkj58Mu9Vz2PM9LksGTsGX2Y6UkodB27bWLEYHp8Pl9dLqL0dOxRFxWyUpUAphBDjpdu82UhzPlvy2ELO/dOHfVa4OPo9/LurQDEPIWZa/gjR+iDhylZCp5oInmwkdLqZUEUzDqfByr+cxaTrhqck2nP/H/dz5sgZyqaV9QoJ+jRiEnAe3Hn6wQ9oOHb6sw4nX9WNQAlwNpmLTHl2j736kYfk9WvW4E5LQwiBsm2UUthWj85XCrfPh9fno6WuDisYww7Fun18AkOYcm3wdNOLWYtKQ/0R64O0f8SOWCJ/3cQ77YjlDVU0E6n2E2sJo6I96hOw+O7JLLxjAslixQMPoJQywoEALfX1tDY20lRTQ/3582x7fSsjy0bidDoxhHaGfhr9V2m62ydIPVQNCMGg7azR6OW255O5yMwryLOnLV6Mx+fDtqx+u1cBLo+HtIwMAOxQDDsY07wS6KHMaSx0FaVPI45jzY5YAKPr/vfwLfEaNnneCFY8MAuHy0g60vPOhx8mFAhIl8eDbVnEYjGqKyp46tvf5rl/fY4db39EydgSRk0YxcjxIykeVUx2Qbayolb3pOAqxjeAPKBJRzLkJ3JNp52VTtKOKRMd8fAiSfhYzXGzZpFdWNhbO/XZMoXD6cSXna3/G7awQzo20fJHiDYECZ9tyW0/ULtq7tPf2Lkn/qbWZcSZHgshmLt8HFn5aVix5Gdwtm13/YSUOFwuSsrKmL5kCZtfeIHdW3aze8tu3QEOE1+Wj9yCXJWWmWZdqXDloaBjXErPvCiSIR562lnNyVd5PTqO8EiiF5jTlyzB4XQmtJ/OME0ycnMBsCMx6l4+TN1L5QQrmvRw1hRGSmvFh9u//B+5ZdQ2HO1TTBqwhjh2gVKKs0frmW8NOXqjUyDSMJizbBkfvfkm7a3da+axaIzmumaa65o7+/+qhxewYbrUrqyE0enPSpgd3RiOtrUSJ9a4mTNlor0pDYPMvDwAVNSm/tXe9WSWwtgVTMmbxI0oXmw5CzueuETMLBKwC058UoW/OYwvy5WQO2Eg2JbFyIkTGT97Np9sHpTP76rAY2jD/TAsF0lGHgu0wSRJOqhdoCcJz5CgE1y6vV6ZyJOTUtJcV8e5Y8cuOef0wdgVcN1fQ8n1OJzprAu34Uwv7i5z0z9rf9Popaxz+gbukOozLZw/2RjX6ZksnG43c5cvx3QOOUDyT4Y8YDfkGnqROCn09GcNAnOBiYkWTshSlYZB9ZkzPPuDH7Bvy5au48KA/Kkw55sw+c/BmwfKBiFY7M1jkjO9W4YrC1b8nNIpn+f2qfeBZ4AN3eFAlGO7L6R0cFK2zYQ5cyidmHD/XHXwAm6YI7TiSu7+0WELhYOrOp8+wnL6w4BRq1JKTu3fzwtPPEHFwe6oHm8+jLkVRi7WGkvZ3R5wISmUDlZljGDvkn+AQB24MyAa4Bbp4JqSheDOhoO/hpYz/dd9bHcVwfYILq+jT4IJITo0mt3hZB34hg3DJG9EIZHYCCKhKCF/iEgwQiwcw7ZsAWDFrk4f1+PoFDg/0wF93sHI6LSzjg7mYr1b+ilgwMDOuMQSQrB/2zZe/OEPqT2r/WOGC4rnwNiVkFmin7fqY8CWktWNx3jKk0O9KwPaqvB6clkHSKUgfzLM/romV82+vus/f6KR2rMtlE7Mx1YKBEghQAismEVbY5ALp5o4fajWrqtstQdyFSil2PHma6QXK5avXE4sahENRYkEIoTaQyLkD0khBZt+umlw3X6ZkQX8EIqlDh0eFCTanzUIOwtgBjpKdttABfsklhAC27LY/sYbvPLkk7Q26IXlzFEwfjUMmwWGs29CdcmQTDVcXG842WDHQFnMEJJ5neeVDenDIW8S1OynT43kbw5xYm8NY6YWomI2QX+E+spWTpfXcfyTas6U11F/oY1oOKYG6idpGJTv2MHpkx8x8YaxCEPikBKn20ladhooRDgQFlerH8uBVlFRWCiGsNlBoad46UBL8pdnoF0cyRNLCEEkHOad55/nD//zDKFAe9e53DIomgNSxieVFoRLmqxtOsnr1z5E7ORG7hCC7K7TEuoOwcmN4HSAxwt+P1gXueD2vn8GX46XioO1nD5YQ925ZsKBMKah8KbBiBHgS0e4nAgF7O7DNSukpO5cJTvfeYXRc4djOswu94rq2J+gl64Utrg6Ix++BxwCYyKsEJpng4KNtrMKGBSxQPsgf8QA2xV7EUtKib+lhd//53+ybf16xufFqELQFNAP4cx72jYae7s23AcyrIXkpvRixh1ZT5PTx4oex2mvgUO/hVAz3P+XsOZuOFMBJ47B8WNQUwUeD+TknSd66gITCmwW3AU5eZCVDRmZkJ4O3jRwOpH+Ngwh4ca5+j6kkBiGgZCSUHuA9195gbxr3PhyrlzOhlSiI5JhlIQlQ5XlRttZxwd3+SRgDrAxXqEuYkkpaaiq4nc//jEH3tvMPdcKvnGjk/ufjdDUsVfDisDRV8BwwJhlDLj0KSTDpYPlwqBaCMr0QYgG4fBL0HwKZs6GrzwERcUweaqeAEQjEAqDaYJpKgxDISVdxnnnJKFzM5QVg2hUa1KAd994iVg0Ynu96WSm53D+6DFisobCsWM+laT6C3TUXQiWCJ3pcEiQDNqfBTp32QoSIZY0DM4dPcpv/uUJag7t5m9vM/nWMgftYXWJi8sKU3P4ZXKkA8eomxgQwuBzQtKG6CCxglOboHIHZGbBI4/CsCKIdQ+BtmXT7PWSTQd1lYJ4K04Xt3H/pBPYYUsKBMofxX/2FAtnTtIL7J/CjH0dORmchTqgb8hGYKed5SOB6V3fuKlDxPn+CphCSo7s3Mnz//wEzqYT/Ne9Tv5stoFpgL/vlAjbYkGmlb/IeMMBJYvit0AaXNsZOiAkXPgYTrwB2HDPvbBwSW/S2DaNkQjvOhzcPdiO84zOwW6PIr0OhCmxQlEufFJDVnH2YEX+SeHT3VUmYX4q5PW0swZJrLHoBHy/66+A/GDDBn75/b9jlH2SFx908bm5ht5F1f+LfRp4O9oOB38D53cw0JAoEdr+aTkLh1+EaDtcOxe+9BUwLtrGZ1nsQLF3QKlxe64jl6itUDEL79QCqgNthFqDV/0W+ovxNfTisalTEw1LlVwP2s4aJBzoiId+N2HKl3/0Q+u2khpefNDFvGsu3aiqAKTAdMhOGycKbADaI23aD1W1i7jOSSEg3ArlL0DbBXA6tcFeWER37Dw6EDUW4xUhaE2Zx12BmeXGmpBB9amaT91mijHAeZ05bMWQhfVAp501hN5Y3CGiT5j/tMri/gUuMj30acnZlmLhzaV8+RvTaGuJ4G+LOP/+0fd3rrtv0j6FWrD1D2fZ/0wbhgMKZ/TthrBjcPy1bkdoJAIH9sIty3sT0rapCLTztsfLHansRGzwzimi8tljjAiPwHBcLRm2B0bHusKUCFybSg+bRA+FaYB/cCJK0ClPT/R10nz4BlP3cz8aQilIS3Nw8+1jMA1JMBAzvvvQja0nonWvOxxywXsbT/OdBzez95dtzHoQ8qf0JpcQcHY7VLzTu44Xn4e5C2DJTd02lmXx1oLpnPvkeIoHLKVwFaXTUOyk8Vw9w8YX6fTbdLfpajXqXweOwVIfpNRAFOioveT2rveCRGvR5/oSozelD9CnTY0hQpEYbo+JhY2fIIFA9A1fuvObN902etjfP3kDf/vQZvb+op1ZX9OOVGVrY73hKBx5GYQlEcLumsG1NMPPfgITJkFBIVgW7dEoG45d6LrvlEJIiXNCDvvWn6fJr/A4weMxcLlNnG4HhsPEjsaQ1tU1VL6vQ2OSjmS4QpgPlAH7Lz4x4CK0ENDWEiYciuHx6OI+0qmsOHN43KScrdIp7l52xzVEoxbfe3gLnzwd4NqvQc5YaK+Dg89DaXE+C+8bwa9+uo9opFud7dkFz/0SHv0bsC32+Fv5yJdc9sfEIMBqC9Fw2EHD2n/hlNeHbKnFbK3D2VaPq64BV6hZmZF2ldBK9pXFTJLKYnNFUYjOtjw4YvnbIgQDMbKyOzvdzcTp+dFgILredBhrbKEct68bTyRi84Nvvs/ep4NMux9Ob4FxRcP5h6duovqcn1/99KL6Fbz4a5g7H+Yt5NXxE2kJDjpxThwoRf271TSO/irM0zvRbSACRBT4LQXhMLQ2CgwDnvnVFXw2A+I2eqQmugpxO/BzLjLVErAHBQF/lIA/2mt8aqoPEgrEttqWfQRACcUdnyvjO/96PbS6+PjfBTOGj+PHv76VyZPzEWa3Z7wnmpvgR/+Pyj9+wJsbXtJEFkIPo0Lqa6TUbgnD6PTG659hdMu07d4zzK7WS0Hbnmqqg4tRC1Z25VHt+tnoCl1OgdsNbs+f+kH1RAFJxED9iTALmHrxwYQ0VigYo6010otYFcebWLdgZdXR0MdvGmaHYAF33TeJWNTm9PFmHnpsDtk5biwUDofsd6p/6ADvPPAFjgHsOQptbahgQHvjoxE9iwwGod0Pba3Q2gqtLdpOa2mBthbsYBC7S3wHOTEE4fMtnN9bQHTtg5qN8bYhXX1IKmrzT4RstA34x54HB05PKSAStmhtDtPTpj5e3sjhGTuxYvarDqd8UEqhZy0S7vmLKSilMEyJjcJAYJgS2XeYcQidW9wC+PwaACzL0qNTNKLXASMdBItF9Szyoklcr7CZWGMQKxhDBiJUvROk/frHICf36vhAXOLojDNP5CNLnWhGb3YeiqGo0P7TwiTkLAf+jR4brRPKexqL2DTXhzC6vrqjx5/mhiDRqL3P7TF3SCm6tiIJozNzfHcKf9OUSKPPdu4Huna3HinvurlB4+hXXwelYkIKwuseh4nTL8cHWi43RqDX5BKFAv4v8AJJ57PtBQsdwfAbdIh9Iuj8yNPbnQcGHgo72vzsk3txeQwmzyzAXVqBbSvK99Zx26pxgZZAeINhyluF6N9mM/vXWK+RdIrM+AidatKNBjh6AKI6Of2nDNcTZ+9lH6gGNgEXUlB3M7CPxCNV09BGfOLE6sTu7VU8eu8mSkZlxI6XNzJnUTE7t55nb+PXUEq97XQZFYYhrunv+n6Gwlq0D/Dy4cONcOoQjJ/2aRoKO3cfJ6N59gAnU1R/O/AeyYVA34Jey6yGJEMwgu1Rjh1qUEopdm7VERM1F/z88LvbT1sxu98vBynANEVfQ+FWoDxFndE36qvg3ZevVuO8P4xhgNREfeBdtL2aKmwhueCH8fSIvhjy8tOBXTV87ydLVDRibVC2au+vnGFKjN7EigLr0e6ky4vN66G28tMU2dCZ4SVRNKBf0lTiEMm99J0feZKQAmJ9+/63aWkM4W+LfGTZqt/vRRuGvDjhxlFS/B3iflFxRA+JBt15VC+BrQi2Q7A9Odmph5sEUxP1wH4GvaOrXzST/PNZQkeEa0oWzI8ebGBScUFLLGq/2t+QY5jiYo31JqkxNAeGbcEbz8HRg9BYB8GAPtb5KXET+HCj4jufs3j8i1ekSXHQmUUvGWxm0EEKA8pNZi1kFDqcZkjT0i58cekr7Kj8CrGYvTEzy/1oRqZruH0Rwwyj11DYDLx6GTqif+zbDg8vhcwcyMqDnELIK4KC4eD2wks/g4rDV4MlthTtcU8UrWh76HJgLzosZlqC5Q20tn0hNV/vBh7/+hbe2nDyyNr7Jr77ha9NvW/2/GI6yaUAwxAY3UPhDpL6+GcKYFvQUK1/lyA1X4hIAdLR0/ZkUE7SH55IGLXAByROLNAfeZqUstixtzacRAis9b86vP7RezeFNvz2CFbU7jIUpKG97+hJ/3qG8J2W1KOLVFfQvO+zqukkmZoIra2aL2ND3yG5CVYxsMqMWR3enYtyJQoBlq0zY9Pj67fEyerWscyy49yplvLHH3lvWmtz2L7nK5MNw9C5TA1DKOAMA6tuq+NmElEjop+yFonHsYmOskPxdEU76rykfxTKsLFFjzTghtKp4S7+mtciulK3J9TmEKmfDV6MT9D7HEYn2D8SWP7/ARySnogVnDj9AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE3LTA5LTEzVDIxOjMzOjU3LTA0OjAw9dwHaQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNy0wOS0xM1QyMTozMzo1Ny0wNDowMISBv9UAAAAASUVORK5CYII=";
    }
  }

  
  doLogin() {
    if (this.loginData.username == "" || this.loginData.password == "" ){
      this.presentToast('Mohon isi semua form');
    }else{
      this.authService.login(this.loginData).then((result) => {
        this.data = result;
        localStorage.setItem('bsw-foto', this.site_url+this.data.foto);
        localStorage.setItem('bsw-nama-lengkap', this.data.nama_lengkap);
        localStorage.setItem('bsw-token', this.data.token);
        localStorage.setItem('bsw-riwayat-golongan', this.data.riwayat_golongan);
        localStorage.setItem('bsw-riwayat-unit-kerja', this.data.riwayat_unit_kerja);
        localStorage.setItem('bsw-riwayat-jabatan', this.data.riwayat_jabatan);
        localStorage.setItem('bsw-groups', this.data.groups);
        localStorage.setItem('bsw-username', this.loginData.username);
        localStorage.setItem('bsw-password', this.loginData.password);
        this.ShowLoadingPage();
      }, (err) => {
        this.presentToast("Username atau Password salah");
      });
    }
  }

  checkingTokenToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  ShowLoadingPage() {
  let loading = this.loadingCtrl.create({
    content:  `<ion-spinner name="bubbles"></ion-spinner>`
  });

  loading.present();
  setTimeout(() => {
  this.navCtrl.setRoot(DashboardPage);
  this.menu.swipeEnable(true, 'myMenu'); 
  loading.dismiss();
  }, 4000);
}

}

