var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, LoadingController, ToastController, Platform } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { App } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
/**
 * Generated class for the SigninPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SigninPage = (function () {
    function SigninPage(navCtrl, navParams, menu, authService, loadingCtrl, toastCtrl, platform, app) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.authService = authService;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.app = app;
        this.loginData = { username: '', password: '' };
        this.menu.swipeEnable(false, 'myMenu');
        this.checkingToken();
    }
    SigninPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SigninPage');
    };
    SigninPage.prototype.checkingToken = function () {
        if (localStorage.getItem('bsw-token') && localStorage.getItem('session-status') == '1') {
            console.log("You don't need to login");
            console.log('Your Token : ' + localStorage.getItem('bsw-token'));
            this.checkingTokenToast(localStorage.getItem('bsw-token'));
            this.navCtrl.setRoot(DashboardPage);
            console.log(localStorage.getItem('session-status'));
            localStorage.setItem('session-status', '0');
            console.log(localStorage.getItem('session-status'));
            window.location.reload();
            this.menu.swipeEnable(true, 'myMenu');
        }
        else if (localStorage.getItem('bsw-token') && localStorage.getItem('session-status') == '0') {
            console.log("You don't need to login");
            console.log('Your Token : ' + localStorage.getItem('bsw-token'));
            this.checkingTokenToast(localStorage.getItem('bsw-token'));
            this.navCtrl.setRoot(DashboardPage);
            this.menu.swipeEnable(true, 'myMenu');
            console.log(localStorage.getItem('session-status'));
        }
        else {
            console.log('You need to login');
        }
    };
    SigninPage.prototype.doLogin = function () {
        var _this = this;
        if (this.loginData.username == "" || this.loginData.password == "") {
            this.presentToast('Mohon isi semua form');
        }
        else {
            this.showLoader();
            this.authService.login(this.loginData).then(function (result) {
                _this.loading.dismiss();
                _this.data = result;
                localStorage.setItem('bsw-foto', _this.data.foto);
                localStorage.setItem('bsw-nama-lengkap', _this.data.nama_lengkap);
                localStorage.setItem('bsw-token', _this.data.token);
                localStorage.setItem('bsw-riwayat-golongan', _this.data.riwayat_golongan);
                localStorage.setItem('bsw-riwayat-unit-kerja', _this.data.riwayat_unit_kerja);
                localStorage.setItem('bsw-riwayat-jabatan', _this.data.riwayat_jabatan);
                localStorage.setItem('bsw-username', _this.loginData.username);
                localStorage.setItem('bsw-password', _this.loginData.password);
                localStorage.setItem('session-status', '1');
                {
                    var toast = _this.toastCtrl.create({
                        message: "Berhasil : " + localStorage.getItem('bsw-nama-lengkap'),
                        duration: 3000,
                        position: 'top',
                        dismissOnPageChange: true
                    });
                    toast.onDidDismiss(function () {
                        console.log('Berhasil');
                    });
                    toast.present();
                    _this.checkingToken();
                }
            }, function (err) {
                _this.loading.dismiss();
                _this.presentToast("Username atau Password salah");
            });
        }
    };
    SigninPage.prototype.checkingTokenToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    SigninPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Authenticating...'
        });
        this.loading.present();
    };
    SigninPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    return SigninPage;
}());
SigninPage = __decorate([
    Component({
        selector: 'page-signin',
        templateUrl: 'signin.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams,
        MenuController, AuthServiceProvider, LoadingController,
        ToastController, Platform, App])
], SigninPage);
export { SigninPage };
//# sourceMappingURL=signin.js.map