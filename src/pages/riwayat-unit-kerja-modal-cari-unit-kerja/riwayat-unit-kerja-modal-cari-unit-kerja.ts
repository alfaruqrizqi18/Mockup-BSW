import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController  } from 'ionic-angular';
import { Http } from '@angular/http';

import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the RiwayatUnitKerjaModalCariUnitKerjaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-riwayat-unit-kerja-modal-cari-unit-kerja',
  templateUrl: 'riwayat-unit-kerja-modal-cari-unit-kerja.html',
})
export class RiwayatUnitKerjaModalCariUnitKerjaPage {
loading: any;
public base_url;
cari_unit_kerja: string;
nama_unit_kerja: string;
items: string[];
public buttonClicked: any; 
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public http: Http,
    public loadingCtrl: LoadingController, public dataService: DataServiceRiwayatProvider) {
    this.base_url = this.dataService.base_url.toString();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RiwayatUnitKerjaModalCariUnitKerjaPage');
  }
  checkDigit(){
    if (this.cari_unit_kerja.length <= 3) {
        this.buttonClicked = "false";
    }else if (this.cari_unit_kerja.length > 3){
        this.buttonClicked = "true";
    }
  }
  closeModal(id, nama_unit_kerja) {
    let data = {
    	id: id, 
    	nama_unit_kerja:nama_unit_kerja
    };
    this.viewCtrl.dismiss(data);
    console.log('Text Modal :'+data.id+" "+data.nama_unit_kerja);
  }
  getListDataUnitKerja(){
    this.showLoader();
    this.http.get(this.base_url+'unit-kerja/?q='+this.cari_unit_kerja).map(res => res.json()).subscribe(data => {
       this.loading.dismiss();
        //this.items = data;
        this.items = data.results;
    }, (err) => {
      this.loading.dismiss();
    });
  }
  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Fetching Data...'
    });

    this.loading.present();
  }

}
