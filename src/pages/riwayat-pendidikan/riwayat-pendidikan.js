var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { DetailRiwayatPendidikanPage } from '../../pages/detail-riwayat-pendidikan/detail-riwayat-pendidikan';
/**
 * Generated class for the RiwayatPendidikanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RiwayatPendidikanPage = (function () {
    function RiwayatPendidikanPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.foto = localStorage.getItem('bsw-foto');
    }
    RiwayatPendidikanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatPendidikanPage');
    };
    RiwayatPendidikanPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(DashboardPage);
    };
    RiwayatPendidikanPage.prototype.GoToDetailRiwayatPendidikan = function () {
        this.navCtrl.push(DetailRiwayatPendidikanPage);
    };
    return RiwayatPendidikanPage;
}());
RiwayatPendidikanPage = __decorate([
    Component({
        selector: 'page-riwayat-pendidikan',
        templateUrl: 'riwayat-pendidikan.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams])
], RiwayatPendidikanPage);
export { RiwayatPendidikanPage };
//# sourceMappingURL=riwayat-pendidikan.js.map