import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { DetailRiwayatPendidikanPage } from '../../pages/detail-riwayat-pendidikan/detail-riwayat-pendidikan';


import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the RiwayatPendidikanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-riwayat-pendidikan',
  templateUrl: 'riwayat-pendidikan.html',
})
export class RiwayatPendidikanPage {
public foto: any;
form_operator = {operator_cari_pegawai: ''};
form_verifikator = {verifikator_cari_pegawai: ''};
loading: any;
lists_riwayat_pendidikan: any;
groups : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public dataService: DataServiceRiwayatProvider, 
    public loadingCtrl: LoadingController) {
    this.foto = localStorage.getItem('bsw-foto');
    this.getDataRiwayatPendidikan();
    this.groups = localStorage.getItem('bsw-groups');
  }
  doRefresh(refresh){
    this.getDataRiwayatPendidikan();
     refresh.complete();
  }
  getDataRiwayatPendidikan(){
      this.showLoader();
      this.dataService.getListRiwayatPendidikan().subscribe(data => {
        this.loading.dismiss();
        //this.lists_riwayat_kenaikan_pangkat = data;
        this.lists_riwayat_pendidikan = data.results;
    });
  }
  operator_getDataRiwayatUnitKerja(){
      let param;
      if (this.form_operator.operator_cari_pegawai == "" ||  this.form_operator.operator_cari_pegawai == null) {
    this.getDataRiwayatPendidikan();
      }else {
        param = this.form_operator.operator_cari_pegawai;
          this.showLoader();
          this.dataService.non_pegawai_getDetailRiwayatRiwayatPendidikan(param).subscribe(data => {
          this.loading.dismiss();
            //this.lists_riwayat_unit_kerja = data;
            this.lists_riwayat_pendidikan = data.results;
            console.log(data.results)
        });
      }
  }

  verifikator_getDataRiwayatUnitKerja(){
      let param;
      if (this.form_verifikator.verifikator_cari_pegawai == "" ||  this.form_verifikator.verifikator_cari_pegawai == null) {
    this.getDataRiwayatPendidikan();
      }else {
        param = this.form_verifikator.verifikator_cari_pegawai;
          this.showLoader();
          this.dataService.non_pegawai_getDetailRiwayatRiwayatPendidikan(param).subscribe(data => {
          this.loading.dismiss();
            //this.lists_riwayat_unit_kerja = data;
            this.lists_riwayat_pendidikan = data.results;
            console.log(data.results)
        });
      }
  }
   showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Mengambil data dari server...'
    });

    this.loading.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RiwayatPendidikanPage');
  }
  GoToDashboard(){
  	this.navCtrl.setRoot(DashboardPage);
  }
  GoToDetailRiwayatPendidikan(id){
    this.navCtrl.push(DetailRiwayatPendidikanPage, {id: id});
  }
}
