import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';


import { RiwayatJabatanPage } from '../../pages/riwayat-jabatan/riwayat-jabatan';
import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';

/**
 * Generated class for the DetailRiwayatJabatanPegawaiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-detail-riwayat-jabatan-pegawai',
  templateUrl: 'detail-riwayat-jabatan-pegawai.html',
})
export class DetailRiwayatJabatanPegawaiPage {
public foto: any;
lists_detail_riwayat_jabatan: any;
post_data ={status_to_verifikator: '', status_verifikasi: ''};
error = {data: ''};
loading: any;
data_result_post: any;
groups : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController, public loadingCtrl: LoadingController, 
   private dataService: DataServiceRiwayatProvider) {
    this.foto = localStorage.getItem('bsw-foto');
    console.log("Parameter : "+navParams.get('id'));
    this.getDetailRiwayatJabatan(navParams.get('id'));
    this.groups = localStorage.getItem('bsw-groups');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailRiwayatJabatanPegawaiPage');
  }

  getDetailRiwayatJabatan(id){
    this.showLoader("Mengambil data dari server...");
     this.dataService.getDetailRiwayatJabatan(id).subscribe(data => {
      this.loading.dismiss();
        //this.lists_detail_riwayat_jabatan = data;
        this.lists_detail_riwayat_jabatan = data.results;
    });
  }
  showLoader(msg){
    this.loading = this.loadingCtrl.create({
        content: msg
    });
    this.loading.present();
  }
  doSubmitVerifikasi(id){
      let data = {
        status : 1
      }
       this.postData(id,data);
       console.log(data);
  }
  
  doSubmitToVerifikator(id){
      let data = {
        status : 2
      }
       this.postData(id,data);
       console.log(data);
  }

  postData(id,data){
    this.showLoader("Mohon tunggu sebentar...");
    this.dataService.postStatusDetailJabatan(id,data).then((result) => {
        this.loading.dismiss();
        this.data_result_post = result; {
          let toast = this.toastCtrl.create({
            message: "Berhasil mengirim data...",
            duration: 3000,
            position: 'top',
            dismissOnPageChange: true
          });
          toast.onDidDismiss(() => {
            console.log('Berhasil');
          });
          toast.present();
          this.navCtrl.setRoot(RiwayatJabatanPage);
        }
      }, (err) => {
        this.loading.dismiss();
        this.presentToast(err);
      });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  
  GoToDashboard(){
  	this.navCtrl.setRoot(DashboardPage);
  }

}
