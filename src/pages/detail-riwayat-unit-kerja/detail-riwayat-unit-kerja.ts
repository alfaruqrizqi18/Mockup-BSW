import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { RiwayatUnitKerjaPage } from '../../pages/riwayat-unit-kerja/riwayat-unit-kerja';

import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the DetailRiwayatUnitKerjaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */



@Component({
  selector: 'page-detail-riwayat-unit-kerja',
  templateUrl: 'detail-riwayat-unit-kerja.html',
})
export class DetailRiwayatUnitKerjaPage {
public foto: any;
public status: string;
lists_detail_riwayat_unit_kerja: any;
post_data ={status_to_verifikator: '', status_verifikasi: ''};
error = {data: ''};
loading: any;
data_result_post: any;
groups : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public dataService: DataServiceRiwayatProvider, 
   private toastCtrl: ToastController, public loadingCtrl: LoadingController, private transfer: FileTransfer, private androidPermissions: AndroidPermissions) {
    this.foto = localStorage.getItem('bsw-foto');
    this.getDetailRiwayatUnitKerja(navParams.get('id'));
    this.groups = localStorage.getItem('bsw-groups');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailRiwayatUnitKerjaPage');
  }

  getDetailRiwayatUnitKerja(id){
    this.showLoader("Mengambil data dari server...")
    this.dataService.getDetailRiwayatUnitKerja(id).subscribe(data => {
      this.loading.dismiss();
        //this.lists_detail_riwayat_unit_kerja = data;
        this.lists_detail_riwayat_unit_kerja = data.results;
    });
  }

  doSubmitVerifikasi(id){
      let data = {
        status : 1
      }
       this.postData(id,data);
       console.log(data);
  }
  
  doSubmitToVerifikator(id){
      let data = {
        status : 2
      }
       this.postData(id,data);
       console.log(data);
  }

  showLoader(msg){
    this.loading = this.loadingCtrl.create({
        content: msg
    });
    this.loading.present();
  }

  postData(id,data){
    this.showLoader("Mohon tunggu sebentar...");
    this.dataService.postStatusDetailUnitKerja(id,data).then((result) => {
        this.loading.dismiss();
        this.data_result_post = result; {
          let toast = this.toastCtrl.create({
            message: "Berhasil mengirim data...",
            duration: 3000,
            position: 'top',
            dismissOnPageChange: true
          });
          toast.onDidDismiss(() => {
            console.log('Berhasil');
          });
          toast.present();
          this.navCtrl.setRoot(RiwayatUnitKerjaPage);
        }
      }, (err) => {
        this.loading.dismiss();
        this.presentToast(err);
      });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  downloadFile(fileUrl){
    const fileTransfer: FileTransferObject = this.transfer.create();
    const url = fileUrl;
    const pemilik = localStorage.getItem('bsw-nama-lengkap');
    var date = new Date().toISOString();
    var filesdownload =  pemilik + '-' + date + '-' + '.png';
    fileTransfer.download(url, 'file:///storage/sdcard0/Download/' + filesdownload ).then((entry) => {
      //this.error.data = "Benar : "+entry.toURL();
      this.presentToast("File telah berhasil di unduh")
    }, (error) => {
      //this.error.data = "Salah : "+JSON.stringify(error);
    });
  }
  checking_permissions_to_download(fileUrl){
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
      success => this.downloadFile(fileUrl),
      err => this.getting_permissions(fileUrl)
    );
  }
  getting_permissions(fileUrl){
    this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
    this.downloadFile(fileUrl);
  }
  GoToDashboard(){
  	this.navCtrl.setRoot(DashboardPage);
  }
}
