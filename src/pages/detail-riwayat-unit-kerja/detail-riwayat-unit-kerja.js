var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { DataServiceRiwayatProvider } from '../../providers/data-service-riwayat/data-service-riwayat';
/**
 * Generated class for the DetailRiwayatUnitKerjaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var DetailRiwayatUnitKerjaPage = (function () {
    function DetailRiwayatUnitKerjaPage(navCtrl, navParams, dataService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.foto = localStorage.getItem('bsw-foto');
        this.getDetailRiwayatUnitKerja(navParams.get('id'));
    }
    DetailRiwayatUnitKerjaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailRiwayatUnitKerjaPage');
    };
    DetailRiwayatUnitKerjaPage.prototype.getDetailRiwayatUnitKerja = function (id) {
        var _this = this;
        this.dataService.getDetailRiwayatUnitKerja(id).subscribe(function (data) {
            //this.lists_detail_riwayat_unit_kerja = data;
            _this.lists_detail_riwayat_unit_kerja = data.results;
        });
    };
    DetailRiwayatUnitKerjaPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(DashboardPage);
    };
    return DetailRiwayatUnitKerjaPage;
}());
DetailRiwayatUnitKerjaPage = __decorate([
    Component({
        selector: 'page-detail-riwayat-unit-kerja',
        templateUrl: 'detail-riwayat-unit-kerja.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, DataServiceRiwayatProvider])
], DetailRiwayatUnitKerjaPage);
export { DetailRiwayatUnitKerjaPage };
//# sourceMappingURL=detail-riwayat-unit-kerja.js.map