import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http'; // import untuk ambil data json
import { FileChooser } from '@ionic-native/file-chooser';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { FileTransfer} from '@ionic-native/file-transfer';
import { Base64 } from '@ionic-native/base64';
import { AndroidPermissions } from '@ionic-native/android-permissions';
//===================

import { MyApp } from './app.component';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { SigninPage } from '../pages/signin/signin';
import { RiwayatJabatanPage } from '../pages/riwayat-jabatan/riwayat-jabatan';
import { RiwayatKenaikanPangkatPage } from '../pages/riwayat-kenaikan-pangkat/riwayat-kenaikan-pangkat';
import { RiwayatUnitKerjaPage } from '../pages/riwayat-unit-kerja/riwayat-unit-kerja';
import { RiwayatPendidikanPage } from '../pages/riwayat-pendidikan/riwayat-pendidikan';
import { RiwayatHukumanPage } from '../pages/riwayat-hukuman/riwayat-hukuman';
import { RiwayatCutiPage } from '../pages/riwayat-cuti/riwayat-cuti';
import { DataKeluargaPage } from '../pages/data-keluarga/data-keluarga';
import { RiwayatUnitKerjaNonPegawaiPage } from '../pages/riwayat-unit-kerja-non-pegawai/riwayat-unit-kerja-non-pegawai';
//===================

import { DetailRiwayatJabatanPegawaiPage } from '../pages/detail-riwayat-jabatan-pegawai/detail-riwayat-jabatan-pegawai';
import { DetailRiwayatKenaikanPangkatPage } from '../pages/detail-riwayat-kenaikan-pangkat/detail-riwayat-kenaikan-pangkat';
import { DetailRiwayatUnitKerjaPage } from '../pages/detail-riwayat-unit-kerja/detail-riwayat-unit-kerja';
import { DetailRiwayatPendidikanPage } from '../pages/detail-riwayat-pendidikan/detail-riwayat-pendidikan';
import { DetailRiwayatHukumanPage } from '../pages/detail-riwayat-hukuman/detail-riwayat-hukuman';
import { DetailRiwayatCutiPage } from '../pages/detail-riwayat-cuti/detail-riwayat-cuti';
//===================

import { RiwayatUnitKerjaAddPage } from '../pages/riwayat-unit-kerja-add/riwayat-unit-kerja-add';
import { RiwayatJabatanAddPage } from '../pages/riwayat-jabatan-add/riwayat-jabatan-add';
//===================

import { RiwayatUnitKerjaModalCariPegawaiPage } from '../pages/riwayat-unit-kerja-modal-cari-pegawai/riwayat-unit-kerja-modal-cari-pegawai';
import { RiwayatUnitKerjaModalCariUnitKerjaPage } from '../pages/riwayat-unit-kerja-modal-cari-unit-kerja/riwayat-unit-kerja-modal-cari-unit-kerja';
//===================

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { IonicStorageModule } from '@ionic/storage';
import { DataServiceRiwayatProvider } from '../providers/data-service-riwayat/data-service-riwayat';


@NgModule({
  declarations: [
    MyApp,
    DashboardPage,
    SigninPage,
    RiwayatJabatanPage,
    RiwayatKenaikanPangkatPage,
    RiwayatUnitKerjaPage,
    RiwayatPendidikanPage,
    RiwayatHukumanPage,
    RiwayatCutiPage,
    DataKeluargaPage,
    DetailRiwayatJabatanPegawaiPage,
    DetailRiwayatKenaikanPangkatPage,
    DetailRiwayatUnitKerjaPage,
    DetailRiwayatPendidikanPage,
    DetailRiwayatHukumanPage,
    DetailRiwayatCutiPage,
    RiwayatUnitKerjaAddPage,
    RiwayatUnitKerjaModalCariPegawaiPage,
    RiwayatUnitKerjaModalCariUnitKerjaPage,
    RiwayatUnitKerjaNonPegawaiPage,
    RiwayatJabatanAddPage
  ],
  imports: [
    BrowserModule,
    HttpModule, // import untuk ambil data json
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DashboardPage,
    SigninPage,
    RiwayatJabatanPage,
    RiwayatKenaikanPangkatPage,
    RiwayatUnitKerjaPage,
    RiwayatPendidikanPage,
    RiwayatHukumanPage,
    RiwayatCutiPage,
    DataKeluargaPage,
    DetailRiwayatJabatanPegawaiPage,
    DetailRiwayatKenaikanPangkatPage,
    DetailRiwayatUnitKerjaPage,
    DetailRiwayatPendidikanPage,
    DetailRiwayatHukumanPage,
    DetailRiwayatCutiPage,
    RiwayatUnitKerjaAddPage,
    RiwayatUnitKerjaModalCariPegawaiPage,
    RiwayatUnitKerjaModalCariUnitKerjaPage,
    RiwayatUnitKerjaNonPegawaiPage,
    RiwayatJabatanAddPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FileChooser,
    File,
    FilePath,
    FileTransfer,
    Base64,
    AndroidPermissions,
    {provide: ErrorHandler,  useClass: IonicErrorHandler},
    AuthServiceProvider,
    DataServiceRiwayatProvider
    ]
})
export class AppModule {}
