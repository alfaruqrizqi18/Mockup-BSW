var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http'; // import untuk ambil data json
//===================
import { MyApp } from './app.component';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { SigninPage } from '../pages/signin/signin';
import { RiwayatJabatanPage } from '../pages/riwayat-jabatan/riwayat-jabatan';
import { RiwayatKenaikanPangkatPage } from '../pages/riwayat-kenaikan-pangkat/riwayat-kenaikan-pangkat';
import { RiwayatUnitKerjaPage } from '../pages/riwayat-unit-kerja/riwayat-unit-kerja';
import { RiwayatPendidikanPage } from '../pages/riwayat-pendidikan/riwayat-pendidikan';
import { RiwayatHukumanPage } from '../pages/riwayat-hukuman/riwayat-hukuman';
import { RiwayatCutiPage } from '../pages/riwayat-cuti/riwayat-cuti';
import { DataKeluargaPage } from '../pages/data-keluarga/data-keluarga';
//===================
import { DetailRiwayatJabatanPegawaiPage } from '../pages/detail-riwayat-jabatan-pegawai/detail-riwayat-jabatan-pegawai';
import { DetailRiwayatKenaikanPangkatPage } from '../pages/detail-riwayat-kenaikan-pangkat/detail-riwayat-kenaikan-pangkat';
import { DetailRiwayatUnitKerjaPage } from '../pages/detail-riwayat-unit-kerja/detail-riwayat-unit-kerja';
import { DetailRiwayatPendidikanPage } from '../pages/detail-riwayat-pendidikan/detail-riwayat-pendidikan';
import { DetailRiwayatHukumanPage } from '../pages/detail-riwayat-hukuman/detail-riwayat-hukuman';
import { DetailRiwayatCutiPage } from '../pages/detail-riwayat-cuti/detail-riwayat-cuti';
//===================
import { RiwayatUnitKerjaAddPage } from '../pages/riwayat-unit-kerja-add/riwayat-unit-kerja-add';
//===================
import { RiwayatUnitKerjaModalCariPegawaiPage } from '../pages/riwayat-unit-kerja-modal-cari-pegawai/riwayat-unit-kerja-modal-cari-pegawai';
import { RiwayatUnitKerjaModalCariUnitKerjaPage } from '../pages/riwayat-unit-kerja-modal-cari-unit-kerja/riwayat-unit-kerja-modal-cari-unit-kerja';
//===================
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { IonicStorageModule } from '@ionic/storage';
import { DataServiceRiwayatProvider } from '../providers/data-service-riwayat/data-service-riwayat';
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    NgModule({
        declarations: [
            MyApp,
            DashboardPage,
            SigninPage,
            RiwayatJabatanPage,
            RiwayatKenaikanPangkatPage,
            RiwayatUnitKerjaPage,
            RiwayatPendidikanPage,
            RiwayatHukumanPage,
            RiwayatCutiPage,
            DataKeluargaPage,
            DetailRiwayatJabatanPegawaiPage,
            DetailRiwayatKenaikanPangkatPage,
            DetailRiwayatUnitKerjaPage,
            DetailRiwayatPendidikanPage,
            DetailRiwayatHukumanPage,
            DetailRiwayatCutiPage,
            RiwayatUnitKerjaAddPage,
            RiwayatUnitKerjaModalCariPegawaiPage,
            RiwayatUnitKerjaModalCariUnitKerjaPage
        ],
        imports: [
            BrowserModule,
            HttpModule,
            IonicModule.forRoot(MyApp),
            IonicStorageModule.forRoot()
        ],
        bootstrap: [IonicApp],
        entryComponents: [
            MyApp,
            DashboardPage,
            SigninPage,
            RiwayatJabatanPage,
            RiwayatKenaikanPangkatPage,
            RiwayatUnitKerjaPage,
            RiwayatPendidikanPage,
            RiwayatHukumanPage,
            RiwayatCutiPage,
            DataKeluargaPage,
            DetailRiwayatJabatanPegawaiPage,
            DetailRiwayatKenaikanPangkatPage,
            DetailRiwayatUnitKerjaPage,
            DetailRiwayatPendidikanPage,
            DetailRiwayatHukumanPage,
            DetailRiwayatCutiPage,
            RiwayatUnitKerjaAddPage,
            RiwayatUnitKerjaModalCariPegawaiPage,
            RiwayatUnitKerjaModalCariUnitKerjaPage
        ],
        providers: [
            StatusBar,
            SplashScreen,
            { provide: ErrorHandler, useClass: IonicErrorHandler },
            AuthServiceProvider,
            DataServiceRiwayatProvider
        ]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map