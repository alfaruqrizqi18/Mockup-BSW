import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SigninPage } from '../pages/signin/signin';
import { RiwayatJabatanPage } from '../pages/riwayat-jabatan/riwayat-jabatan';
import { RiwayatKenaikanPangkatPage } from '../pages/riwayat-kenaikan-pangkat/riwayat-kenaikan-pangkat';
import { RiwayatUnitKerjaPage } from '../pages/riwayat-unit-kerja/riwayat-unit-kerja';
import { RiwayatPendidikanPage } from '../pages/riwayat-pendidikan/riwayat-pendidikan';
import { RiwayatHukumanPage } from '../pages/riwayat-hukuman/riwayat-hukuman';
import { RiwayatCutiPage } from '../pages/riwayat-cuti/riwayat-cuti';
import { DataKeluargaPage } from '../pages/data-keluarga/data-keluarga';

import { DataServiceRiwayatProvider } from '../providers/data-service-riwayat/data-service-riwayat';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SigninPage;
  pages: Array<{title: string, component: any, icon: string, badge: string}>;
  groups: string;
  pagesLain_lain: Array<{title: string, component: any, icon: string}>;
  dataHeader : Array<{foto: any, nama_lengkap: any, token: any, riwayat_golongan: any, riwayat_unit_kerja: any, riwayat_jabatan: any}>;
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, 
    public authService: AuthServiceProvider, private dataService: DataServiceRiwayatProvider) {
    this.initializeApp();
    // used for an example of ngFor and navigation
    
    this.pagesLain_lain = [
      { title: 'Riwayat Cuti', component: RiwayatCutiPage, icon: 'heart' },
      { title: 'Riwayat Hukuman', component: RiwayatHukumanPage, icon: 'sad'},
      { title: 'Data Keluarga', component: DataKeluargaPage, icon: 'people' },

    ];
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.statusBar.overlaysWebView(true);
      this.statusBar.backgroundColorByHexString('#388e3c');
      this.getCountRiwayat();
      this.groups = localStorage.getItem('bsw-groups');
    });
  }
  public getCountRiwayat(){
    let badge_jabatan, badge_kenaikan_pangkat, badge_unit_kerja, badge_pendidikan;
    if (localStorage.getItem('bsw-groups') == 'Operator' || localStorage.getItem('bsw-groups') == 'Verifikator') {
      this.dataService.getCountRiwayat().subscribe(data => {
        //this.lists_riwayat_jabatan = data;
        if (data.results[0].get_count_riwayat_jabatan_pegawai != 0) {
          badge_jabatan = data.results[0].get_count_riwayat_jabatan_pegawai;
        }else if(data.results[0].get_count_riwayat_golongan_pegawai != 0){
          badge_kenaikan_pangkat = data.results[0].get_count_riwayat_golongan_pegawai;
        }else if(data.results[0].get_count_riwayat_unitkerja_pegawai != 0){
          badge_unit_kerja = data.results[0].get_count_riwayat_unitkerja_pegawai;
        }else if(data.results[0].get_count_riwayat_pendidikan_pegawai != 0){
          badge_pendidikan = data.results[0].get_count_riwayat_pendidikan_pegawai;
        }

        this.pages = [
        { title: 'Riwayat Jabatan', component: RiwayatJabatanPage, icon: 'briefcase', badge:badge_jabatan },
        { title: 'Riwayat Kenaikan Pangkat', component: RiwayatKenaikanPangkatPage, icon: 'medal', badge:badge_kenaikan_pangkat},
        { title: 'Riwayat Unit Kerja', component: RiwayatUnitKerjaPage, icon: 'ribbon', badge:badge_unit_kerja},
        { title: 'Riwayat Pendidikan', component: RiwayatPendidikanPage, icon: 'school', badge:badge_pendidikan},

      ]
      this.dataHeader = [
         { 
           foto : localStorage.getItem('bsw-foto'), 
           nama_lengkap : localStorage.getItem('bsw-nama-lengkap'),
           token : localStorage.getItem('bsw-token'),
           riwayat_golongan : localStorage.getItem('bsw-riwayat-golongan'),
           riwayat_unit_kerja : localStorage.getItem('bsw-riwayat-unit-kerja'),
           riwayat_jabatan : localStorage.getItem('bsw-riwayat-jabatan')
         }
        ];
       
    
    });
    }else{
        this.dataHeader = [
         { 
           foto : localStorage.getItem('bsw-foto'), 
           nama_lengkap : localStorage.getItem('bsw-nama-lengkap'),
           token : localStorage.getItem('bsw-token'),
           riwayat_golongan : localStorage.getItem('bsw-riwayat-golongan'),
           riwayat_unit_kerja : localStorage.getItem('bsw-riwayat-unit-kerja'),
           riwayat_jabatan : localStorage.getItem('bsw-riwayat-jabatan')
         }
        ];
        
        this.pages = [
        { title: 'Riwayat Jabatan', component: RiwayatJabatanPage, icon: 'briefcase', badge:'' },
        { title: 'Riwayat Kenaikan Pangkat', component: RiwayatKenaikanPangkatPage, icon: 'medal', badge:'' },
        { title: 'Riwayat Unit Kerja', component: RiwayatUnitKerjaPage, icon: 'ribbon', badge:'' },
        { title: 'Riwayat Pendidikan', component: RiwayatPendidikanPage, icon: 'school', badge:''},

      ];
    }
    console.log("Berhasil load menu...");
  }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
    //this.getCountRiwayat();
  }
  refreshBadge(){
    //console.log("Hello")
    this.getCountRiwayat();
  }


  doLogout(){
    this.authService.logout().then((result) => {
    }, (err) => {
      
    });
      this.nav.setRoot(SigninPage);
  }
}
