var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SigninPage } from '../pages/signin/signin';
import { RiwayatJabatanPage } from '../pages/riwayat-jabatan/riwayat-jabatan';
import { RiwayatKenaikanPangkatPage } from '../pages/riwayat-kenaikan-pangkat/riwayat-kenaikan-pangkat';
import { RiwayatUnitKerjaPage } from '../pages/riwayat-unit-kerja/riwayat-unit-kerja';
import { RiwayatPendidikanPage } from '../pages/riwayat-pendidikan/riwayat-pendidikan';
import { RiwayatHukumanPage } from '../pages/riwayat-hukuman/riwayat-hukuman';
import { RiwayatCutiPage } from '../pages/riwayat-cuti/riwayat-cuti';
import { DataKeluargaPage } from '../pages/data-keluarga/data-keluarga';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, authService, loadingCtrl, toastCtrl) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.authService = authService;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.rootPage = SigninPage;
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Riwayat Jabatan', component: RiwayatJabatanPage, icon: 'briefcase' },
            { title: 'Riwayat Kenaikan Pangkat', component: RiwayatKenaikanPangkatPage, icon: 'medal' },
            { title: 'Riwayat Unit Kerja', component: RiwayatUnitKerjaPage, icon: 'ribbon' },
            { title: 'Riwayat Pendidikan', component: RiwayatPendidikanPage, icon: 'school' },
        ];
        this.pagesLain_lain = [
            { title: 'Riwayat Cuti', component: RiwayatCutiPage, icon: 'heart' },
            { title: 'Riwayat Hukuman', component: RiwayatHukumanPage, icon: 'sad' },
            { title: 'Data Keluarga', component: DataKeluargaPage, icon: 'people' },
        ];
        this.dataHeader = [
            {
                foto: localStorage.getItem('bsw-foto'),
                nama_lengkap: localStorage.getItem('bsw-nama-lengkap'),
                token: localStorage.getItem('bsw-token'),
                riwayat_golongan: localStorage.getItem('bsw-riwayat-golongan'),
                riwayat_unit_kerja: localStorage.getItem('bsw-riwayat-unit-kerja'),
                riwayat_jabatan: localStorage.getItem('bsw-riwayat-jabatan')
            }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.statusBar.overlaysWebView(true);
            _this.statusBar.backgroundColorByHexString('#388e3c');
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.doLogout = function () {
        var _this = this;
        this.authService.logout().then(function (result) {
        }, function (err) {
            _this.presentToast(err);
        });
        this.nav.setRoot(SigninPage);
    };
    MyApp.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    return MyApp;
}());
__decorate([
    ViewChild(Nav),
    __metadata("design:type", Nav)
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Component({
        templateUrl: 'app.html'
    }),
    __metadata("design:paramtypes", [Platform, StatusBar, SplashScreen,
        AuthServiceProvider, LoadingController, ToastController])
], MyApp);
export { MyApp };
//# sourceMappingURL=app.component.js.map