var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/*
Generated class for the DataServiceRiwayatProvider provider.

See https://angular.io/docs/ts/latest/guide/dependency-injection.html
for more info on providers and Angular DI.
    */
//let base_url = "https://bsw.kedirikab.go.id/api/";
var base_url = "http://192.168.200.88:8899/api-mobile/";
var DataServiceRiwayatProvider = (function () {
    function DataServiceRiwayatProvider(http) {
        this.http = http;
        console.log('Hello DataServiceRiwayatProvider Provider');
    }
    //==================================================//
    //====================== GET DATA =====================//
    //==================================================//
    DataServiceRiwayatProvider.prototype.getListRiwayatJabatan = function () {
        console.log(this.http.get(base_url + 'riwayat-jabatan-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); }));
        return this.http.get(base_url + 'riwayat-jabatan-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); });
    };
    DataServiceRiwayatProvider.prototype.getDetailRiwayatJabatan = function (id) {
        console.log(this.http.get(base_url + 'detail-riwayat-jabatan/' + id).map(function (res) { return res.json(); }));
        return this.http.get(base_url + 'detail-riwayat-jabatan/' + id).map(function (res) { return res.json(); });
    };
    //======================================================================//
    DataServiceRiwayatProvider.prototype.getListRiwayatUnitKerja = function () {
        console.log(this.http.get(base_url + 'riwayat-unit-kerja-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); }));
        return this.http.get(base_url + 'riwayat-unit-kerja-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); });
    };
    DataServiceRiwayatProvider.prototype.getDetailRiwayatUnitKerja = function (id) {
        console.log(this.http.get(base_url + 'detail-riwayat-unit-kerja/' + id).map(function (res) { return res.json(); }));
        return this.http.get(base_url + 'detail-riwayat-unit-kerja/' + id).map(function (res) { return res.json(); });
    };
    //======================================================================//
    DataServiceRiwayatProvider.prototype.getListRiwayatKenaikanPangkat = function () {
        console.log(this.http.get(base_url + 'riwayat-golongan-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); }));
        return this.http.get(base_url + 'riwayat-golongan-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); });
    };
    DataServiceRiwayatProvider.prototype.getDetailRiwayatKenaikanPangkat = function (id) {
        console.log(this.http.get(base_url + 'detail-riwayat-golongan/' + id).map(function (res) { return res.json(); }));
        return this.http.get(base_url + 'detail-riwayat-golongan/' + id).map(function (res) { return res.json(); });
    };
    return DataServiceRiwayatProvider;
}());
DataServiceRiwayatProvider = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http])
], DataServiceRiwayatProvider);
export { DataServiceRiwayatProvider };
//# sourceMappingURL=data-service-riwayat.js.map