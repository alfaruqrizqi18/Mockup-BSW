import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
Generated class for the DataServiceRiwayatProvider provider.

See https://angular.io/docs/ts/latest/guide/dependency-injection.html
for more info on providers and Angular DI.
	*/

//let base_url = "http://192.168.200.88:8899/api-mobile/";
@Injectable()
export class DataServiceRiwayatProvider {
lists_riwayat_jabatan : any;
public base_url = "http://simita.aitc.co.id/api-mobile/";
public site_url = "http://simita.aitc.co.id";
constructor(public http: Http) {
	console.log('Hello DataServiceRiwayatProvider Provider');
}


//==================================================//
//====================== GET DATA =====================//
//==================================================//
getListPegawaiDetail(){
console.log(this.http.get(this.base_url+'pegawai-detail/'+localStorage.getItem('bsw-token')).map(res => res.json()));
return this.http.get(this.base_url+'pegawai-detail/'+localStorage.getItem('bsw-token')).map(res => res.json());
}

//============

getListRiwayatJabatan(){
console.log(this.http.get(this.base_url+'riwayat-jabatan-list/'+localStorage.getItem('bsw-token')).map(res => res.json()));
return this.http.get(this.base_url+'riwayat-jabatan-list/'+localStorage.getItem('bsw-token')).map(res => res.json());
}
getDetailRiwayatJabatan(id){
console.log(this.http.get(this.base_url+'detail-riwayat-jabatan/'+id).map(res => res.json()));
return this.http.get(this.base_url+'detail-riwayat-jabatan/'+id).map(res => res.json());
}
non_pegawai_getDetailRiwayatJabatan(param){
console.log(this.http.get(this.base_url+'riwayat-jabatan-list/'+localStorage.getItem('bsw-token')+'/?q='+param).map(res => res.json()));
return this.http.get(this.base_url+'riwayat-jabatan-list/'+localStorage.getItem('bsw-token')+'/?q='+param).map(res => res.json());
}

//======================================================================//

getListRiwayatUnitKerja(){
console.log(this.http.get(this.base_url+'riwayat-unit-kerja-list/'+localStorage.getItem('bsw-token')).map(res => res.json()));
return this.http.get(this.base_url+'riwayat-unit-kerja-list/'+localStorage.getItem('bsw-token')).map(res => res.json());
}
getDetailRiwayatUnitKerja(id){
console.log(this.http.get(this.base_url+'detail-riwayat-unit-kerja/'+id).map(res => res.json()));
return this.http.get(this.base_url+'detail-riwayat-unit-kerja/'+id).map(res => res.json());
}
non_pegawai_getDataRiwayatUnitKerja(param){
console.log(this.http.get(this.base_url+'riwayat-unit-kerja-list/'+localStorage.getItem('bsw-token')+'/?q='+param).map(res => res.json()));
return this.http.get(this.base_url+'riwayat-unit-kerja-list/'+localStorage.getItem('bsw-token')+'/?q='+param).map(res => res.json());
}

//======================================================================//

getListRiwayatKenaikanPangkat(){
console.log(this.http.get(this.base_url+'riwayat-golongan-list/'+localStorage.getItem('bsw-token')).map(res => res.json()));
return this.http.get(this.base_url+'riwayat-golongan-list/'+localStorage.getItem('bsw-token')).map(res => res.json());
}
getDetailRiwayatKenaikanPangkat(id){
console.log(this.http.get(this.base_url+'detail-riwayat-golongan/'+id).map(res => res.json()));
return this.http.get(this.base_url+'detail-riwayat-golongan/'+id).map(res => res.json());
}
non_pegawai_getDetailRiwayatKenaikanPangkat(param){
console.log(this.http.get(this.base_url+'riwayat-golongan-list/'+localStorage.getItem('bsw-token')+'/?q='+param).map(res => res.json()));
return this.http.get(this.base_url+'riwayat-golongan-list/'+localStorage.getItem('bsw-token')+'/?q='+param).map(res => res.json());
}

//============

getListRiwayatPendidikan(){
console.log(this.http.get(this.base_url+'riwayat-pendidikan-list/'+localStorage.getItem('bsw-token')).map(res => res.json()));
return this.http.get(this.base_url+'riwayat-pendidikan-list/'+localStorage.getItem('bsw-token')).map(res => res.json());
}

getDetailRiwayatPendidikan(id){
console.log(this.http.get(this.base_url+'detail-riwayat-pendidikan/'+id).map(res => res.json()));
return this.http.get(this.base_url+'detail-riwayat-pendidikan/'+id).map(res => res.json());
}
non_pegawai_getDetailRiwayatRiwayatPendidikan(param){
console.log(this.http.get(this.base_url+'riwayat-pendidikan-list/'+localStorage.getItem('bsw-token')+'/?q='+param).map(res => res.json()));
return this.http.get(this.base_url+'riwayat-pendidikan-list/'+localStorage.getItem('bsw-token')+'/?q='+param).map(res => res.json());
}

//============

getListDataPerkawinan(){
console.log(this.http.get(this.base_url+'data-perkawinan-detail/'+localStorage.getItem('bsw-token')).map(res => res.json()));
return this.http.get(this.base_url+'data-perkawinan-detail/'+localStorage.getItem('bsw-token')).map(res => res.json());
}

//============

getListRiwayatCuti(){
console.log(this.http.get(this.base_url+'cuti-list/'+localStorage.getItem('bsw-token')).map(res => res.json()));
return this.http.get(this.base_url+'cuti-list/'+localStorage.getItem('bsw-token')).map(res => res.json());
}
getDetailRiwayatCuti(id){
console.log(this.http.get(this.base_url+'detail-cuti/'+id).map(res => res.json()));
return this.http.get(this.base_url+'detail-cuti/'+id).map(res => res.json());
}

//============

getListRiwayatHukuman(){
console.log(this.http.get(this.base_url+'hukuman-list/'+localStorage.getItem('bsw-token')).map(res => res.json()));
return this.http.get(this.base_url+'hukuman-list/'+localStorage.getItem('bsw-token')).map(res => res.json());
}
getDetailRiwayatHukuman(id){
console.log(this.http.get(this.base_url+'detail-hukuman/'+id).map(res => res.json()));
return this.http.get(this.base_url+'detail-hukuman/'+id).map(res => res.json());
}

//============

getCountRiwayat(){
console.log(this.http.get(this.base_url+'count-riwayat/'+localStorage.getItem('bsw-token')).map(res => res.json()));
return this.http.get(this.base_url+'count-riwayat/'+localStorage.getItem('bsw-token')).map(res => res.json());
}

//==================================================//
//====================== GET DATA =====================//
//==================================================//


//==================================================//
//====================== POST DATA =====================//
//==================================================//
postRiwayatUnitKerjaBaru(data) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.http.post(this.base_url+'riwayat-unit-kerja/post/'+localStorage.getItem('bsw-token')+'/', JSON.stringify(data), {headers: headers})
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }

 postStatusDetailUnitKerja(id,data) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.http.post(this.base_url+'riwayat-unit-kerja/status-post/'+localStorage.getItem('bsw-token')+'/'+id+'/', JSON.stringify(data), {headers: headers})
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }
  postStatusDetailJabatan(id,data) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.http.post(this.base_url+'riwayat-jabatan/status-post/'+localStorage.getItem('bsw-token')+'/'+id+'/', JSON.stringify(data), {headers: headers})
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }
  postStatusDetailKenaikanPangkat(id,data) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.http.post(this.base_url+'riwayat-golongan/status-post/'+localStorage.getItem('bsw-token')+'/'+id+'/', JSON.stringify(data), {headers: headers})
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }
  postStatusDetailPendidikan(id,data) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.http.post(this.base_url+'riwayat-pendidikan/status-post/'+localStorage.getItem('bsw-token')+'/'+id+'/', JSON.stringify(data), {headers: headers})
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }

//==================================================//
//====================== POST DATA =====================//
//==================================================//
}
