webpackJsonp([0],{

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SigninPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the SigninPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SigninPage = (function () {
    function SigninPage(navCtrl, navParams, menu, authService, loadingCtrl, toastCtrl, platform, app, dataService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.authService = authService;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.app = app;
        this.dataService = dataService;
        this.loginData = { username: '', password: '' };
        this.menu.swipeEnable(false, 'myMenu');
        this.site_url = this.dataService.site_url.toString();
    }
    SigninPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SigninPage');
        if (localStorage.getItem('bsw-token')) {
            this.ShowLoadingPage();
        }
        else {
            this.img_base64 =
                "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAA7CAYAAACZg3PSAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4QkNFSE5J8EkWwAAHL5JREFUeNrdnXmUVdWV/z/n3PvGejVPVEFRgEAxTwIyCDiBKIMCtnYSY0zHNlGTtKuT1ctfOjGd/vXqX3fbGbrbxO60SWuMiUYbMQ4EB0RQggjIWMwUQ0HN86s333t+f5waoerVe1UPgn5ZbwH3nrvPued+7z777LPPvgIImpjO6VzLLOYKE1PQB2zs19povdvACD3H01wBCOAp4KuAFaecBGqBW4CDPY5/GXgaUP1WIAW3fXmGuPGeKaLuXGtzY7X/VmmIj3/+2DsADwNPAvaVuNkBYAC/Be5Nsj1FwNvA5H76UABtwB3A+6lssOkjXc5ijpzIFAzMfgtK5HVp+CYCn1zWLuzGSODGjn8bA5Tt67xAk65fzFgyips/P5X0bA8ZOR6zaEy2NAzR83qRQN1XCnIQ18wGronTRwCZwM2kmFjyNlbLKcyIS6oOFDTRsPI2ViMQicgeKm4Bxl4u4WOmFnLXo/PIyPagbKV/Sv8+IxDASsCdQNlbgZxUVi7zKIj7JggEMWLsYzcb+f3KtSzNVVz2zvcAawGJIWBEGswtAO+A5E8IBSUZ3PPt+RSMzMC2PzNE6kI6kAUjRbfGHwhT0NotZTAVql8NJBC00sJOtnOco9hY04CFwO8vc9/MxG3MpzQdZubCuExIM6HADZsqIRrfzJj/JRAS6a+HhgpoqYZgC1hRSM92c9ej8xkztRDb+uyR6gYgC8iHG9bDmIbELvMCtwNvpaodcVVAJWf5I9uopbrzkButSd4gvkE9OHxzCvz7Qbij9E7yPdmU+sBlgFLaBJ9fCP4obK2GOJrm/v+BkB/DdEGoRROr4TSc3gWtR4qYsnDkZ1JTASwFDHB4YOXHYCRILNCmRxFQlYp2XEIsAUSJUc4B9vARAQK9zufnipuXLpZl7QHKX92UYm5lOeF7M0fgNVdiSE2engQwBdxYrMm1q75/OR0K2HRAej5k5MPIqTDlVnj9u4001bSTX5KOukLkGscsShjPUEwIG9toplY4cLErjmLxAQrGeWFBKbA3mWbCAuB/U3HPvYglgDav4OOsCxy7sBWrh1LKSIdVSw0e/pI5YtJ4sTItTZTv3mcxf1UkFe2Ar07Uw13YvhnE+D41kgLcBiwvAX8MjjTHFaku+tt0QvGsVioOVVEwMmNIDzoZFDGaKSzEHrSSF8SIymPswUdmv6UeQA+DbXCzgOJRgAOIJlaJE23sv0IKXCyys+ECxXl3E5uWT+BwVmMXqbweuONWg5f/28UvfuxkwTyJL03cWVevskeVDGYG3A+ynFAbdGOKtRjC6HfiqYB0J6waCSN9SVWhgNHzFBcqzxAJxlLX9gHrVdhY2IP+Y6GwUR2S+sNYoBK8BqxQwDCIQ8M+sRgYlYp7NgUCiwiHXRXsvnMF7WPL4K19OB2waJ7kkftNbr3BwOtDW1UWSIMZGenMd7nEmynr/V+fgJg9hRLfArJdMDUbCj19uzeVgnwP3FEKL5yEulBCVSggZyQ48qqpOdNCSVnOZ8rWSgNsmCxhtkKTqgioT1zEKDS5Tg21LbKVZj5w7uTD1YtoX3o/HH4fM9DA97/l4JVfuliz0sDrpZepLgUeh0OsffPdmDz+oSs1vXK+HWqCq9hVl8fblbClCmJxHrqtoMQHq0oh05lwNYaEkdcFOFV+HiEFQlwRn9xlx7fQJDJhmYBc0GPbqOTESPRwmHiH9ifoD6537PJVK7CWfR1C7bBvEyOK4M/XGKRnoAnVx/M1DW5ZdJ0xNj83ZQ+mAFjV9b/DTXCmDWQc+baC8ZmwuhSuyRBMyBqwEgWUXgsXKis4ua+W5jo/kVAUpRSi09d+FaO/V60QOAmZEm7reXwUDOz67o0FaEN+SDAb7v624vovg8MFJ3fAmf3MvlVSUiTiOhSkpNTjFss9+eLY759xsvr+IRvxi9BrWiDAkAb2ngbUyHT6Wb7sxqQsgxFp2i77x71xiwqgvsJg7LiFeI0pVB2pJtDehGUHlW1HVA8N9gnwBCRs4d8KTE/0ZhWq3sb6HRBkYDpLi9jebr/LpUgDLJgpYVp3HVqLZQJJuB2K0Es8hxK/5FKYLP6KgeEA24YDb0G0lZuud+BwA3HsWyHANFlTecx+ZvZ02TqURqAnL+sMn9PpLcslc+FI3Ndkc+7fdhCuaIWyrLh+KxRgiPjaDa3nq45C845lfP6r3yQzJ4OYHSMcDNHa1EpjbaMwDINfPL4RYHvHL1FkkwSxQFVFCH5fQf0zfH9InfcdNKs3we1CO967uiULzZQkiAWwAr2AH0jusm50a0l/PRx8h4IcWDhHJvSeGpLZmenMcTrFu4NtwPBH5oCUE0Hd4JsxDO+4XIwMF6GzLditEfioFkang9nPDFQI3YVhO+4yrQCaq+H0m7NZu/YbZOT4sIghJLjT3Lg9buHxejDMQa85D2IQ7Wj7EJELbIACn/aP9kKnnXUwOZGz0cs8OwfbJk0sYcDpPVBZzvSFkrGjZUKeDCnxOZ1i7Z89EN58aItLTb4hnFTlE55dQ/YtYwhVNK2WHkeRkGinpa2w/RFUxIJjLXCqDSZk9dZanY+xOgA7ay1OtVmdQ6bs8etEqA0OvTyGZTd+i+LSYdgX3aDdNZ3/9M0SvUAM5gqY0Nf5UegHnYSDJQetBIdILNuC/X8AK8gNCx14OwbsASHB6WLZC//lHCWEqEi2cmeRj/YDNXlmtns1SqGsbrlWIIodtSBid2stRwdVpIDWCOyqgx210ByBHq9++WaIBFCZRZA9HFzpcOCVPK6b9NdMmFF2Cak+zfg74FkQ98FK0UckQ6edlQE0Jid6OfAfQPNg2mUipF5MK3+PjDRYMm+A8aQjyikagsoLiu277DEbN1s3L5gtk4r+G//fqzAzXaiItVBIMfXiiqz2CCrWQYDjLXCyFSZlQyAKh5pgey1mbYi0dCeZE/KEN90plIKjuy7w5EpQNjGnFzIKwZcHK9auY+4XF3ymSAXaML8bRsh+Ihl62llJEmsaMAvYPJh2mQgDTu2EquNMnCGYVCZ6D4M9wuWC7YqTpxUffmzz7gc2H39iU1mtZCzGmufXW8+jZzgJwZmfRvuBWjNtcv46hOj9pgmw/BFUpx8rYiM/qiWzyEfu2SCFtpviu6YybEw2eSPS8WW6ZTRsGQh47PbniXa0IhaGQJP+99kJtdjK+sz4rQBmomeDYVgkYEx/5VxAKUlP83xo18UgiWVFYP9GUBEWzTPJzhIdsyw9uW1psjl01GLbTtj2scnuvRHqGhR2J/mEwMxyzfdckzMz1hreHjye2PzDzHZjZrjGY8ibLjmpwGqL6AZ0YPrwHNYsLCNruRtXmgNpdLBdKayYjb85hIgzK9y5eSdVZ6oYMXrEZ0ZrrQVOglmqh8G47qpR6BDSJFcrl6JdZDXJts2krgKObsPjhJuXSBCK6iqbvYdsPthlsu9kPn45noJx1+IYH6b+vaew7SiGz0nmwhJ8s4pIm1yQ7ShIW+vO8m73n26k/O6X4lZa9vRqXMMziNT4bxdSDL+kQIfG6sTIsjzWPjKHYcM7AvMU2FY3ORIJ+qw6XcXuLbspGV2SbB9dtcgA0mGMhOvjlVNAcUf5puSqmABcxyDi70wOvQt1p8kpgOPHLd7e5uFEbSkqYypF42dy3Q1l5BTm407zcnLvXjY948SKRXGXZjHy/yzCzHKDUthR+/Zwa/AnjlxP5UCVukoyCB5vyDJzPHdqu617aUXFbKzWMJFqv+68XC9r/+o6ho3KGlJgnm3bbH1tK8u/sByH0zFoOVcLvoh2WAXgJgkj4pVVaCfbMJImlgvt03qNJP0iJh+/jNtlkj58Mu9Vz2PM9LksGTsGX2Y6UkodB27bWLEYHp8Pl9dLqL0dOxRFxWyUpUAphBDjpdu82UhzPlvy2ELO/dOHfVa4OPo9/LurQDEPIWZa/gjR+iDhylZCp5oInmwkdLqZUEUzDqfByr+cxaTrhqck2nP/H/dz5sgZyqaV9QoJ+jRiEnAe3Hn6wQ9oOHb6sw4nX9WNQAlwNpmLTHl2j736kYfk9WvW4E5LQwiBsm2UUthWj85XCrfPh9fno6WuDisYww7Fun18AkOYcm3wdNOLWYtKQ/0R64O0f8SOWCJ/3cQ77YjlDVU0E6n2E2sJo6I96hOw+O7JLLxjAslixQMPoJQywoEALfX1tDY20lRTQ/3582x7fSsjy0bidDoxhHaGfhr9V2m62ydIPVQNCMGg7azR6OW255O5yMwryLOnLV6Mx+fDtqx+u1cBLo+HtIwMAOxQDDsY07wS6KHMaSx0FaVPI45jzY5YAKPr/vfwLfEaNnneCFY8MAuHy0g60vPOhx8mFAhIl8eDbVnEYjGqKyp46tvf5rl/fY4db39EydgSRk0YxcjxIykeVUx2Qbayolb3pOAqxjeAPKBJRzLkJ3JNp52VTtKOKRMd8fAiSfhYzXGzZpFdWNhbO/XZMoXD6cSXna3/G7awQzo20fJHiDYECZ9tyW0/ULtq7tPf2Lkn/qbWZcSZHgshmLt8HFn5aVix5Gdwtm13/YSUOFwuSsrKmL5kCZtfeIHdW3aze8tu3QEOE1+Wj9yCXJWWmWZdqXDloaBjXErPvCiSIR562lnNyVd5PTqO8EiiF5jTlyzB4XQmtJ/OME0ycnMBsCMx6l4+TN1L5QQrmvRw1hRGSmvFh9u//B+5ZdQ2HO1TTBqwhjh2gVKKs0frmW8NOXqjUyDSMJizbBkfvfkm7a3da+axaIzmumaa65o7+/+qhxewYbrUrqyE0enPSpgd3RiOtrUSJ9a4mTNlor0pDYPMvDwAVNSm/tXe9WSWwtgVTMmbxI0oXmw5CzueuETMLBKwC058UoW/OYwvy5WQO2Eg2JbFyIkTGT97Np9sHpTP76rAY2jD/TAsF0lGHgu0wSRJOqhdoCcJz5CgE1y6vV6ZyJOTUtJcV8e5Y8cuOef0wdgVcN1fQ8n1OJzprAu34Uwv7i5z0z9rf9Popaxz+gbukOozLZw/2RjX6ZksnG43c5cvx3QOOUDyT4Y8YDfkGnqROCn09GcNAnOBiYkWTshSlYZB9ZkzPPuDH7Bvy5au48KA/Kkw55sw+c/BmwfKBiFY7M1jkjO9W4YrC1b8nNIpn+f2qfeBZ4AN3eFAlGO7L6R0cFK2zYQ5cyidmHD/XHXwAm6YI7TiSu7+0WELhYOrOp8+wnL6w4BRq1JKTu3fzwtPPEHFwe6oHm8+jLkVRi7WGkvZ3R5wISmUDlZljGDvkn+AQB24MyAa4Bbp4JqSheDOhoO/hpYz/dd9bHcVwfYILq+jT4IJITo0mt3hZB34hg3DJG9EIZHYCCKhKCF/iEgwQiwcw7ZsAWDFrk4f1+PoFDg/0wF93sHI6LSzjg7mYr1b+ilgwMDOuMQSQrB/2zZe/OEPqT2r/WOGC4rnwNiVkFmin7fqY8CWktWNx3jKk0O9KwPaqvB6clkHSKUgfzLM/romV82+vus/f6KR2rMtlE7Mx1YKBEghQAismEVbY5ALp5o4fajWrqtstQdyFSil2PHma6QXK5avXE4sahENRYkEIoTaQyLkD0khBZt+umlw3X6ZkQX8EIqlDh0eFCTanzUIOwtgBjpKdttABfsklhAC27LY/sYbvPLkk7Q26IXlzFEwfjUMmwWGs29CdcmQTDVcXG842WDHQFnMEJJ5neeVDenDIW8S1OynT43kbw5xYm8NY6YWomI2QX+E+spWTpfXcfyTas6U11F/oY1oOKYG6idpGJTv2MHpkx8x8YaxCEPikBKn20ladhooRDgQFlerH8uBVlFRWCiGsNlBoad46UBL8pdnoF0cyRNLCEEkHOad55/nD//zDKFAe9e53DIomgNSxieVFoRLmqxtOsnr1z5E7ORG7hCC7K7TEuoOwcmN4HSAxwt+P1gXueD2vn8GX46XioO1nD5YQ925ZsKBMKah8KbBiBHgS0e4nAgF7O7DNSukpO5cJTvfeYXRc4djOswu94rq2J+gl64Utrg6Ix++BxwCYyKsEJpng4KNtrMKGBSxQPsgf8QA2xV7EUtKib+lhd//53+ybf16xufFqELQFNAP4cx72jYae7s23AcyrIXkpvRixh1ZT5PTx4oex2mvgUO/hVAz3P+XsOZuOFMBJ47B8WNQUwUeD+TknSd66gITCmwW3AU5eZCVDRmZkJ4O3jRwOpH+Ngwh4ca5+j6kkBiGgZCSUHuA9195gbxr3PhyrlzOhlSiI5JhlIQlQ5XlRttZxwd3+SRgDrAxXqEuYkkpaaiq4nc//jEH3tvMPdcKvnGjk/ufjdDUsVfDisDRV8BwwJhlDLj0KSTDpYPlwqBaCMr0QYgG4fBL0HwKZs6GrzwERcUweaqeAEQjEAqDaYJpKgxDISVdxnnnJKFzM5QVg2hUa1KAd994iVg0Ynu96WSm53D+6DFisobCsWM+laT6C3TUXQiWCJ3pcEiQDNqfBTp32QoSIZY0DM4dPcpv/uUJag7t5m9vM/nWMgftYXWJi8sKU3P4ZXKkA8eomxgQwuBzQtKG6CCxglOboHIHZGbBI4/CsCKIdQ+BtmXT7PWSTQd1lYJ4K04Xt3H/pBPYYUsKBMofxX/2FAtnTtIL7J/CjH0dORmchTqgb8hGYKed5SOB6V3fuKlDxPn+CphCSo7s3Mnz//wEzqYT/Ne9Tv5stoFpgL/vlAjbYkGmlb/IeMMBJYvit0AaXNsZOiAkXPgYTrwB2HDPvbBwSW/S2DaNkQjvOhzcPdiO84zOwW6PIr0OhCmxQlEufFJDVnH2YEX+SeHT3VUmYX4q5PW0swZJrLHoBHy/66+A/GDDBn75/b9jlH2SFx908bm5ht5F1f+LfRp4O9oOB38D53cw0JAoEdr+aTkLh1+EaDtcOxe+9BUwLtrGZ1nsQLF3QKlxe64jl6itUDEL79QCqgNthFqDV/0W+ovxNfTisalTEw1LlVwP2s4aJBzoiId+N2HKl3/0Q+u2khpefNDFvGsu3aiqAKTAdMhOGycKbADaI23aD1W1i7jOSSEg3ArlL0DbBXA6tcFeWER37Dw6EDUW4xUhaE2Zx12BmeXGmpBB9amaT91mijHAeZ05bMWQhfVAp501hN5Y3CGiT5j/tMri/gUuMj30acnZlmLhzaV8+RvTaGuJ4G+LOP/+0fd3rrtv0j6FWrD1D2fZ/0wbhgMKZ/TthrBjcPy1bkdoJAIH9sIty3sT0rapCLTztsfLHansRGzwzimi8tljjAiPwHBcLRm2B0bHusKUCFybSg+bRA+FaYB/cCJK0ClPT/R10nz4BlP3cz8aQilIS3Nw8+1jMA1JMBAzvvvQja0nonWvOxxywXsbT/OdBzez95dtzHoQ8qf0JpcQcHY7VLzTu44Xn4e5C2DJTd02lmXx1oLpnPvkeIoHLKVwFaXTUOyk8Vw9w8YX6fTbdLfpajXqXweOwVIfpNRAFOioveT2rveCRGvR5/oSozelD9CnTY0hQpEYbo+JhY2fIIFA9A1fuvObN902etjfP3kDf/vQZvb+op1ZX9OOVGVrY73hKBx5GYQlEcLumsG1NMPPfgITJkFBIVgW7dEoG45d6LrvlEJIiXNCDvvWn6fJr/A4weMxcLlNnG4HhsPEjsaQ1tU1VL6vQ2OSjmS4QpgPlAH7Lz4x4CK0ENDWEiYciuHx6OI+0qmsOHN43KScrdIp7l52xzVEoxbfe3gLnzwd4NqvQc5YaK+Dg89DaXE+C+8bwa9+uo9opFud7dkFz/0SHv0bsC32+Fv5yJdc9sfEIMBqC9Fw2EHD2n/hlNeHbKnFbK3D2VaPq64BV6hZmZF2ldBK9pXFTJLKYnNFUYjOtjw4YvnbIgQDMbKyOzvdzcTp+dFgILredBhrbKEct68bTyRi84Nvvs/ep4NMux9Ob4FxRcP5h6duovqcn1/99KL6Fbz4a5g7H+Yt5NXxE2kJDjpxThwoRf271TSO/irM0zvRbSACRBT4LQXhMLQ2CgwDnvnVFXw2A+I2eqQmugpxO/BzLjLVErAHBQF/lIA/2mt8aqoPEgrEttqWfQRACcUdnyvjO/96PbS6+PjfBTOGj+PHv76VyZPzEWa3Z7wnmpvgR/+Pyj9+wJsbXtJEFkIPo0Lqa6TUbgnD6PTG659hdMu07d4zzK7WS0Hbnmqqg4tRC1Z25VHt+tnoCl1OgdsNbs+f+kH1RAFJxED9iTALmHrxwYQ0VigYo6010otYFcebWLdgZdXR0MdvGmaHYAF33TeJWNTm9PFmHnpsDtk5biwUDofsd6p/6ADvPPAFjgHsOQptbahgQHvjoxE9iwwGod0Pba3Q2gqtLdpOa2mBthbsYBC7S3wHOTEE4fMtnN9bQHTtg5qN8bYhXX1IKmrzT4RstA34x54HB05PKSAStmhtDtPTpj5e3sjhGTuxYvarDqd8UEqhZy0S7vmLKSilMEyJjcJAYJgS2XeYcQidW9wC+PwaACzL0qNTNKLXASMdBItF9Szyoklcr7CZWGMQKxhDBiJUvROk/frHICf36vhAXOLojDNP5CNLnWhGb3YeiqGo0P7TwiTkLAf+jR4brRPKexqL2DTXhzC6vrqjx5/mhiDRqL3P7TF3SCm6tiIJozNzfHcKf9OUSKPPdu4Huna3HinvurlB4+hXXwelYkIKwuseh4nTL8cHWi43RqDX5BKFAv4v8AJJ57PtBQsdwfAbdIh9Iuj8yNPbnQcGHgo72vzsk3txeQwmzyzAXVqBbSvK99Zx26pxgZZAeINhyluF6N9mM/vXWK+RdIrM+AidatKNBjh6AKI6Of2nDNcTZ+9lH6gGNgEXUlB3M7CPxCNV09BGfOLE6sTu7VU8eu8mSkZlxI6XNzJnUTE7t55nb+PXUEq97XQZFYYhrunv+n6Gwlq0D/Dy4cONcOoQjJ/2aRoKO3cfJ6N59gAnU1R/O/AeyYVA34Jey6yGJEMwgu1Rjh1qUEopdm7VERM1F/z88LvbT1sxu98vBynANEVfQ+FWoDxFndE36qvg3ZevVuO8P4xhgNREfeBdtL2aKmwhueCH8fSIvhjy8tOBXTV87ydLVDRibVC2au+vnGFKjN7EigLr0e6ky4vN66G28tMU2dCZ4SVRNKBf0lTiEMm99J0feZKQAmJ9+/63aWkM4W+LfGTZqt/vRRuGvDjhxlFS/B3iflFxRA+JBt15VC+BrQi2Q7A9Odmph5sEUxP1wH4GvaOrXzST/PNZQkeEa0oWzI8ebGBScUFLLGq/2t+QY5jiYo31JqkxNAeGbcEbz8HRg9BYB8GAPtb5KXET+HCj4jufs3j8i1ekSXHQmUUvGWxm0EEKA8pNZi1kFDqcZkjT0i58cekr7Kj8CrGYvTEzy/1oRqZruH0Rwwyj11DYDLx6GTqif+zbDg8vhcwcyMqDnELIK4KC4eD2wks/g4rDV4MlthTtcU8UrWh76HJgLzosZlqC5Q20tn0hNV/vBh7/+hbe2nDyyNr7Jr77ha9NvW/2/GI6yaUAwxAY3UPhDpL6+GcKYFvQUK1/lyA1X4hIAdLR0/ZkUE7SH55IGLXAByROLNAfeZqUstixtzacRAis9b86vP7RezeFNvz2CFbU7jIUpKG97+hJ/3qG8J2W1KOLVFfQvO+zqukkmZoIra2aL2ND3yG5CVYxsMqMWR3enYtyJQoBlq0zY9Pj67fEyerWscyy49yplvLHH3lvWmtz2L7nK5MNw9C5TA1DKOAMA6tuq+NmElEjop+yFonHsYmOskPxdEU76rykfxTKsLFFjzTghtKp4S7+mtciulK3J9TmEKmfDV6MT9D7HEYn2D8SWP7/ARySnogVnDj9AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE3LTA5LTEzVDIxOjMzOjU3LTA0OjAw9dwHaQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNy0wOS0xM1QyMTozMzo1Ny0wNDowMISBv9UAAAAASUVORK5CYII=";
        }
    };
    SigninPage.prototype.doLogin = function () {
        var _this = this;
        if (this.loginData.username == "" || this.loginData.password == "") {
            this.presentToast('Mohon isi semua form');
        }
        else {
            this.authService.login(this.loginData).then(function (result) {
                _this.data = result;
                localStorage.setItem('bsw-foto', _this.site_url + _this.data.foto);
                localStorage.setItem('bsw-nama-lengkap', _this.data.nama_lengkap);
                localStorage.setItem('bsw-token', _this.data.token);
                localStorage.setItem('bsw-riwayat-golongan', _this.data.riwayat_golongan);
                localStorage.setItem('bsw-riwayat-unit-kerja', _this.data.riwayat_unit_kerja);
                localStorage.setItem('bsw-riwayat-jabatan', _this.data.riwayat_jabatan);
                localStorage.setItem('bsw-groups', _this.data.groups);
                localStorage.setItem('bsw-username', _this.loginData.username);
                localStorage.setItem('bsw-password', _this.loginData.password);
                _this.ShowLoadingPage();
            }, function (err) {
                _this.presentToast("Username atau Password salah");
            });
        }
    };
    SigninPage.prototype.checkingTokenToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    SigninPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    SigninPage.prototype.ShowLoadingPage = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "<ion-spinner name=\"bubbles\"></ion-spinner>"
        });
        loading.present();
        setTimeout(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
            _this.menu.swipeEnable(true, 'myMenu');
            loading.dismiss();
        }, 4000);
    };
    return SigninPage;
}());
SigninPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-signin',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/signin/signin.html"*/'<!--\n  Generated template for the SigninPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n</ion-header>\n\n\n<ion-content scroll="true">\n	<div id="formContent">\n		<!-- Tabs Titles -->\n		<h2 class="active">Sign In</h2>\n		<!-- Icon -->\n		<div class="fadeIn first">\n		<img src="{{ img_base64 }}" id="icon" alt="User Icon" />\n		</div>\n	\n		<!-- Login Form -->\n		<form (submit)="doLogin()">\n		<input id="login" class="fadeIn second" type="text" [(ngModel)]="loginData.username" name="username" placeholder="Username">\n		<input id="password" class="fadeIn third" type="password" [(ngModel)]="loginData.password" name="password" type="password" placeholder="Password">\n		<input type="submit" class="fadeIn fourth" value="Log In">\n		</form>\n	\n	</div>\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/signin/signin.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */], __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */], __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */]])
], SigninPage);

//# sourceMappingURL=signin.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
//let base_url = "http://192.168.200.88:8899/api-mobile/login/auth/";
var AuthServiceProvider = (function () {
    function AuthServiceProvider(http) {
        this.http = http;
        this.base_url = "http://simita.aitc.co.id/api-mobile/login/auth/";
        console.log('Hello AuthServiceProvider Provider');
    }
    AuthServiceProvider.prototype.login = function (credentials) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            _this.http.post(_this.base_url, JSON.stringify(credentials), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    AuthServiceProvider.prototype.logout = function () {
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('X-Auth-Token', localStorage.getItem('bsw-token'));
            localStorage.clear();
        });
    };
    return AuthServiceProvider;
}());
AuthServiceProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], AuthServiceProvider);

//# sourceMappingURL=auth-service.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiwayatJabatanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_detail_riwayat_jabatan_pegawai_detail_riwayat_jabatan_pegawai__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_riwayat_jabatan_add_riwayat_jabatan_add__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the RiwayatJabatanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RiwayatJabatanPage = (function () {
    function RiwayatJabatanPage(navCtrl, navParams, dataService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.loadingCtrl = loadingCtrl;
        this.form_operator = { operator_cari_pegawai: '' };
        this.form_verifikator = { verifikator_cari_pegawai: '' };
        this.foto = localStorage.getItem('bsw-foto');
        this.getDataRiwayatJabatan();
        this.groups = localStorage.getItem('bsw-groups');
    }
    RiwayatJabatanPage.prototype.doRefresh = function (refresh) {
        this.getDataRiwayatJabatan();
        refresh.complete();
    };
    RiwayatJabatanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatJabatanPage');
    };
    RiwayatJabatanPage.prototype.getDataRiwayatJabatan = function () {
        var _this = this;
        this.showLoader();
        this.dataService.getListRiwayatJabatan().subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_riwayat_jabatan = data;
            _this.lists_riwayat_jabatan = data.results;
        });
    };
    RiwayatJabatanPage.prototype.operator_getDataRiwayatUnitKerja = function () {
        var _this = this;
        var param;
        if (this.form_operator.operator_cari_pegawai == "" || this.form_operator.operator_cari_pegawai == null) {
            this.getDataRiwayatJabatan();
        }
        else {
            param = this.form_operator.operator_cari_pegawai;
            this.showLoader();
            this.dataService.non_pegawai_getDetailRiwayatJabatan(param).subscribe(function (data) {
                _this.loading.dismiss();
                //this.lists_riwayat_unit_kerja = data;
                _this.lists_riwayat_jabatan = data.results;
                console.log(data.results);
            });
        }
    };
    RiwayatJabatanPage.prototype.verifikator_getDataRiwayatUnitKerja = function () {
        var _this = this;
        var param;
        if (this.form_verifikator.verifikator_cari_pegawai == "" || this.form_verifikator.verifikator_cari_pegawai == null) {
            this.getDataRiwayatJabatan();
        }
        else {
            param = this.form_verifikator.verifikator_cari_pegawai;
            this.showLoader();
            this.dataService.non_pegawai_getDetailRiwayatJabatan(param).subscribe(function (data) {
                _this.loading.dismiss();
                //this.lists_riwayat_unit_kerja = data;
                _this.lists_riwayat_jabatan = data.results;
                console.log(data.results);
            });
        }
    };
    RiwayatJabatanPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    RiwayatJabatanPage.prototype.GoToDetailRiwayatJabatanPegawai = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_detail_riwayat_jabatan_pegawai_detail_riwayat_jabatan_pegawai__["a" /* DetailRiwayatJabatanPegawaiPage */], { id: id });
        console.log("ID Detail Riwayat : " + id);
    };
    RiwayatJabatanPage.prototype.GoToAddRiwayatJabatan = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_riwayat_jabatan_add_riwayat_jabatan_add__["a" /* RiwayatJabatanAddPage */]);
    };
    RiwayatJabatanPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Mengambil data dari server...'
        });
        this.loading.present();
    };
    return RiwayatJabatanPage;
}());
RiwayatJabatanPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-riwayat-jabatan',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-jabatan/riwayat-jabatan.html"*/'<!--\n  Generated template for the RiwayatJabatanPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n  	<button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Riwayat Jabatan</ion-title>\n    <ion-buttons end style="width: 10%">\n      <a (click)="GoToDashboard()">\n      <img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n    </ion-buttons>   \n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n<ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingText="Pull to refresh..."\n      refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n<ion-fab right bottom  *ngIf=" groups == \'Operator\' ">\n    <button ion-fab color="primary" (click)="GoToAddRiwayatJabatan()"><ion-icon name="add"></ion-icon></button>\n</ion-fab>\n\n<div *ngIf=" groups == \'Operator\' ">\n  <form (submit)="operator_getDataRiwayatUnitKerja()">\n    <ion-searchbar placeholder="Cari Pegawai...." [(ngModel)]="form_operator.operator_cari_pegawai" name="operator_cari_pegawai"></ion-searchbar>\n  </form>\n</div>\n<div *ngIf=" groups == \'Verifikator\' ">\n  <form (submit)="verifikator_getDataRiwayatUnitKerja()">\n    <ion-searchbar placeholder="Cari Pegawai...." [(ngModel)]="form_verifikator.verifikator_cari_pegawai" name="verifikator_cari_pegawai"></ion-searchbar>\n  </form>\n</div>\n\n<ion-list no-lines>\n<ion-item-sliding *ngFor="let list_riwayat_jabatan of lists_riwayat_jabatan">\n     <ion-item style="color: #f53d3d" *ngIf=" list_riwayat_jabatan.status == 6">\n      <ion-avatar item-start>\n        <img src="{{ list_riwayat_jabatan.foto_url }}">\n      </ion-avatar>\n      <b>Pegawai</b> : {{ list_riwayat_jabatan.pegawai }} <br>\n      <b>{{ list_riwayat_jabatan.username }}</b> <br>\n      <b>Nama Unit Kerja</b> : {{ list_riwayat_jabatan.jabatan }} <br>\n      <b>TMT</b> : {{ list_riwayat_jabatan.tmt }}\n    </ion-item>\n    <ion-item-options side="right">\n    <button ion-button small color="danger" (click)="GoToDetailRiwayatJabatanPegawai(list_riwayat_jabatan.id)">\n    <ion-icon name="text"></ion-icon>Detail</button>\n    </ion-item-options>\n  </ion-item-sliding>\n\n<ion-item-sliding *ngFor="let list_riwayat_jabatan of lists_riwayat_jabatan">\n    <ion-item  style="color: #4caf50;" *ngIf=" list_riwayat_jabatan.status == 2">\n      <ion-avatar item-start>\n        <img src="{{ list_riwayat_jabatan.foto_url }}">\n      </ion-avatar>\n      <b>Pegawai</b> : {{ list_riwayat_jabatan.pegawai }} <br>\n      <b>{{ list_riwayat_jabatan.username }}</b> <br>\n      <b>Nama Unit Kerja</b> : {{ list_riwayat_jabatan.jabatan }} <br>\n      <b>TMT</b> : {{ list_riwayat_jabatan.tmt }}\n    </ion-item>\n    <ion-item-options side="right">\n    <button ion-button small color="primary" (click)="GoToDetailRiwayatJabatanPegawai(list_riwayat_jabatan.id)">\n    <ion-icon name="text"></ion-icon>Detail</button>\n    </ion-item-options>\n  </ion-item-sliding>\n\n<ion-item-sliding *ngFor="let list_riwayat_jabatan of lists_riwayat_jabatan">\n    <ion-item style="color:black;" *ngIf=" list_riwayat_jabatan.status == 1">\n      <ion-avatar item-start>\n        <img src="{{ list_riwayat_jabatan.foto_url }}">\n      </ion-avatar>\n      <b>Pegawai</b> : {{ list_riwayat_jabatan.pegawai }} <br>\n      <b>{{ list_riwayat_jabatan.username }}</b> <br>\n      <b>Nama Unit Kerja</b> : {{ list_riwayat_jabatan.jabatan }} <br>\n      <b>TMT</b> : {{ list_riwayat_jabatan.tmt }}\n    </ion-item>\n    <ion-item-options side="right">\n    <button ion-button small color="secondary" (click)="GoToDetailRiwayatJabatanPegawai(list_riwayat_jabatan.id)">\n    <ion-icon name="text"></ion-icon>Detail</button>\n    </ion-item-options>\n  </ion-item-sliding>\n</ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-jabatan/riwayat-jabatan.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], RiwayatJabatanPage);

//# sourceMappingURL=riwayat-jabatan.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiwayatKenaikanPangkatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_detail_riwayat_kenaikan_pangkat_detail_riwayat_kenaikan_pangkat__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RiwayatKenaikanPangkatPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RiwayatKenaikanPangkatPage = (function () {
    function RiwayatKenaikanPangkatPage(navCtrl, navParams, dataService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.loadingCtrl = loadingCtrl;
        this.form_operator = { operator_cari_pegawai: '' };
        this.form_verifikator = { verifikator_cari_pegawai: '' };
        this.foto = localStorage.getItem('bsw-foto');
        this.groups = localStorage.getItem('bsw-groups');
        this.getDataRiwayatKenaikanPangkat();
    }
    RiwayatKenaikanPangkatPage.prototype.doRefresh = function (refresh) {
        this.getDataRiwayatKenaikanPangkat();
        refresh.complete();
    };
    RiwayatKenaikanPangkatPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatKenaikanPangkatPage');
    };
    RiwayatKenaikanPangkatPage.prototype.getDataRiwayatKenaikanPangkat = function () {
        var _this = this;
        this.showLoader();
        this.dataService.getListRiwayatKenaikanPangkat().subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_riwayat_kenaikan_pangkat = data;
            _this.lists_riwayat_kenaikan_pangkat = data.results;
        });
    };
    RiwayatKenaikanPangkatPage.prototype.operator_getDataRiwayatUnitKerja = function () {
        var _this = this;
        var param;
        if (this.form_operator.operator_cari_pegawai == "" || this.form_operator.operator_cari_pegawai == null) {
            this.getDataRiwayatKenaikanPangkat();
        }
        else {
            param = this.form_operator.operator_cari_pegawai;
            this.showLoader();
            this.dataService.non_pegawai_getDetailRiwayatKenaikanPangkat(param).subscribe(function (data) {
                _this.loading.dismiss();
                //this.lists_riwayat_unit_kerja = data;
                _this.lists_riwayat_kenaikan_pangkat = data.results;
                console.log(data.results);
            });
        }
    };
    RiwayatKenaikanPangkatPage.prototype.verifikator_getDataRiwayatUnitKerja = function () {
        var _this = this;
        var param;
        if (this.form_verifikator.verifikator_cari_pegawai == "" || this.form_verifikator.verifikator_cari_pegawai == null) {
            this.getDataRiwayatKenaikanPangkat();
        }
        else {
            param = this.form_verifikator.verifikator_cari_pegawai;
            this.showLoader();
            this.dataService.non_pegawai_getDetailRiwayatKenaikanPangkat(param).subscribe(function (data) {
                _this.loading.dismiss();
                //this.lists_riwayat_unit_kerja = data;
                _this.lists_riwayat_kenaikan_pangkat = data.results;
                console.log(data.results);
            });
        }
    };
    RiwayatKenaikanPangkatPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    RiwayatKenaikanPangkatPage.prototype.GoToDetailRiwayatKenaikanPangkat = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_detail_riwayat_kenaikan_pangkat_detail_riwayat_kenaikan_pangkat__["a" /* DetailRiwayatKenaikanPangkatPage */], { id: id });
    };
    RiwayatKenaikanPangkatPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Mengambil data dari server...'
        });
        this.loading.present();
    };
    return RiwayatKenaikanPangkatPage;
}());
RiwayatKenaikanPangkatPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-riwayat-kenaikan-pangkat',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-kenaikan-pangkat/riwayat-kenaikan-pangkat.html"*/'<!--\n  Generated template for the RiwayatKenaikanPangkatPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n  	<button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title style="font-size: 13px;">Riwayat Kenaikan Pangkat</ion-title>\n    <ion-buttons end style="width: 10%">\n      <a (click)="GoToDashboard()"><img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n    </ion-buttons>   \n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n<ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingText="Pull to refresh..."\n      refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n<div style="padding-bottom: auto;" *ngIf=" groups == \'Operator\' ">\n  <form (submit)="operator_getDataRiwayatUnitKerja()">\n    <ion-searchbar placeholder="Cari Pegawai...." [(ngModel)]="form_operator.operator_cari_pegawai" name="operator_cari_pegawai"></ion-searchbar>\n  </form>\n</div>\n<div *ngIf=" groups == \'Verifikator\' ">\n  <form (submit)="verifikator_getDataRiwayatUnitKerja()">\n    <ion-searchbar placeholder="Cari Pegawai...." [(ngModel)]="form_verifikator.verifikator_cari_pegawai" name="verifikator_cari_pegawai"></ion-searchbar>\n  </form>\n</div>\n\n<ion-list no-lines>\n<ion-item-sliding *ngFor="let list_riwayat_kenaikan_pangkat of lists_riwayat_kenaikan_pangkat">\n    <ion-item style="color: #f53d3d" *ngIf=" list_riwayat_kenaikan_pangkat.status == 6">\n      <ion-avatar item-start>\n        <img src="{{ list_riwayat_kenaikan_pangkat.foto_url }}">\n      </ion-avatar>\n    <b>Pegawai</b> : {{ list_riwayat_kenaikan_pangkat.pegawai }} <br>\n    <b>{{ list_riwayat_kenaikan_pangkat.username }}</b> <br>\n    <b>Nama KP</b> : {{ list_riwayat_kenaikan_pangkat.jabatan }} <br>\n    <b>TMT</b> : {{ list_riwayat_kenaikan_pangkat.tmt }}\n    </ion-item>\n    <ion-item-options side="right">\n    <button ion-button small color="danger" (click)="GoToDetailRiwayatKenaikanPangkat(list_riwayat_kenaikan_pangkat.id)">\n    <ion-icon name="text"></ion-icon>Detail</button>\n    </ion-item-options>\n  </ion-item-sliding>\n  \n<ion-item-sliding *ngFor="let list_riwayat_kenaikan_pangkat of lists_riwayat_kenaikan_pangkat">\n    <ion-item style="color: #4caf50" *ngIf=" list_riwayat_kenaikan_pangkat.status == 2">\n      <ion-avatar item-start>\n        <img src="{{ list_riwayat_kenaikan_pangkat.foto_url }}">\n      </ion-avatar>\n    <b>Pegawai</b> : {{ list_riwayat_kenaikan_pangkat.pegawai }} <br>\n    <b>{{ list_riwayat_kenaikan_pangkat.username }}</b> <br>\n    <b>Nama KP</b> : {{ list_riwayat_kenaikan_pangkat.jabatan }} <br>\n    <b>TMT</b> : {{ list_riwayat_kenaikan_pangkat.tmt }}\n    </ion-item>\n    <ion-item-options side="right">\n    <button ion-button small color="primary" (click)="GoToDetailRiwayatKenaikanPangkat(list_riwayat_kenaikan_pangkat.id)">\n    <ion-icon name="text"></ion-icon>Detail</button>\n    </ion-item-options>\n  </ion-item-sliding>\n\n<ion-item-sliding *ngFor="let list_riwayat_kenaikan_pangkat of lists_riwayat_kenaikan_pangkat">\n    <ion-item style="color: black;" *ngIf=" list_riwayat_kenaikan_pangkat.status == 1">\n      <ion-avatar item-start>\n        <img src="{{ list_riwayat_kenaikan_pangkat.foto_url }}">\n      </ion-avatar>\n    <b>Pegawai</b> : {{ list_riwayat_kenaikan_pangkat.pegawai }} <br>\n    <b>{{ list_riwayat_kenaikan_pangkat.username }}</b> <br>\n    <b>Nama KP</b> : {{ list_riwayat_kenaikan_pangkat.jabatan }} <br>\n    <b>TMT</b> : {{ list_riwayat_kenaikan_pangkat.tmt }}\n    </ion-item>\n    <ion-item-options side="right">\n    <button ion-button small color="secondary" (click)="GoToDetailRiwayatKenaikanPangkat(list_riwayat_kenaikan_pangkat.id)">\n    <ion-icon name="text"></ion-icon>Detail</button>\n    </ion-item-options>\n  </ion-item-sliding>\n</ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-kenaikan-pangkat/riwayat-kenaikan-pangkat.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], RiwayatKenaikanPangkatPage);

//# sourceMappingURL=riwayat-kenaikan-pangkat.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiwayatPendidikanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_detail_riwayat_pendidikan_detail_riwayat_pendidikan__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RiwayatPendidikanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RiwayatPendidikanPage = (function () {
    function RiwayatPendidikanPage(navCtrl, navParams, dataService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.loadingCtrl = loadingCtrl;
        this.form_operator = { operator_cari_pegawai: '' };
        this.form_verifikator = { verifikator_cari_pegawai: '' };
        this.foto = localStorage.getItem('bsw-foto');
        this.getDataRiwayatPendidikan();
        this.groups = localStorage.getItem('bsw-groups');
    }
    RiwayatPendidikanPage.prototype.doRefresh = function (refresh) {
        this.getDataRiwayatPendidikan();
        refresh.complete();
    };
    RiwayatPendidikanPage.prototype.getDataRiwayatPendidikan = function () {
        var _this = this;
        this.showLoader();
        this.dataService.getListRiwayatPendidikan().subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_riwayat_kenaikan_pangkat = data;
            _this.lists_riwayat_pendidikan = data.results;
        });
    };
    RiwayatPendidikanPage.prototype.operator_getDataRiwayatUnitKerja = function () {
        var _this = this;
        var param;
        if (this.form_operator.operator_cari_pegawai == "" || this.form_operator.operator_cari_pegawai == null) {
            this.getDataRiwayatPendidikan();
        }
        else {
            param = this.form_operator.operator_cari_pegawai;
            this.showLoader();
            this.dataService.non_pegawai_getDetailRiwayatRiwayatPendidikan(param).subscribe(function (data) {
                _this.loading.dismiss();
                //this.lists_riwayat_unit_kerja = data;
                _this.lists_riwayat_pendidikan = data.results;
                console.log(data.results);
            });
        }
    };
    RiwayatPendidikanPage.prototype.verifikator_getDataRiwayatUnitKerja = function () {
        var _this = this;
        var param;
        if (this.form_verifikator.verifikator_cari_pegawai == "" || this.form_verifikator.verifikator_cari_pegawai == null) {
            this.getDataRiwayatPendidikan();
        }
        else {
            param = this.form_verifikator.verifikator_cari_pegawai;
            this.showLoader();
            this.dataService.non_pegawai_getDetailRiwayatRiwayatPendidikan(param).subscribe(function (data) {
                _this.loading.dismiss();
                //this.lists_riwayat_unit_kerja = data;
                _this.lists_riwayat_pendidikan = data.results;
                console.log(data.results);
            });
        }
    };
    RiwayatPendidikanPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Mengambil data dari server...'
        });
        this.loading.present();
    };
    RiwayatPendidikanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatPendidikanPage');
    };
    RiwayatPendidikanPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    RiwayatPendidikanPage.prototype.GoToDetailRiwayatPendidikan = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_detail_riwayat_pendidikan_detail_riwayat_pendidikan__["a" /* DetailRiwayatPendidikanPage */], { id: id });
    };
    return RiwayatPendidikanPage;
}());
RiwayatPendidikanPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-riwayat-pendidikan',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-pendidikan/riwayat-pendidikan.html"*/'<!--\n  Generated template for the RiwayatPendidikanPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n  	<button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Riwayat Pendidikan</ion-title>\n    <ion-buttons end style="width: 10%">\n      <a (click)="GoToDashboard()">\n      <img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n    </ion-buttons>   \n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n<ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingText="Pull to refresh..."\n      refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n\n<div *ngIf=" groups == \'Operator\' ">\n  <form (submit)="operator_getDataRiwayatUnitKerja()">\n    <ion-searchbar placeholder="Cari Pegawai...." [(ngModel)]="form_operator.operator_cari_pegawai" name="operator_cari_pegawai"></ion-searchbar>\n  </form>\n</div>\n<div *ngIf=" groups == \'Verifikator\' ">\n  <form (submit)="verifikator_getDataRiwayatUnitKerja()">\n    <ion-searchbar placeholder="Cari Pegawai...." [(ngModel)]="form_verifikator.verifikator_cari_pegawai" name="verifikator_cari_pegawai"></ion-searchbar>\n  </form>\n</div>\n\n<ion-list no-lines>\n    <ion-item-sliding *ngFor="let list_riwayat_pendidikan of lists_riwayat_pendidikan">\n     <ion-item style="color: #f53d3d" *ngIf=" list_riwayat_pendidikan.status == 6">\n      <ion-avatar item-start>\n        <img src="{{ list_riwayat_pendidikan.foto_url }}">\n      </ion-avatar>\n      <b>Pegawai</b> : {{ list_riwayat_pendidikan.pegawai }} <br>\n      <b>{{ list_riwayat_pendidikan.username }}</b> <br>\n      <b>Jenjang Pendidikan</b> : {{ list_riwayat_pendidikan.jenjang_pendidikan }} <br>\n      <b>Tanggal Ijazah</b> : {{ list_riwayat_pendidikan.tanggal_ijazah }}\n    </ion-item>\n    <ion-item-options side="right">\n    <button ion-button small color="danger" (click)="GoToDetailRiwayatPendidikan(list_riwayat_pendidikan.id)">\n    <ion-icon name="text"></ion-icon>Detail</button>\n    </ion-item-options>\n  </ion-item-sliding>\n\n  <ion-item-sliding *ngFor="let list_riwayat_pendidikan of lists_riwayat_pendidikan">\n     <ion-item style="color: #4caf50" *ngIf=" list_riwayat_pendidikan.status == 2">\n      <ion-avatar item-start>\n        <img src="{{ list_riwayat_pendidikan.foto_url }}">\n      </ion-avatar>\n      <b>Pegawai</b> : {{ list_riwayat_pendidikan.pegawai }} <br>\n      <b>{{ list_riwayat_pendidikan.username }}</b> <br>\n      <b>Jenjang Pendidikan</b> : {{ list_riwayat_pendidikan.jenjang_pendidikan }} <br>\n      <b>Tanggal Ijazah</b> : {{ list_riwayat_pendidikan.tanggal_ijazah }}\n    </ion-item>\n    <ion-item-options side="right">\n    <button ion-button small color="primary" (click)="GoToDetailRiwayatPendidikan(list_riwayat_pendidikan.id)">\n    <ion-icon name="text"></ion-icon>Detail</button>\n    </ion-item-options>\n  </ion-item-sliding>\n\n  <ion-item-sliding *ngFor="let list_riwayat_pendidikan of lists_riwayat_pendidikan">\n     <ion-item style="color: black;" *ngIf=" list_riwayat_pendidikan.status == 1">\n      <ion-avatar item-start>\n        <img src="{{ list_riwayat_pendidikan.foto_url }}">\n      </ion-avatar>\n      <b>Pegawai</b> : {{ list_riwayat_pendidikan.pegawai }} <br>\n      <b>{{ list_riwayat_pendidikan.username }}</b> <br>\n      <b>Jenjang Pendidikan</b> : {{ list_riwayat_pendidikan.jenjang_pendidikan }} <br>\n      <b>Tanggal Ijazah</b> : {{ list_riwayat_pendidikan.tanggal_ijazah }}\n    </ion-item>\n    <ion-item-options side="right">\n    <button ion-button small color="secondary" (click)="GoToDetailRiwayatPendidikan(list_riwayat_pendidikan.id)">\n    <ion-icon name="text"></ion-icon>Detail</button>\n    </ion-item-options>\n  </ion-item-sliding>\n</ion-list>\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-pendidikan/riwayat-pendidikan.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], RiwayatPendidikanPage);

//# sourceMappingURL=riwayat-pendidikan.js.map

/***/ }),

/***/ 116:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 116;

/***/ }),

/***/ 13:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_signin_signin__ = __webpack_require__(104);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the DashboardPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var DashboardPage = DashboardPage_1 = (function () {
    function DashboardPage(navCtrl, http, navParams, dataService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.navParams = navParams;
        this.dataService = dataService;
        this.loadingCtrl = loadingCtrl;
        this.foto = localStorage.getItem('bsw-foto');
        this.getListPegawaiDetail();
    }
    DashboardPage.prototype.doRefresh = function (refresh) {
        this.getListPegawaiDetail();
        refresh.complete();
    };
    DashboardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DashboardPage');
        console.log('Your Token : ' + localStorage.getItem('bsw-token'));
        if (localStorage.getItem('bsw-token')) {
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_signin_signin__["a" /* SigninPage */]);
            console.log('You need to login');
        }
    };
    DashboardPage.prototype.getListPegawaiDetail = function () {
        var _this = this;
        this.dataService.getListPegawaiDetail().subscribe(function (data) {
            _this.lists_pegawai_detail = data.results;
        });
    };
    DashboardPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(DashboardPage_1);
    };
    DashboardPage.prototype.ShowLoadingPage = function () {
        var loading = this.loadingCtrl.create({
            content: "<ion-spinner name=\"bubbles\"></ion-spinner>"
        });
        loading.present();
        setTimeout(function () {
            loading.dismiss();
        }, 1000);
    };
    return DashboardPage;
}());
DashboardPage = DashboardPage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-dashboard',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/dashboard/dashboard.html"*/'<!--\n  Generated template for the DashboardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n     <button ion-button menuToggle >\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Dashboard</ion-title>\n\n    <ion-buttons end style="width: 10%">\n      <a (click)="GoToDashboard()"><img style="border-radius: 50%" src="{{ foto }}" width="100%" height="100%"></a>\n    </ion-buttons>    \n</ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingText="Pull to refresh..."\n      refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-card class="fadeIn first">\n    <ion-card-header text-center no-padding padding-top>\n      Profil Pegawai\n    </ion-card-header>\n    <hr>\n\n    <ion-grid *ngFor="let list_pegawai_detail of lists_pegawai_detail">\n\n      <ion-row>\n        <ion-col col-6>Username\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.username }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>Nama Lengkap\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.nama_lengkap }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>Tempat / Tanggal Lahir\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.tempat_lahir}} / {{ list_pegawai_detail.tanggal_lahir }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>Alamat\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.alamat }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>Jenis Kelamin\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.jenis_kelamin }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>Agama\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.agama }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>Golongan Darah\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.golongan_darah }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>Nomor Taspen\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.nomor_taspen }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>Nomor Karpeg\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.nomor_karpeg }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>BPJS\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.bpjs }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>NPWP\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.npwp }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>Karis Karsu\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.nomor_karis_karsu }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>Email\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.email }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>Nomor Telepon\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.telephone }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>HP\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.nomor_hp }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col col-6>Keterangan\n        </ion-col>\n        <ion-col col-6 text-right>{{ list_pegawai_detail.keterangan }}\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/dashboard/dashboard.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], DashboardPage);

var DashboardPage_1;
//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 158:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 158;

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailRiwayatJabatanPegawaiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_riwayat_jabatan_riwayat_jabatan__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the DetailRiwayatJabatanPegawaiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var DetailRiwayatJabatanPegawaiPage = (function () {
    function DetailRiwayatJabatanPegawaiPage(navCtrl, navParams, toastCtrl, loadingCtrl, dataService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.dataService = dataService;
        this.post_data = { status_to_verifikator: '', status_verifikasi: '' };
        this.error = { data: '' };
        this.foto = localStorage.getItem('bsw-foto');
        console.log("Parameter : " + navParams.get('id'));
        this.getDetailRiwayatJabatan(navParams.get('id'));
        this.groups = localStorage.getItem('bsw-groups');
    }
    DetailRiwayatJabatanPegawaiPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailRiwayatJabatanPegawaiPage');
    };
    DetailRiwayatJabatanPegawaiPage.prototype.getDetailRiwayatJabatan = function (id) {
        var _this = this;
        this.showLoader("Mengambil data dari server...");
        this.dataService.getDetailRiwayatJabatan(id).subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_detail_riwayat_jabatan = data;
            _this.lists_detail_riwayat_jabatan = data.results;
        });
    };
    DetailRiwayatJabatanPegawaiPage.prototype.showLoader = function (msg) {
        this.loading = this.loadingCtrl.create({
            content: msg
        });
        this.loading.present();
    };
    DetailRiwayatJabatanPegawaiPage.prototype.doSubmitVerifikasi = function (id) {
        var data = {
            status: 1
        };
        this.postData(id, data);
        console.log(data);
    };
    DetailRiwayatJabatanPegawaiPage.prototype.doSubmitToVerifikator = function (id) {
        var data = {
            status: 2
        };
        this.postData(id, data);
        console.log(data);
    };
    DetailRiwayatJabatanPegawaiPage.prototype.postData = function (id, data) {
        var _this = this;
        this.showLoader("Mohon tunggu sebentar...");
        this.dataService.postStatusDetailJabatan(id, data).then(function (result) {
            _this.loading.dismiss();
            _this.data_result_post = result;
            {
                var toast = _this.toastCtrl.create({
                    message: "Berhasil mengirim data...",
                    duration: 3000,
                    position: 'top',
                    dismissOnPageChange: true
                });
                toast.onDidDismiss(function () {
                    console.log('Berhasil');
                });
                toast.present();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_riwayat_jabatan_riwayat_jabatan__["a" /* RiwayatJabatanPage */]);
            }
        }, function (err) {
            _this.loading.dismiss();
            _this.presentToast(err);
        });
    };
    DetailRiwayatJabatanPegawaiPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    DetailRiwayatJabatanPegawaiPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    return DetailRiwayatJabatanPegawaiPage;
}());
DetailRiwayatJabatanPegawaiPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-detail-riwayat-jabatan-pegawai',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/detail-riwayat-jabatan-pegawai/detail-riwayat-jabatan-pegawai.html"*/'<!--\n  Generated template for the DetailRiwayatJabatanPegawaiPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar color="primary">\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>Detail Riwayat</ion-title>\n		<ion-buttons end style="width: 10%">\n			<a (click)="GoToDashboard()">\n			<img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n		</ion-buttons>   \n	</ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding>\n	<ion-card>\n		<ion-grid col-12>\n			<ion-row>\n				<ion-col col-12 text-center>Detail Jabatan Pegawai</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card>\n\n	<ion-card *ngFor="let list_detail_riwayat_jabatan of lists_detail_riwayat_jabatan">\n\n		<ion-grid>\n			<ion-row>\n				<ion-col col-6>Nama Jabatan\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_jabatan.jabatan }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Jenis Jabatan\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_jabatan.jenis_jabatan }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Apakah PLT ?\n				</ion-col>\n				<ion-col col-6 text-left *ngIf=" list_detail_riwayat_jabatan.plt == false" >Bukan\n				</ion-col>\n				<ion-col col-6 text-left *ngIf=" list_detail_riwayat_jabatan.plt == true" >Ya\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Eselon\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_jabatan.eselon_jabatan }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>TMT\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_jabatan.tmt }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Tanggal SK\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_jabatan.tgl_sk}}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Nomor SK\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_jabatan.no_sk }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Satuan Kerja Jabatan\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_jabatan.unit_kerja}}\n				</ion-col>\n			</ion-row>\n\n\n		</ion-grid>\n		<div id="button-content">\n		<div *ngIf=" groups == \'Operator\' ">\n			<input type="submit" (click)="doSubmitToVerifikator(list_detail_riwayat_jabatan.id)" value="Kirim ke Verifikator" class="kirim_ke_verifikator" *ngIf=" list_detail_riwayat_jabatan.status == 6">\n		</div>\n		<div *ngIf=" groups == \'Verifikator\' ">\n			<input type="submit" (click)="doSubmitVerifikasi(list_detail_riwayat_jabatan.id)" value="Verifikasi" class="kirim_verifikasi" *ngIf=" list_detail_riwayat_jabatan.status == 2">\n		</div>\n		<!-- <input type="submit" value="Download Berkas" class="download" (click)=downloadFile(list_detail_riwayat_jabatan.berkas_url)> -->\n\n			<!-- <input type="button" value="Kirim ke Verifikator" class="kirim_ke_verifikator">\n			<input type="button" value="Verifikasi" class="kirim_verifikasi">\n			<input type="button" value="Download Berkas" class="download"> -->\n		</div>\n		<ion-input type="text" [(ngModel)]="error.data" name="data"></ion-input>\n	</ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/detail-riwayat-jabatan-pegawai/detail-riwayat-jabatan-pegawai.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */]])
], DetailRiwayatJabatanPegawaiPage);

//# sourceMappingURL=detail-riwayat-jabatan-pegawai.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiwayatJabatanAddPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the RiwayatJabatanAddPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RiwayatJabatanAddPage = (function () {
    function RiwayatJabatanAddPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.jabatan_data = { pegawai: '', nama_pegawai: '', unit_kerja: '', value_unit_kerja: '',
            jabatan: '', jenis_jabatan: '', eselon_jabatan: '', plt: '', angka_kredit_utama: '', tmt: '', keterangan: '', surat: '', nama_lampiran: '', lampiran: '', nama_pejabat_penandatangan: '',
            jabatan_pejabat_penandatangan: '', tanggal_surat: '' };
    }
    RiwayatJabatanAddPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatJabatanAddPage');
    };
    return RiwayatJabatanAddPage;
}());
RiwayatJabatanAddPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-riwayat-jabatan-add',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-jabatan-add/riwayat-jabatan-add.html"*/'<!--\n  Generated template for the RiwayatJabatanAddPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  	<ion-navbar color="primary">\n 		<ion-title>Riwayat Jabatan Baru</ion-title>\n 	</ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<form (submit)="doSubmitJabatan()">\n 		<ion-item-group>\n 		<ion-item-divider color="light">Pegawai</ion-item-divider>\n 		<ion-item>\n 			<ion-label style="color: black;">NIP Pegawai</ion-label>\n 			<ion-input type="text" [(ngModel)]="jabatan_data.nama_pegawai" name="nama_pegawai"\n 			 (click)="openModalCariPegawai()" disabled></ion-input>\n 		</ion-item>\n 			<ion-input type="hidden" [(ngModel)]="jabatan_data.pegawai" name="pegawai"></ion-input>\n\n 		<ion-item>\n 			<ion-label style="color: black;">Unit Kerja</ion-label>\n 			<ion-input type="text" [(ngModel)]="jabatan_data.unit_kerja" name="unit_kerja" \n 			(click)="openModalCariUnitKerja()" disabled></ion-input>\n 		</ion-item>\n 			<ion-input type="hidden" [(ngModel)]="jabatan_data.value_unit_kerja" name="value_unit_kerja" ></ion-input>\n\n 		<ion-item>\n 			<ion-label>Jabatan</ion-label>\n 			<ion-select [(ngModel)]="jabatan_data.jabatan" name="jabatan">\n 				<ion-option *ngFor="let item of items" value="{{ item.id }}">{{ item.jabatan }}</ion-option>\n 			</ion-select>\n 		</ion-item>\n\n 		<ion-item>\n 			<ion-label>Jenis Jabatan</ion-label>\n 			<ion-select [(ngModel)]="jabatan_data.jenis_jabatan" name="jenis_jabatan">\n 				<ion-option *ngFor="let item of items" value="{{ item.id }}">{{ item.jabatan }}</ion-option>\n 			</ion-select>\n 		</ion-item>\n\n 		<ion-item>\n 			<ion-label>Eselon Jabatan</ion-label>\n 			<ion-select [(ngModel)]="jabatan_data.eselon_jabatan" name="eselon_jabatan">\n 				<ion-option *ngFor="let item of items" value="{{ item.id }}">{{ item.jabatan }}</ion-option>\n 			</ion-select>\n 		</ion-item>\n\n 		<ion-item>\n		  <ion-label>Apakah PLT ?</ion-label>\n		  <ion-checkbox [(ngModel)]="jabatan_data.plt" name="plt" color="primary" checked="true"></ion-checkbox>\n		</ion-item>\n\n		<ion-item>\n 			<ion-input [(ngModel)]="jabatan_data.angka_kredit_utama"  type="text" placeholder="Angka Kredit Utama" name="angka_kredit_utama"></ion-input>\n 		</ion-item>\n\n 		<ion-item>\n 			<ion-label>TMT</ion-label>\n 			<ion-datetime displayFormat="MMMM, DD YYYY" max="2030"  [(ngModel)]="jabatan_data.tmt" name="tmt">\n 			</ion-datetime>\n 		</ion-item>\n\n 		<ion-item>\n 			<ion-textarea [(ngModel)]="jabatan_data.keterangan" type="textarea" placeholder="Keterangan" name="keterangan"></ion-textarea>\n 		</ion-item>\n\n 		<ion-item-divider color="light">Surat</ion-item-divider>\n 		<ion-item>\n 			<ion-input [(ngModel)]="jabatan_data.surat" type="text" placeholder="No. Surat" name="surat"></ion-input>\n 		</ion-item>\n 		<ion-item>\n 			<ion-label>Tanggal Surat</ion-label>\n 			<ion-datetime displayFormat="MMMM, DD YYYY" max="2030"  [(ngModel)]="jabatan_data.tanggal_surat" name="tanggal_surat">\n 			</ion-datetime>\n 		</ion-item>\n 		<ion-item>\n 			<ion-input [(ngModel)]="jabatan_data.nama_pejabat_penandatangan" type="text" placeholder="Nama Pejabat Penandatangan" name="nama_pejabat_penandatangan"></ion-input>\n 		</ion-item>\n 		<ion-item>\n 			<ion-input [(ngModel)]="jabatan_data.jabatan_pejabat_penandatangan" type="text" placeholder="Jabatan Pejabat Penandatangan" name="jabatan_pejabat_penandatangan"></ion-input>\n 		</ion-item>\n 			<ion-input  type="hidden"  [(ngModel)]="jabatan_data.nama_lampiran" placeholder="Lampiran" name="nama_lampiran"></ion-input >\n 		<ion-row>\n 			<ion-col style="text-align: left; padding-left: 20px;">\n 			<button ion-button (click)="pilihfile()" type="button">Pilih file</button>\n 			</ion-col>\n 		</ion-row>\n 		<ion-item *ngIf=" imageFilePath != null ">\n	 		<div style="margin: 0 auto;text-align: left;width: 90%; height:70%;">\n	 			<img src="{{ imageFilePath }}"/>\n	 		</div>\n 		</ion-item> \n 		</ion-item-group>\n 		<br>\n 		<ion-row>\n 			<ion-col style="text-align: center;">\n 				<button ion-button block type="submit">Submit</button>\n 			</ion-col>\n 		</ion-row>\n 		<ion-row>\n 			<ion-col style="text-align: center;">\n 				<button ion-button block type="reset" color="danger" (click)="resetImage()">Reset</button>\n 			</ion-col>\n 		</ion-row>\n 	</form>	\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-jabatan-add/riwayat-jabatan-add.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], RiwayatJabatanAddPage);

//# sourceMappingURL=riwayat-jabatan-add.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailRiwayatKenaikanPangkatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_riwayat_kenaikan_pangkat_riwayat_kenaikan_pangkat__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the DetailRiwayatKenaikanPangkatPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var DetailRiwayatKenaikanPangkatPage = (function () {
    function DetailRiwayatKenaikanPangkatPage(navCtrl, navParams, toastCtrl, loadingCtrl, dataService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.dataService = dataService;
        this.error = { data: '' };
        this.foto = localStorage.getItem('bsw-foto');
        this.getDetailRiwayatKenaikanPangkat(navParams.get('id'));
        this.groups = localStorage.getItem('bsw-groups');
    }
    DetailRiwayatKenaikanPangkatPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailRiwayatKenaikanPangkatPage');
    };
    DetailRiwayatKenaikanPangkatPage.prototype.getDetailRiwayatKenaikanPangkat = function (id) {
        var _this = this;
        this.showLoader("Mengambil data dari server...");
        this.dataService.getDetailRiwayatKenaikanPangkat(id).subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_detail_riwayat_kenaikan_pangkat = data;
            _this.lists_detail_riwayat_kenaikan_pangkat = data.results;
        });
    };
    DetailRiwayatKenaikanPangkatPage.prototype.showLoader = function (msg) {
        this.loading = this.loadingCtrl.create({
            content: msg
        });
        this.loading.present();
    };
    DetailRiwayatKenaikanPangkatPage.prototype.doSubmitVerifikasi = function (id) {
        var data = {
            status: 1
        };
        this.postData(id, data);
        console.log(data);
    };
    DetailRiwayatKenaikanPangkatPage.prototype.doSubmitToVerifikator = function (id) {
        var data = {
            status: 2
        };
        this.postData(id, data);
        console.log(data);
    };
    DetailRiwayatKenaikanPangkatPage.prototype.postData = function (id, data) {
        var _this = this;
        this.showLoader("Mohon tunggu sebentar...");
        this.dataService.postStatusDetailKenaikanPangkat(id, data).then(function (result) {
            _this.loading.dismiss();
            _this.data_result_post = result;
            {
                var toast = _this.toastCtrl.create({
                    message: "Berhasil mengirim data...",
                    duration: 3000,
                    position: 'top',
                    dismissOnPageChange: true
                });
                toast.onDidDismiss(function () {
                    console.log('Berhasil');
                });
                toast.present();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_riwayat_kenaikan_pangkat_riwayat_kenaikan_pangkat__["a" /* RiwayatKenaikanPangkatPage */]);
            }
        }, function (err) {
            _this.loading.dismiss();
            _this.presentToast(err);
        });
    };
    DetailRiwayatKenaikanPangkatPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    DetailRiwayatKenaikanPangkatPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    return DetailRiwayatKenaikanPangkatPage;
}());
DetailRiwayatKenaikanPangkatPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-detail-riwayat-kenaikan-pangkat',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/detail-riwayat-kenaikan-pangkat/detail-riwayat-kenaikan-pangkat.html"*/'<!--\n  Generated template for the DetailRiwayatKenaikanPangkatPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header color="primary">\n\n	<ion-navbar color="primary">\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>Riwayat Kenaikan Pangkat</ion-title>\n		<ion-buttons end style="width: 10%">\n			<a (click)="GoToDashboard()">\n			<img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n		</ion-buttons>   \n	</ion-navbar>\n</ion-header>\n\n\n<ion-content no-padding>\n	<ion-card *ngFor="let list_detail_riwayat_kenaikan_pangkat of lists_detail_riwayat_kenaikan_pangkat">\n		<ion-grid col-12>\n			<ion-row>\n				<ion-col col-12 text-left>Detail Riwayat ({{ list_detail_riwayat_kenaikan_pangkat.golongan }})</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card>\n\n	<ion-card *ngFor="let list_detail_riwayat_kenaikan_pangkat of lists_detail_riwayat_kenaikan_pangkat">\n\n		<ion-grid>\n			<ion-row>\n				<ion-col col-6>Golongan\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_kenaikan_pangkat.golongan }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Jabatan \n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_kenaikan_pangkat.jabatan }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Status Pegawai\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_kenaikan_pangkat.status_pegawai }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Jenis Kenaikan Pangkat\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_kenaikan_pangkat.jenis_kenaikan_pangkat }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>TMT\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_kenaikan_pangkat.tmt }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Gaji Pokok Baru\n				</ion-col>\n				<ion-col col-6 text-left> {{ list_detail_riwayat_kenaikan_pangkat.gaji_pokok_baru }}\n				</ion-col>\n			</ion-row>\n\n		</ion-grid>\n		<div id="button-content">\n		<div *ngIf=" groups == \'Operator\' ">\n			<input type="submit" (click)="doSubmitToVerifikator(list_detail_riwayat_kenaikan_pangkat.id)" value="Kirim ke Verifikator" class="kirim_ke_verifikator" *ngIf=" list_detail_riwayat_kenaikan_pangkat.status == 6">\n		</div>\n		<div *ngIf=" groups == \'Verifikator\' ">\n			<input type="submit" (click)="doSubmitVerifikasi(list_detail_riwayat_kenaikan_pangkat.id)" value="Verifikasi" class="kirim_verifikasi" *ngIf=" list_detail_riwayat_kenaikan_pangkat.status == 2">\n		</div>\n		<!-- <input type="submit" value="Download Berkas" class="download" (click)=downloadFile(list_detail_riwayat_kenaikan_pangkat.berkas_url)> -->\n\n			<!-- <input type="button" value="Kirim ke Verifikator" class="kirim_ke_verifikator">\n			<input type="button" value="Verifikasi" class="kirim_verifikasi">\n			<input type="button" value="Download Berkas" class="download"> -->\n		</div>\n		<ion-input type="text" [(ngModel)]="error.data" name="data"></ion-input>\n	</ion-card>\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/detail-riwayat-kenaikan-pangkat/detail-riwayat-kenaikan-pangkat.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */]])
], DetailRiwayatKenaikanPangkatPage);

//# sourceMappingURL=detail-riwayat-kenaikan-pangkat.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailRiwayatUnitKerjaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_riwayat_unit_kerja_riwayat_unit_kerja__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_android_permissions__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the DetailRiwayatUnitKerjaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var DetailRiwayatUnitKerjaPage = (function () {
    function DetailRiwayatUnitKerjaPage(navCtrl, navParams, dataService, toastCtrl, loadingCtrl, transfer, androidPermissions) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.transfer = transfer;
        this.androidPermissions = androidPermissions;
        this.post_data = { status_to_verifikator: '', status_verifikasi: '' };
        this.error = { data: '' };
        this.foto = localStorage.getItem('bsw-foto');
        this.getDetailRiwayatUnitKerja(navParams.get('id'));
        this.groups = localStorage.getItem('bsw-groups');
    }
    DetailRiwayatUnitKerjaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailRiwayatUnitKerjaPage');
    };
    DetailRiwayatUnitKerjaPage.prototype.getDetailRiwayatUnitKerja = function (id) {
        var _this = this;
        this.showLoader("Mengambil data dari server...");
        this.dataService.getDetailRiwayatUnitKerja(id).subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_detail_riwayat_unit_kerja = data;
            _this.lists_detail_riwayat_unit_kerja = data.results;
        });
    };
    DetailRiwayatUnitKerjaPage.prototype.doSubmitVerifikasi = function (id) {
        var data = {
            status: 1
        };
        this.postData(id, data);
        console.log(data);
    };
    DetailRiwayatUnitKerjaPage.prototype.doSubmitToVerifikator = function (id) {
        var data = {
            status: 2
        };
        this.postData(id, data);
        console.log(data);
    };
    DetailRiwayatUnitKerjaPage.prototype.showLoader = function (msg) {
        this.loading = this.loadingCtrl.create({
            content: msg
        });
        this.loading.present();
    };
    DetailRiwayatUnitKerjaPage.prototype.postData = function (id, data) {
        var _this = this;
        this.showLoader("Mohon tunggu sebentar...");
        this.dataService.postStatusDetailUnitKerja(id, data).then(function (result) {
            _this.loading.dismiss();
            _this.data_result_post = result;
            {
                var toast = _this.toastCtrl.create({
                    message: "Berhasil mengirim data...",
                    duration: 3000,
                    position: 'top',
                    dismissOnPageChange: true
                });
                toast.onDidDismiss(function () {
                    console.log('Berhasil');
                });
                toast.present();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_riwayat_unit_kerja_riwayat_unit_kerja__["a" /* RiwayatUnitKerjaPage */]);
            }
        }, function (err) {
            _this.loading.dismiss();
            _this.presentToast(err);
        });
    };
    DetailRiwayatUnitKerjaPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    DetailRiwayatUnitKerjaPage.prototype.downloadFile = function (fileUrl) {
        var _this = this;
        var fileTransfer = this.transfer.create();
        var url = fileUrl;
        var pemilik = localStorage.getItem('bsw-nama-lengkap');
        var date = new Date().toISOString();
        var filesdownload = pemilik + '-' + date + '-' + '.png';
        fileTransfer.download(url, 'file:///storage/sdcard0/Download/' + filesdownload).then(function (entry) {
            //this.error.data = "Benar : "+entry.toURL();
            _this.presentToast("File telah berhasil di unduh");
        }, function (error) {
            //this.error.data = "Salah : "+JSON.stringify(error);
        });
    };
    DetailRiwayatUnitKerjaPage.prototype.checking_permissions_to_download = function (fileUrl) {
        var _this = this;
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(function (success) { return _this.downloadFile(fileUrl); }, function (err) { return _this.getting_permissions(fileUrl); });
    };
    DetailRiwayatUnitKerjaPage.prototype.getting_permissions = function (fileUrl) {
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
        this.downloadFile(fileUrl);
    };
    DetailRiwayatUnitKerjaPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    return DetailRiwayatUnitKerjaPage;
}());
DetailRiwayatUnitKerjaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-detail-riwayat-unit-kerja',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/detail-riwayat-unit-kerja/detail-riwayat-unit-kerja.html"*/'<!--\n  Generated template for the DetailRiwayatUnitKerjaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar color="primary">\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>Riwayat Unit Kerja</ion-title>\n		<ion-buttons end style="width: 10%">\n			<a (click)="GoToDashboard()">\n			<img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n		</ion-buttons>   \n	</ion-navbar>\n</ion-header>\n\n\n<ion-content no-padding>\n\n	<ion-card>\n		<ion-grid col-12>\n			<ion-row *ngFor="let list_detail_riwayat_unit_kerja of lists_detail_riwayat_unit_kerja">\n				<ion-col col-12 text-left>Detail Riwayat ({{ list_detail_riwayat_unit_kerja.unit_kerja }})</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card>\n\n	<ion-card *ngFor="let list_detail_riwayat_unit_kerja of lists_detail_riwayat_unit_kerja">\n\n		<ion-grid>\n			<ion-row>\n				<ion-col col-6>Nama Satker\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_unit_kerja.unit_kerja }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Wilayah Kerja \n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_unit_kerja.wilayah_kerja }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Bidang Struktural\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_unit_kerja.bidang_struktural }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Tanggal Mulai\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_unit_kerja.tmt }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Tanggal SK\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_unit_kerja.tanggal_sk }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Nomor SK\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_unit_kerja.nomor_sk }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Keterangan\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_unit_kerja.keterangan }}\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	<div id="button-content">\n	<div *ngIf=" groups == \'Operator\' ">\n		<input type="submit" (click)="doSubmitToVerifikator(list_detail_riwayat_unit_kerja.id)" value="Kirim ke Verifikator" class="kirim_ke_verifikator" *ngIf=" list_detail_riwayat_unit_kerja.status == 6">\n	</div>\n	<div *ngIf=" groups == \'Verifikator\' ">\n		<input type="submit" (click)="doSubmitVerifikasi(list_detail_riwayat_unit_kerja.id)" value="Verifikasi" class="kirim_verifikasi" *ngIf=" list_detail_riwayat_unit_kerja.status == 2">\n	</div>\n	<div *ngIf=" list_detail_riwayat_unit_kerja.berkas_url != \'-\' ">\n	<input type="submit" value="Download Berkas" class="download" (click)=checking_permissions_to_download(list_detail_riwayat_unit_kerja.berkas_url)>\n	</div>\n\n		<!-- <input type="button" value="Kirim ke Verifikator" class="kirim_ke_verifikator">\n		<input type="button" value="Verifikasi" class="kirim_verifikasi">\n		<input type="button" value="Download Berkas" class="download"> -->\n	</div>\n	<ion-input type="hidden" [(ngModel)]="error.data" name="data"></ion-input>\n	</ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/detail-riwayat-unit-kerja/detail-riwayat-unit-kerja.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_6__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_android_permissions__["a" /* AndroidPermissions */]])
], DetailRiwayatUnitKerjaPage);

//# sourceMappingURL=detail-riwayat-unit-kerja.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiwayatUnitKerjaAddPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_riwayat_unit_kerja_riwayat_unit_kerja__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_riwayat_unit_kerja_modal_cari_pegawai_riwayat_unit_kerja_modal_cari_pegawai__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_riwayat_unit_kerja_modal_cari_unit_kerja_riwayat_unit_kerja_modal_cari_unit_kerja__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_chooser__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file_path__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_base64__ = __webpack_require__(202);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the RiwayatUnitKerjaAddPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var base_url = "http://192.168.200.88:8899/api-mobile/";
var RiwayatUnitKerjaAddPage = (function () {
    function RiwayatUnitKerjaAddPage(navCtrl, navParams, modalCtrl, viewCtrl, http, loadingCtrl, dataService, toastCtrl, fileChooser, filePath, base64) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.dataService = dataService;
        this.toastCtrl = toastCtrl;
        this.fileChooser = fileChooser;
        this.filePath = filePath;
        this.base64 = base64;
        this.unit_kerja_data = { pegawai: '', nama_pegawai: '', unit_kerja: '', value_unit_kerja: '',
            bidang_struktural: '', tmt: '', wilayah_kerja: '', keterangan: '', surat: '', nama_lampiran: '', lampiran: '', nama_pejabat_penandatangan: '',
            jabatan_pejabat_penandatangan: '', tanggal_surat: '' };
    }
    RiwayatUnitKerjaAddPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatUnitKerjaAddPage');
    };
    RiwayatUnitKerjaAddPage.prototype.pilihfile = function () {
        var _this = this;
        this.fileChooser.open().then(function (uri) {
            return _this.filePath.resolveNativePath(uri).then(function (filePath) { return _this.encryptedFile(filePath); });
        })
            .catch(function (e) { return _this.presentToast(e); });
    };
    RiwayatUnitKerjaAddPage.prototype.resetImage = function () {
        this.imageFilePath = null;
        this.encryptedFile("");
    };
    RiwayatUnitKerjaAddPage.prototype.encryptedFile = function (filePath) {
        var _this = this;
        this.base64.encodeFile(filePath).then(function (base64File) {
            _this.imageFilePath = filePath;
            _this.unit_kerja_data.nama_lampiran = base64File;
        }, function (err) {
            console.log(err);
        });
    };
    RiwayatUnitKerjaAddPage.prototype.openModalCariPegawai = function () {
        var _this = this;
        var modal_cari_pegawai = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__pages_riwayat_unit_kerja_modal_cari_pegawai_riwayat_unit_kerja_modal_cari_pegawai__["a" /* RiwayatUnitKerjaModalCariPegawaiPage */]);
        modal_cari_pegawai.onDidDismiss(function (data) {
            _this.unit_kerja_data.pegawai = data.id;
            _this.unit_kerja_data.nama_pegawai = data.username;
        });
        modal_cari_pegawai.present();
    };
    RiwayatUnitKerjaAddPage.prototype.openModalCariUnitKerja = function () {
        var _this = this;
        var modal_cari_unit_kerja = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__pages_riwayat_unit_kerja_modal_cari_unit_kerja_riwayat_unit_kerja_modal_cari_unit_kerja__["a" /* RiwayatUnitKerjaModalCariUnitKerjaPage */]);
        modal_cari_unit_kerja.onDidDismiss(function (data) {
            _this.unit_kerja_data.unit_kerja = data.nama_unit_kerja;
            _this.unit_kerja_data.value_unit_kerja = data.id;
            console.log(data);
            _this.getListDataBidangStruktural();
        });
        modal_cari_unit_kerja.present();
    };
    RiwayatUnitKerjaAddPage.prototype.getListDataBidangStruktural = function () {
        var _this = this;
        this.showLoader();
        this.http.get(base_url + 'bidang-struktural/?q=' + this.unit_kerja_data.value_unit_kerja).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.loading.dismiss();
            //this.items = data;
            _this.items = data.results;
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    RiwayatUnitKerjaAddPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Fetching Data...'
        });
        this.loading.present();
    };
    RiwayatUnitKerjaAddPage.prototype.doSubmitUnitKerja = function () {
        if (this.unit_kerja_data.pegawai == null || this.unit_kerja_data.pegawai == "" || this.unit_kerja_data.value_unit_kerja == null || this.unit_kerja_data.value_unit_kerja == "" ||
            this.unit_kerja_data.tmt == null || this.unit_kerja_data.tmt == "" || this.unit_kerja_data.wilayah_kerja == null || this.unit_kerja_data.wilayah_kerja == "" ||
            this.unit_kerja_data.keterangan == null || this.unit_kerja_data.keterangan == "") {
            console.log("Silahkan isi semua form-fill");
        }
        else {
            var in_surat = {
                no_surat: this.unit_kerja_data.surat,
                tanggal: this.unit_kerja_data.tanggal_surat,
                nama_pejabat_penandatangan: this.unit_kerja_data.nama_pejabat_penandatangan,
                jabatan_pejabat_penandatangan: this.unit_kerja_data.jabatan_pejabat_penandatangan,
                lampiran: this.unit_kerja_data.nama_lampiran
            };
            var data = {
                pegawai: this.unit_kerja_data.pegawai,
                unit_kerja: this.unit_kerja_data.value_unit_kerja,
                bidang_struktural: this.unit_kerja_data.bidang_struktural,
                tmt: this.unit_kerja_data.tmt,
                wilayah_kerja: this.unit_kerja_data.wilayah_kerja,
                keterangan: this.unit_kerja_data.keterangan,
                surat: in_surat
            };
            this.postData(data);
            console.log(data);
        }
    };
    RiwayatUnitKerjaAddPage.prototype.postData = function (data) {
        var _this = this;
        this.dataService.postRiwayatUnitKerjaBaru(data).then(function (result) {
            _this.loading.dismiss();
            _this.data_result_post = result;
            {
                var toast = _this.toastCtrl.create({
                    message: "Berhasil menambahkan data...",
                    duration: 3000,
                    position: 'top',
                    dismissOnPageChange: true
                });
                toast.onDidDismiss(function () {
                    console.log('Berhasil');
                });
                toast.present();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_riwayat_unit_kerja_riwayat_unit_kerja__["a" /* RiwayatUnitKerjaPage */]);
            }
        }, function (err) {
            _this.loading.dismiss();
            _this.presentToast(err);
        });
    };
    RiwayatUnitKerjaAddPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    return RiwayatUnitKerjaAddPage;
}());
RiwayatUnitKerjaAddPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-riwayat-unit-kerja-add',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-unit-kerja-add/riwayat-unit-kerja-add.html"*/'<!--\n  Generated template for the RiwayatUnitKerjaAddPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n -->\n <ion-header>\n\n 	<ion-navbar color="primary">\n 		<ion-title>Riwayat Unit Kerja Baru</ion-title>\n 	</ion-navbar>\n\n </ion-header>\n\n\n <ion-content no-padding>\n 	<form (submit)="doSubmitUnitKerja()">\n 		<ion-item-group>\n 		<ion-item-divider color="light">Pegawai</ion-item-divider>\n 		<ion-item>\n 			<ion-label style="color: black;">NIP Pegawai</ion-label>\n 			<ion-input type="text" [(ngModel)]="unit_kerja_data.nama_pegawai" name="nama_pegawai"\n 			 (click)="openModalCariPegawai()" disabled></ion-input>\n 		</ion-item>\n 			<ion-input type="hidden" [(ngModel)]="unit_kerja_data.pegawai" name="pegawai"></ion-input>\n\n 		<ion-item>\n 			<ion-label style="color: black;">Unit Kerja</ion-label>\n 			<ion-input type="text" [(ngModel)]="unit_kerja_data.unit_kerja" name="unit_kerja" \n 			(click)="openModalCariUnitKerja()" disabled></ion-input>\n 		</ion-item>\n 			<ion-input type="hidden" [(ngModel)]="unit_kerja_data.value_unit_kerja" name="value_unit_kerja" ></ion-input>\n\n 		<ion-item>\n 			<ion-label>Bidang Struktural</ion-label>\n 			<ion-select [(ngModel)]="unit_kerja_data.bidang_struktural" name="bidang_struktural">\n 				<ion-option *ngFor="let item of items" value="{{ item.id }}">{{ item.nama_bidang }}</ion-option>\n 			</ion-select>\n 		</ion-item>\n\n 		<ion-item>\n 			<ion-label>TMT</ion-label>\n 			<ion-datetime displayFormat="MMMM, DD YYYY" max="2030"  [(ngModel)]="unit_kerja_data.tmt" name="tmt">\n 			</ion-datetime>\n 		</ion-item>\n\n 		<ion-item>\n 			<ion-input [(ngModel)]="unit_kerja_data.wilayah_kerja"  type="text" placeholder="Wilayah Kerja" name="wilayah_kerja"></ion-input>\n 		</ion-item>\n\n 		<ion-item>\n 			<ion-textarea [(ngModel)]="unit_kerja_data.keterangan" type="textarea" placeholder="Keterangan" name="keterangan"></ion-textarea>\n 		</ion-item>\n\n 		<ion-item-divider color="light">Surat</ion-item-divider>\n 		<ion-item>\n 			<ion-input [(ngModel)]="unit_kerja_data.surat" type="text" placeholder="No. Surat" name="surat"></ion-input>\n 		</ion-item>\n 		<ion-item>\n 			<ion-label>Tanggal Surat</ion-label>\n 			<ion-datetime displayFormat="MMMM, DD YYYY" max="2030"  [(ngModel)]="unit_kerja_data.tanggal_surat" name="tanggal_surat">\n 			</ion-datetime>\n 		</ion-item>\n 		<ion-item>\n 			<ion-input [(ngModel)]="unit_kerja_data.nama_pejabat_penandatangan" type="text" placeholder="Nama Pejabat Penandatangan" name="nama_pejabat_penandatangan"></ion-input>\n 		</ion-item>\n 		<ion-item>\n 			<ion-input [(ngModel)]="unit_kerja_data.jabatan_pejabat_penandatangan" type="text" placeholder="Jabatan Pejabat Penandatangan" name="jabatan_pejabat_penandatangan"></ion-input>\n 		</ion-item>\n 			<ion-input  type="hidden"  [(ngModel)]="unit_kerja_data.nama_lampiran" placeholder="Lampiran" name="nama_lampiran"></ion-input >\n 		<ion-row>\n 			<ion-col style="text-align: left; padding-left: 20px;">\n 			<button ion-button (click)="pilihfile()" type="button">Pilih file</button>\n 			</ion-col>\n 		</ion-row>\n 		<ion-item *ngIf=" imageFilePath != null ">\n	 		<div style="margin: 0 auto;text-align: left;width: 90%; height:70%;">\n	 			<img src="{{ imageFilePath }}"/>\n	 		</div>\n 		</ion-item> \n 		</ion-item-group>\n 		<br>\n 		<ion-row>\n 			<ion-col style="text-align: center;">\n 				<button ion-button block type="submit">Submit</button>\n 			</ion-col>\n 		</ion-row>\n 		<ion-row>\n 			<ion-col style="text-align: center;">\n 				<button ion-button block type="reset" color="danger" (click)="resetImage()">Reset</button>\n 			</ion-col>\n 		</ion-row>\n 	</form>\n\n </ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-unit-kerja-add/riwayat-unit-kerja-add.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_chooser__["a" /* FileChooser */],
        __WEBPACK_IMPORTED_MODULE_8__ionic_native_file_path__["a" /* FilePath */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_base64__["a" /* Base64 */]])
], RiwayatUnitKerjaAddPage);

//# sourceMappingURL=riwayat-unit-kerja-add.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiwayatUnitKerjaModalCariPegawaiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the RiwayatUnitKerjaModalCariPegawaiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//let base_url = "http://192.168.200.88:8899/api-mobile/";
var RiwayatUnitKerjaModalCariPegawaiPage = (function () {
    function RiwayatUnitKerjaModalCariPegawaiPage(navCtrl, navParams, viewCtrl, http, loadingCtrl, dataService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.dataService = dataService;
        this.base_url = this.dataService.base_url.toString();
    }
    RiwayatUnitKerjaModalCariPegawaiPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatUnitKerjaModalCariPegawaiPage');
    };
    RiwayatUnitKerjaModalCariPegawaiPage.prototype.doSubmitUnitKerja = function () {
    };
    RiwayatUnitKerjaModalCariPegawaiPage.prototype.checkDigit = function () {
        if (this.cari_pegawai.length <= 3) {
            this.buttonClicked = "false";
        }
        else if (this.cari_pegawai.length > 3) {
            this.buttonClicked = "true";
        }
    };
    RiwayatUnitKerjaModalCariPegawaiPage.prototype.closeModal = function (id, username) {
        var data = { id: id, username: username };
        this.viewCtrl.dismiss(data);
        console.log('Text Modal :' + data);
    };
    RiwayatUnitKerjaModalCariPegawaiPage.prototype.getListDataPegawaiAktif = function () {
        var _this = this;
        this.showLoader();
        this.http.get(this.base_url + 'pegawai-aktif/?q=' + this.cari_pegawai).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.loading.dismiss();
            //this.items = data;
            _this.items = data.results;
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    RiwayatUnitKerjaModalCariPegawaiPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Fetching Data...'
        });
        this.loading.present();
    };
    return RiwayatUnitKerjaModalCariPegawaiPage;
}());
RiwayatUnitKerjaModalCariPegawaiPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-riwayat-unit-kerja-modal-cari-pegawai',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-unit-kerja-modal-cari-pegawai/riwayat-unit-kerja-modal-cari-pegawai.html"*/'<!--\n  Generated template for the RiwayatUnitKerjaModalCariPegawaiPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-searchbar placeholder="Cari Pegawai...." [(ngModel)]="cari_pegawai" name="cari_pegawai" (ionInput)="checkDigit()"></ion-searchbar>\n    <ion-buttons end>\n    <button ion-button color="light" clear (click)="getListDataPegawaiAktif()" *ngIf="buttonClicked == \'true\'">Cari</button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<ion-list>\n  <ion-item *ngFor="let item of items" (click)="closeModal(item.id, item.username)">\n    <h2>{{ item.nama_lengkap }}</h2>\n    <h3>{{ item.username }}</h3>\n  </ion-item>\n</ion-list>\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-unit-kerja-modal-cari-pegawai/riwayat-unit-kerja-modal-cari-pegawai.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */]])
], RiwayatUnitKerjaModalCariPegawaiPage);

//# sourceMappingURL=riwayat-unit-kerja-modal-cari-pegawai.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiwayatUnitKerjaModalCariUnitKerjaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the RiwayatUnitKerjaModalCariUnitKerjaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RiwayatUnitKerjaModalCariUnitKerjaPage = (function () {
    function RiwayatUnitKerjaModalCariUnitKerjaPage(navCtrl, navParams, viewCtrl, http, loadingCtrl, dataService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.dataService = dataService;
        this.base_url = this.dataService.base_url.toString();
    }
    RiwayatUnitKerjaModalCariUnitKerjaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatUnitKerjaModalCariUnitKerjaPage');
    };
    RiwayatUnitKerjaModalCariUnitKerjaPage.prototype.checkDigit = function () {
        if (this.cari_unit_kerja.length <= 3) {
            this.buttonClicked = "false";
        }
        else if (this.cari_unit_kerja.length > 3) {
            this.buttonClicked = "true";
        }
    };
    RiwayatUnitKerjaModalCariUnitKerjaPage.prototype.closeModal = function (id, nama_unit_kerja) {
        var data = {
            id: id,
            nama_unit_kerja: nama_unit_kerja
        };
        this.viewCtrl.dismiss(data);
        console.log('Text Modal :' + data.id + " " + data.nama_unit_kerja);
    };
    RiwayatUnitKerjaModalCariUnitKerjaPage.prototype.getListDataUnitKerja = function () {
        var _this = this;
        this.showLoader();
        this.http.get(this.base_url + 'unit-kerja/?q=' + this.cari_unit_kerja).map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.loading.dismiss();
            //this.items = data;
            _this.items = data.results;
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    RiwayatUnitKerjaModalCariUnitKerjaPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Fetching Data...'
        });
        this.loading.present();
    };
    return RiwayatUnitKerjaModalCariUnitKerjaPage;
}());
RiwayatUnitKerjaModalCariUnitKerjaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-riwayat-unit-kerja-modal-cari-unit-kerja',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-unit-kerja-modal-cari-unit-kerja/riwayat-unit-kerja-modal-cari-unit-kerja.html"*/'<!--\n  Generated template for the RiwayatUnitKerjaModalCariUnitKerjaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-searchbar placeholder="Cari Unit Kerja...." [(ngModel)]="cari_unit_kerja" name="cari_unit_kerja" (ionInput)="checkDigit()"></ion-searchbar>\n    <input type="hidden" [(ngModel)]="nama_bidang" name="nama_bidang">\n    <ion-buttons end>\n    <button ion-button color="light" clear (click)="getListDataUnitKerja()" *ngIf="buttonClicked == \'true\'">Cari</button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<ion-list>\n  <ion-item *ngFor="let item of items" (click)="closeModal(item.id, item.nama_unit_kerja)">\n    <h2>{{ item.nama_unit_kerja }}</h2>\n    <h3>{{ item.id }}</h3>\n  </ion-item>\n</ion-list>\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-unit-kerja-modal-cari-unit-kerja/riwayat-unit-kerja-modal-cari-unit-kerja.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */]])
], RiwayatUnitKerjaModalCariUnitKerjaPage);

//# sourceMappingURL=riwayat-unit-kerja-modal-cari-unit-kerja.js.map

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailRiwayatPendidikanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_riwayat_pendidikan_riwayat_pendidikan__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the DetailRiwayatPendidikanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var DetailRiwayatPendidikanPage = (function () {
    function DetailRiwayatPendidikanPage(navCtrl, navParams, toastCtrl, loadingCtrl, dataService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.dataService = dataService;
        this.error = { data: '' };
        this.foto = localStorage.getItem('bsw-foto');
        this.getDetailRiwayatPendidikan(navParams.get('id'));
        this.groups = localStorage.getItem('bsw-groups');
    }
    DetailRiwayatPendidikanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailRiwayatPendidikanPage');
    };
    DetailRiwayatPendidikanPage.prototype.getDetailRiwayatPendidikan = function (id) {
        var _this = this;
        this.showLoader("Mengambil data dari server...");
        this.dataService.getDetailRiwayatPendidikan(id).subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_detail_riwayat_jabatan = data;
            _this.lists_detail_riwayat_pendidikan = data.results;
        });
    };
    DetailRiwayatPendidikanPage.prototype.showLoader = function (msg) {
        this.loading = this.loadingCtrl.create({
            content: msg
        });
        this.loading.present();
    };
    DetailRiwayatPendidikanPage.prototype.doSubmitVerifikasi = function (id) {
        var data = {
            status: 1
        };
        this.postData(id, data);
        console.log(data);
    };
    DetailRiwayatPendidikanPage.prototype.doSubmitToVerifikator = function (id) {
        var data = {
            status: 2
        };
        this.postData(id, data);
        console.log(data);
    };
    DetailRiwayatPendidikanPage.prototype.postData = function (id, data) {
        var _this = this;
        this.showLoader("Mohon tunggu sebentar...");
        this.dataService.postStatusDetailPendidikan(id, data).then(function (result) {
            _this.loading.dismiss();
            _this.data_result_post = result;
            {
                var toast = _this.toastCtrl.create({
                    message: "Berhasil mengirim data...",
                    duration: 3000,
                    position: 'top',
                    dismissOnPageChange: true
                });
                toast.onDidDismiss(function () {
                    console.log('Berhasil');
                });
                toast.present();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_riwayat_pendidikan_riwayat_pendidikan__["a" /* RiwayatPendidikanPage */]);
            }
        }, function (err) {
            _this.loading.dismiss();
            _this.presentToast(err);
        });
    };
    DetailRiwayatPendidikanPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    DetailRiwayatPendidikanPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    return DetailRiwayatPendidikanPage;
}());
DetailRiwayatPendidikanPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-detail-riwayat-pendidikan',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/detail-riwayat-pendidikan/detail-riwayat-pendidikan.html"*/'<!--\n  Generated template for the DetailRiwayatPendidikanPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header color="primary">\n\n	<ion-navbar color="primary">\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>Riwayat Pendidikan</ion-title>\n		<ion-buttons end style="width: 10%">\n			<a (click)="GoToDashboard()">\n			<img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n		</ion-buttons>   \n	</ion-navbar>\n</ion-header>\n\n<ion-content no-padding>\n	<ion-card>\n		<ion-grid col-12>\n			<ion-row *ngFor="let list_detail_riwayat_pendidikan of lists_detail_riwayat_pendidikan">\n				<ion-col col-12 text-left>Detail Riwayat ({{ list_detail_riwayat_pendidikan.jenjang_pendidikan }})</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card>\n\n	<ion-card  *ngFor="let list_detail_riwayat_pendidikan of lists_detail_riwayat_pendidikan">\n\n		<ion-grid>\n			<ion-row>\n				<ion-col col-6>Pendidikan\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_pendidikan.jenjang_pendidikan }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Jurusan \n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_pendidikan.jurusan_pendidikan }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Instansi Pendidikan\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_pendidikan.instansi_pendidikan }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Kota\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_pendidikan.kota }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Pendidikan Terakhir\n				</ion-col>\n				<ion-col col-6 text-left *ngIf=" list_detail_riwayat_pendidikan.pendidikan_terakhir == \'True\' ">Ya\n				</ion-col>\n				<ion-col col-6 text-left *ngIf=" list_detail_riwayat_pendidikan.pendidikan_terakhir == \'False\' ">Tidak\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Tanggal Ijazah\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_pendidikan.tanggal_ijazah }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Nomer Ijazah\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_pendidikan.nomor_ijazah }}\n				</ion-col>\n			</ion-row>\n\n		</ion-grid>\n		<div id="button-content">\n		<div *ngIf=" groups == \'Operator\' ">\n			<input type="submit" (click)="doSubmitToVerifikator(list_detail_riwayat_pendidikan.id)" value="Kirim ke Verifikator" class="kirim_ke_verifikator" *ngIf=" list_detail_riwayat_pendidikan.status == 6">\n		</div>\n		<div *ngIf=" groups == \'Verifikator\' ">\n			<input type="submit" (click)="doSubmitVerifikasi(list_detail_riwayat_pendidikan.id)" value="Verifikasi" class="kirim_verifikasi" *ngIf=" list_detail_riwayat_pendidikan.status == 2">\n		</div>\n		<!-- <input type="submit" value="Download Berkas" class="download" (click)=downloadFile(list_detail_riwayat_pendidikan.berkas_url)> -->\n\n			<!-- <input type="button" value="Kirim ke Verifikator" class="kirim_ke_verifikator">\n			<input type="button" value="Verifikasi" class="kirim_verifikasi">\n			<input type="button" value="Download Berkas" class="download"> -->\n		</div>\n		<ion-input type="text" [(ngModel)]="error.data" name="data"></ion-input>\n	</ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/detail-riwayat-pendidikan/detail-riwayat-pendidikan.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */]])
], DetailRiwayatPendidikanPage);

//# sourceMappingURL=detail-riwayat-pendidikan.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiwayatHukumanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_detail_riwayat_hukuman_detail_riwayat_hukuman__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RiwayatHukumanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RiwayatHukumanPage = (function () {
    function RiwayatHukumanPage(navCtrl, navParams, dataService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.loadingCtrl = loadingCtrl;
        this.foto = localStorage.getItem('bsw-foto');
        this.getDataRiwayatHukuman();
    }
    RiwayatHukumanPage.prototype.doRefresh = function (refresh) {
        this.getDataRiwayatHukuman();
        refresh.complete();
    };
    RiwayatHukumanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatHukumanPage');
    };
    RiwayatHukumanPage.prototype.getDataRiwayatHukuman = function () {
        var _this = this;
        this.showLoader();
        this.dataService.getListRiwayatHukuman().subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_riwayat_jabatan = data;
            _this.lists_riwayat_hukuman = data.results;
        });
    };
    RiwayatHukumanPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Mengambil data dari server...'
        });
        this.loading.present();
    };
    RiwayatHukumanPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    RiwayatHukumanPage.prototype.GoToDetailRiwayatHukuman = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_detail_riwayat_hukuman_detail_riwayat_hukuman__["a" /* DetailRiwayatHukumanPage */], { id: id });
    };
    return RiwayatHukumanPage;
}());
RiwayatHukumanPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-riwayat-hukuman',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-hukuman/riwayat-hukuman.html"*/'<!--\n  Generated template for the RiwayatHukumanPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n  	<button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Riwayat Hukuman</ion-title>\n    <ion-buttons end style="width: 10%">\n      <a (click)="GoToDashboard()"><img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n    </ion-buttons>   \n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n<ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingText="Pull to refresh..."\n      refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-list no-lines>\n    <ion-item-sliding *ngFor="let list_riwayat_hukuman of lists_riwayat_hukuman">\n        <ion-item>\n          <ion-avatar item-start>\n            <img src="{{ list_riwayat_hukuman.foto_url }}">\n          </ion-avatar>\n          <b>Pegawai</b> : {{ list_riwayat_hukuman.pegawai }} <br>\n          <b>{{ list_riwayat_hukuman.username }}</b> <br>\n          <b>Jenis Hukuman</b> : {{ list_riwayat_hukuman.jenis_hukuman }} <br>\n          <b>TMT</b> : {{ list_riwayat_hukuman.tmt }}\n        </ion-item>\n        <ion-item-options side="left">\n        <button ion-button small color="primary" (click)="GoToDetailRiwayatHukuman(list_riwayat_hukuman.id)">\n        <ion-icon name="text"></ion-icon>Detail</button>\n        </ion-item-options>\n      </ion-item-sliding>\n</ion-list>\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-hukuman/riwayat-hukuman.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], RiwayatHukumanPage);

//# sourceMappingURL=riwayat-hukuman.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailRiwayatHukumanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the DetailRiwayatHukumanPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var DetailRiwayatHukumanPage = (function () {
    function DetailRiwayatHukumanPage(navCtrl, navParams, dataService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.loadingCtrl = loadingCtrl;
        this.foto = localStorage.getItem('bsw-foto');
        this.getDetailRiwayatHukuman(navParams.get('id'));
    }
    DetailRiwayatHukumanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailRiwayatHukumanPage');
    };
    DetailRiwayatHukumanPage.prototype.getDetailRiwayatHukuman = function (id) {
        var _this = this;
        this.showLoader("Mengambil data dari server...");
        this.dataService.getDetailRiwayatHukuman(id).subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_detail_riwayat_jabatan = data;
            _this.lists_detail_riwayat_hukuman = data.results;
        });
    };
    DetailRiwayatHukumanPage.prototype.showLoader = function (msg) {
        this.loading = this.loadingCtrl.create({
            content: msg
        });
        this.loading.present();
    };
    DetailRiwayatHukumanPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    return DetailRiwayatHukumanPage;
}());
DetailRiwayatHukumanPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-detail-riwayat-hukuman',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/detail-riwayat-hukuman/detail-riwayat-hukuman.html"*/'<!--\n  Generated template for the DetailRiwayatHukumanPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header color="primary">\n\n	<ion-navbar color="primary">\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>Riwayat Hukuman</ion-title>\n		<ion-buttons end style="width: 10%">\n			<a (click)="GoToDashboard()"><img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n		</ion-buttons>   \n	</ion-navbar>\n</ion-header>\n\n\n<ion-content no-padding>\n	<ion-card>\n		<ion-grid col-12>\n			<ion-row *ngFor="let list_detail_riwayat_hukuman of lists_detail_riwayat_hukuman">\n				<ion-col col-12 text-left>Detail Riwayat ({{ list_detail_riwayat_hukuman.jenis_hukuman }})</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card>\n\n	<ion-card>\n\n		<ion-grid *ngFor="let list_detail_riwayat_hukuman of lists_detail_riwayat_hukuman">\n			<ion-row>\n				<ion-col col-6>Jenis Hukuman\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_hukuman.jenis_hukuman }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>TMT\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_hukuman.tmt }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Tanggal Akhir\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_hukuman.tanggal_akhir }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Dugaan Pelanggaran\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_hukuman.dugaan_pelanggaran }}\n				</ion-col>\n			</ion-row>\n\n			<ion-row>\n				<ion-col col-6>Keterangan\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_hukuman.keterangan }}\n				</ion-col>\n			</ion-row>\n\n		</ion-grid>\n	</ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/detail-riwayat-hukuman/detail-riwayat-hukuman.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], DetailRiwayatHukumanPage);

//# sourceMappingURL=detail-riwayat-hukuman.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiwayatCutiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_detail_riwayat_cuti_detail_riwayat_cuti__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RiwayatCutiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RiwayatCutiPage = (function () {
    function RiwayatCutiPage(navCtrl, navParams, dataService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.loadingCtrl = loadingCtrl;
        this.foto = localStorage.getItem('bsw-foto');
        this.getDataRiwayatCuti();
    }
    RiwayatCutiPage.prototype.doRefresh = function (refresh) {
        this.getDataRiwayatCuti();
        refresh.complete();
    };
    RiwayatCutiPage.prototype.getDataRiwayatCuti = function () {
        var _this = this;
        this.showLoader();
        this.dataService.getListRiwayatCuti().subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_riwayat_jabatan = data;
            _this.lists_riwayat_cuti = data.results;
        });
    };
    RiwayatCutiPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Mengambil data dari server...'
        });
        this.loading.present();
    };
    RiwayatCutiPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatCutiPage');
    };
    RiwayatCutiPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    RiwayatCutiPage.prototype.GoToDetailRiwayatCuti = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_detail_riwayat_cuti_detail_riwayat_cuti__["a" /* DetailRiwayatCutiPage */], { id: id });
    };
    return RiwayatCutiPage;
}());
RiwayatCutiPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-riwayat-cuti',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-cuti/riwayat-cuti.html"*/'<!--\n  Generated template for the RiwayatCutiPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n  	<button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Riwayat Cuti</ion-title>\n    <ion-buttons end style="width: 10%">\n      <a (click)="GoToDashboard()"><img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n    </ion-buttons>   \n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n<ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingText="Pull to refresh..."\n      refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n<ion-list no-lines>\n    <ion-item-sliding *ngFor="let list_riwayat_cuti of lists_riwayat_cuti">\n        <ion-item>\n          <ion-avatar item-start>\n            <img src="{{ list_riwayat_cuti.foto_url }}">\n          </ion-avatar>\n          <b>Pegawai</b> : {{ list_riwayat_cuti.pegawai }} <br>\n          <b>{{ list_riwayat_cuti.username }}</b> <br>\n          <b>Jenis Cuti</b> : {{ list_riwayat_cuti.jenis_cuti }} <br>\n          <b>TMT</b> : {{ list_riwayat_cuti.tmt }}\n        </ion-item>\n        <ion-item-options side="left">\n        <button ion-button small color="primary" (click)="GoToDetailRiwayatCuti(list_riwayat_cuti.id)">\n        <ion-icon name="text"></ion-icon>Detail</button>\n        </ion-item-options>\n      </ion-item-sliding>\n</ion-list>\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-cuti/riwayat-cuti.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], RiwayatCutiPage);

//# sourceMappingURL=riwayat-cuti.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailRiwayatCutiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the DetailRiwayatCutiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var DetailRiwayatCutiPage = (function () {
    function DetailRiwayatCutiPage(navCtrl, navParams, dataService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.loadingCtrl = loadingCtrl;
        this.foto = localStorage.getItem('bsw-foto');
        this.getDetailRiwayatCuti(navParams.get('id'));
    }
    DetailRiwayatCutiPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailRiwayatCutiPage');
    };
    DetailRiwayatCutiPage.prototype.getDetailRiwayatCuti = function (id) {
        var _this = this;
        this.showLoader("Mengambil data dari server...");
        this.dataService.getDetailRiwayatCuti(id).subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_detail_riwayat_jabatan = data;
            _this.lists_detail_riwayat_cuti = data.results;
        });
    };
    DetailRiwayatCutiPage.prototype.showLoader = function (msg) {
        this.loading = this.loadingCtrl.create({
            content: msg
        });
        this.loading.present();
    };
    DetailRiwayatCutiPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    return DetailRiwayatCutiPage;
}());
DetailRiwayatCutiPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-detail-riwayat-cuti',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/detail-riwayat-cuti/detail-riwayat-cuti.html"*/'<!--\n  Generated template for the DetailRiwayatCutiPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header color="primary">\n\n	<ion-navbar color="primary">\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>Riwayat Cuti</ion-title>\n		<ion-buttons end style="width: 10%">\n			<a (click)="GoToDashboard()">\n			<img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n		</ion-buttons>   \n	</ion-navbar>\n</ion-header>\n\n\n<ion-content no-padding>\n	<ion-card>\n		<ion-grid col-12>\n			<ion-row *ngFor="let list_detail_riwayat_cuti of lists_detail_riwayat_cuti">\n				<ion-col col-12 text-left>Detail Riwayat ({{ list_detail_riwayat_cuti.jenis_cuti }})</ion-col>\n			</ion-row>\n		</ion-grid>\n	</ion-card>\n\n	<ion-card>\n\n		<ion-grid *ngFor="let list_detail_riwayat_cuti of lists_detail_riwayat_cuti">\n			<ion-row>\n				<ion-col col-6>Jenis Cuti\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_cuti.jenis_cuti }}\n				</ion-col>\n			</ion-row>\n			<ion-row>\n				<ion-col col-6>TMT\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_cuti.tmt }}\n				</ion-col>\n			</ion-row>\n			<ion-row>\n				<ion-col col-6>Jumlah Cuti\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_cuti.jumlah_cuti }}\n				</ion-col>\n			</ion-row>\n			<ion-row>\n				<ion-col col-6>Keterangan\n				</ion-col>\n				<ion-col col-6 text-left>{{ list_detail_riwayat_cuti.keterangan }}\n				</ion-col>\n			</ion-row>\n\n		</ion-grid>\n	</ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/detail-riwayat-cuti/detail-riwayat-cuti.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], DetailRiwayatCutiPage);

//# sourceMappingURL=detail-riwayat-cuti.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataKeluargaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the DataKeluargaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var DataKeluargaPage = (function () {
    function DataKeluargaPage(navCtrl, navParams, dataService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.loadingCtrl = loadingCtrl;
        this.foto = localStorage.getItem('bsw-foto');
        this.getDataKeluarga();
    }
    DataKeluargaPage.prototype.doRefresh = function (refresh) {
        this.getDataKeluarga();
        refresh.complete();
    };
    DataKeluargaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DataKeluargaPage');
    };
    DataKeluargaPage.prototype.getDataKeluarga = function () {
        var _this = this;
        this.showLoader();
        this.dataService.getListDataPerkawinan().subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_riwayat_jabatan = data;
            _this.lists_data_keluarga = data.results;
            _this.lists_data_anak = data.results[0].data_perkawinan;
        });
    };
    DataKeluargaPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Mengambil data dari server...'
        });
        this.loading.present();
    };
    DataKeluargaPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    return DataKeluargaPage;
}());
DataKeluargaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-data-keluarga',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/data-keluarga/data-keluarga.html"*/'t<!--\n  Generated template for the DataKeluargaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n  	<button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Data Keluarga</ion-title>\n    <ion-buttons end style="width: 10%">\n      <a (click)="GoToDashboard()"><img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n    </ion-buttons>   \n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n<ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingText="Pull to refresh..."\n      refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-card *ngFor="let list_data_keluarga of lists_data_keluarga">\n    <ion-card-header text-center>\n      <b>Nama Pegawai</b> : {{ list_data_keluarga.pegawai }} <br>\n      <b>Nama Pasangan</b> : {{ list_data_keluarga.get_pasangan_pegawai }}\n      <hr>\n    </ion-card-header>\n    <ion-card-content>\n    <div *ngIf=" list_data_keluarga.status_tunjangan == true">\n      <b>Status Tunjangan</b> : Ya <br>\n    </div>\n    <div *ngIf=" list_data_keluarga.status_tunjangan == false">\n      <b>Status Tunjangan</b> : Tidak <br>\n    </div>\n      <b>Jenis Perceraian</b> : {{ list_data_keluarga.jenis_perceraian }} <br>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n  <ion-card-header text-center>\n    <b>Data Anak</b>\n    <hr>\n  </ion-card-header>\n  <ion-card *ngFor="let list_data_anak of lists_data_anak">\n  <ion-card-content>\n    <b>Nama Anak </b> : {{ list_data_anak.nama_anak }} <br>\n    <b>Jenis Anak</b> : {{ list_data_anak.jenis_anak }} <br>\n    <b>Tempat Tanggal Lahir</b> : {{ list_data_anak.tempat_lahir }}, {{ list_data_anak.tanggal_lahir }} <br>\n    <b>Jenis Kelamin</b> : {{ list_data_anak.jenis_kelamin }} <br>\n    <b>Agama</b> : {{ list_data_anak.agama }} <br>\n    <div *ngIf=" list_data_anak.status_tunjangan == true">\n      <b>Status Tunjangan</b> : Ya <br>\n    </div>\n    <div *ngIf=" list_data_anak.status_tunjangan == false">\n      <b>Status Tunjangan</b> : Tidak <br>\n    </div>\n  </ion-card-content>\n  </ion-card>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/data-keluarga/data-keluarga.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], DataKeluargaPage);

//# sourceMappingURL=data-keluarga.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(239);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_chooser__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_path__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_base64__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_android_permissions__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_signin_signin__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_riwayat_jabatan_riwayat_jabatan__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_riwayat_kenaikan_pangkat_riwayat_kenaikan_pangkat__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_riwayat_unit_kerja_riwayat_unit_kerja__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_riwayat_pendidikan_riwayat_pendidikan__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_riwayat_hukuman_riwayat_hukuman__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_riwayat_cuti_riwayat_cuti__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_data_keluarga_data_keluarga__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_riwayat_unit_kerja_non_pegawai_riwayat_unit_kerja_non_pegawai__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_detail_riwayat_jabatan_pegawai_detail_riwayat_jabatan_pegawai__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_detail_riwayat_kenaikan_pangkat_detail_riwayat_kenaikan_pangkat__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_detail_riwayat_unit_kerja_detail_riwayat_unit_kerja__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_detail_riwayat_pendidikan_detail_riwayat_pendidikan__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_detail_riwayat_hukuman_detail_riwayat_hukuman__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_detail_riwayat_cuti_detail_riwayat_cuti__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_riwayat_unit_kerja_add_riwayat_unit_kerja_add__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_riwayat_jabatan_add_riwayat_jabatan_add__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_riwayat_unit_kerja_modal_cari_pegawai_riwayat_unit_kerja_modal_cari_pegawai__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_riwayat_unit_kerja_modal_cari_unit_kerja_riwayat_unit_kerja_modal_cari_unit_kerja__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_status_bar__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ionic_native_splash_screen__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__providers_auth_service_auth_service__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_storage__ = __webpack_require__(290);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



 // import untuk ambil data json






//===================











//===================






//===================


//===================


//===================





var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_11__pages_dashboard_dashboard__["a" /* DashboardPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_signin_signin__["a" /* SigninPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_riwayat_jabatan_riwayat_jabatan__["a" /* RiwayatJabatanPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_riwayat_kenaikan_pangkat_riwayat_kenaikan_pangkat__["a" /* RiwayatKenaikanPangkatPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_riwayat_unit_kerja_riwayat_unit_kerja__["a" /* RiwayatUnitKerjaPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_riwayat_pendidikan_riwayat_pendidikan__["a" /* RiwayatPendidikanPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_riwayat_hukuman_riwayat_hukuman__["a" /* RiwayatHukumanPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_riwayat_cuti_riwayat_cuti__["a" /* RiwayatCutiPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_data_keluarga_data_keluarga__["a" /* DataKeluargaPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_detail_riwayat_jabatan_pegawai_detail_riwayat_jabatan_pegawai__["a" /* DetailRiwayatJabatanPegawaiPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_detail_riwayat_kenaikan_pangkat_detail_riwayat_kenaikan_pangkat__["a" /* DetailRiwayatKenaikanPangkatPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_detail_riwayat_unit_kerja_detail_riwayat_unit_kerja__["a" /* DetailRiwayatUnitKerjaPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_detail_riwayat_pendidikan_detail_riwayat_pendidikan__["a" /* DetailRiwayatPendidikanPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_detail_riwayat_hukuman_detail_riwayat_hukuman__["a" /* DetailRiwayatHukumanPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_detail_riwayat_cuti_detail_riwayat_cuti__["a" /* DetailRiwayatCutiPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_riwayat_unit_kerja_add_riwayat_unit_kerja_add__["a" /* RiwayatUnitKerjaAddPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_riwayat_unit_kerja_modal_cari_pegawai_riwayat_unit_kerja_modal_cari_pegawai__["a" /* RiwayatUnitKerjaModalCariPegawaiPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_riwayat_unit_kerja_modal_cari_unit_kerja_riwayat_unit_kerja_modal_cari_unit_kerja__["a" /* RiwayatUnitKerjaModalCariUnitKerjaPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_riwayat_unit_kerja_non_pegawai_riwayat_unit_kerja_non_pegawai__["a" /* RiwayatUnitKerjaNonPegawaiPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_riwayat_jabatan_add_riwayat_jabatan_add__["a" /* RiwayatJabatanAddPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */]),
            __WEBPACK_IMPORTED_MODULE_34__ionic_storage__["a" /* IonicStorageModule */].forRoot()
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_11__pages_dashboard_dashboard__["a" /* DashboardPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_signin_signin__["a" /* SigninPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_riwayat_jabatan_riwayat_jabatan__["a" /* RiwayatJabatanPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_riwayat_kenaikan_pangkat_riwayat_kenaikan_pangkat__["a" /* RiwayatKenaikanPangkatPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_riwayat_unit_kerja_riwayat_unit_kerja__["a" /* RiwayatUnitKerjaPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_riwayat_pendidikan_riwayat_pendidikan__["a" /* RiwayatPendidikanPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_riwayat_hukuman_riwayat_hukuman__["a" /* RiwayatHukumanPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_riwayat_cuti_riwayat_cuti__["a" /* RiwayatCutiPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_data_keluarga_data_keluarga__["a" /* DataKeluargaPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_detail_riwayat_jabatan_pegawai_detail_riwayat_jabatan_pegawai__["a" /* DetailRiwayatJabatanPegawaiPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_detail_riwayat_kenaikan_pangkat_detail_riwayat_kenaikan_pangkat__["a" /* DetailRiwayatKenaikanPangkatPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_detail_riwayat_unit_kerja_detail_riwayat_unit_kerja__["a" /* DetailRiwayatUnitKerjaPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_detail_riwayat_pendidikan_detail_riwayat_pendidikan__["a" /* DetailRiwayatPendidikanPage */],
            __WEBPACK_IMPORTED_MODULE_25__pages_detail_riwayat_hukuman_detail_riwayat_hukuman__["a" /* DetailRiwayatHukumanPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_detail_riwayat_cuti_detail_riwayat_cuti__["a" /* DetailRiwayatCutiPage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_riwayat_unit_kerja_add_riwayat_unit_kerja_add__["a" /* RiwayatUnitKerjaAddPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_riwayat_unit_kerja_modal_cari_pegawai_riwayat_unit_kerja_modal_cari_pegawai__["a" /* RiwayatUnitKerjaModalCariPegawaiPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_riwayat_unit_kerja_modal_cari_unit_kerja_riwayat_unit_kerja_modal_cari_unit_kerja__["a" /* RiwayatUnitKerjaModalCariUnitKerjaPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_riwayat_unit_kerja_non_pegawai_riwayat_unit_kerja_non_pegawai__["a" /* RiwayatUnitKerjaNonPegawaiPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_riwayat_jabatan_add_riwayat_jabatan_add__["a" /* RiwayatJabatanAddPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_31__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_32__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_chooser__["a" /* FileChooser */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_base64__["a" /* Base64 */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_33__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_35__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_signin_signin__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_riwayat_jabatan_riwayat_jabatan__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_riwayat_kenaikan_pangkat_riwayat_kenaikan_pangkat__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_riwayat_unit_kerja_riwayat_unit_kerja__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_riwayat_pendidikan_riwayat_pendidikan__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_riwayat_hukuman_riwayat_hukuman__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_riwayat_cuti_riwayat_cuti__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_data_keluarga_data_keluarga__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_auth_service_auth_service__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, authService, dataService) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.authService = authService;
        this.dataService = dataService;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_signin_signin__["a" /* SigninPage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pagesLain_lain = [
            { title: 'Riwayat Cuti', component: __WEBPACK_IMPORTED_MODULE_10__pages_riwayat_cuti_riwayat_cuti__["a" /* RiwayatCutiPage */], icon: 'heart' },
            { title: 'Riwayat Hukuman', component: __WEBPACK_IMPORTED_MODULE_9__pages_riwayat_hukuman_riwayat_hukuman__["a" /* RiwayatHukumanPage */], icon: 'sad' },
            { title: 'Data Keluarga', component: __WEBPACK_IMPORTED_MODULE_11__pages_data_keluarga_data_keluarga__["a" /* DataKeluargaPage */], icon: 'people' },
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.statusBar.overlaysWebView(true);
            _this.statusBar.backgroundColorByHexString('#388e3c');
            _this.getCountRiwayat();
            _this.groups = localStorage.getItem('bsw-groups');
        });
    };
    MyApp.prototype.getCountRiwayat = function () {
        var _this = this;
        var badge_jabatan, badge_kenaikan_pangkat, badge_unit_kerja, badge_pendidikan;
        if (localStorage.getItem('bsw-groups') == 'Operator' || localStorage.getItem('bsw-groups') == 'Verifikator') {
            this.dataService.getCountRiwayat().subscribe(function (data) {
                //this.lists_riwayat_jabatan = data;
                if (data.results[0].get_count_riwayat_jabatan_pegawai != 0) {
                    badge_jabatan = data.results[0].get_count_riwayat_jabatan_pegawai;
                }
                else if (data.results[0].get_count_riwayat_golongan_pegawai != 0) {
                    badge_kenaikan_pangkat = data.results[0].get_count_riwayat_golongan_pegawai;
                }
                else if (data.results[0].get_count_riwayat_unitkerja_pegawai != 0) {
                    badge_unit_kerja = data.results[0].get_count_riwayat_unitkerja_pegawai;
                }
                else if (data.results[0].get_count_riwayat_pendidikan_pegawai != 0) {
                    badge_pendidikan = data.results[0].get_count_riwayat_pendidikan_pegawai;
                }
                _this.pages = [
                    { title: 'Riwayat Jabatan', component: __WEBPACK_IMPORTED_MODULE_5__pages_riwayat_jabatan_riwayat_jabatan__["a" /* RiwayatJabatanPage */], icon: 'briefcase', badge: badge_jabatan },
                    { title: 'Riwayat Kenaikan Pangkat', component: __WEBPACK_IMPORTED_MODULE_6__pages_riwayat_kenaikan_pangkat_riwayat_kenaikan_pangkat__["a" /* RiwayatKenaikanPangkatPage */], icon: 'medal', badge: badge_kenaikan_pangkat },
                    { title: 'Riwayat Unit Kerja', component: __WEBPACK_IMPORTED_MODULE_7__pages_riwayat_unit_kerja_riwayat_unit_kerja__["a" /* RiwayatUnitKerjaPage */], icon: 'ribbon', badge: badge_unit_kerja },
                    { title: 'Riwayat Pendidikan', component: __WEBPACK_IMPORTED_MODULE_8__pages_riwayat_pendidikan_riwayat_pendidikan__["a" /* RiwayatPendidikanPage */], icon: 'school', badge: badge_pendidikan },
                ];
                _this.dataHeader = [
                    {
                        foto: localStorage.getItem('bsw-foto'),
                        nama_lengkap: localStorage.getItem('bsw-nama-lengkap'),
                        token: localStorage.getItem('bsw-token'),
                        riwayat_golongan: localStorage.getItem('bsw-riwayat-golongan'),
                        riwayat_unit_kerja: localStorage.getItem('bsw-riwayat-unit-kerja'),
                        riwayat_jabatan: localStorage.getItem('bsw-riwayat-jabatan')
                    }
                ];
            });
        }
        else {
            this.dataHeader = [
                {
                    foto: localStorage.getItem('bsw-foto'),
                    nama_lengkap: localStorage.getItem('bsw-nama-lengkap'),
                    token: localStorage.getItem('bsw-token'),
                    riwayat_golongan: localStorage.getItem('bsw-riwayat-golongan'),
                    riwayat_unit_kerja: localStorage.getItem('bsw-riwayat-unit-kerja'),
                    riwayat_jabatan: localStorage.getItem('bsw-riwayat-jabatan')
                }
            ];
            this.pages = [
                { title: 'Riwayat Jabatan', component: __WEBPACK_IMPORTED_MODULE_5__pages_riwayat_jabatan_riwayat_jabatan__["a" /* RiwayatJabatanPage */], icon: 'briefcase', badge: '' },
                { title: 'Riwayat Kenaikan Pangkat', component: __WEBPACK_IMPORTED_MODULE_6__pages_riwayat_kenaikan_pangkat_riwayat_kenaikan_pangkat__["a" /* RiwayatKenaikanPangkatPage */], icon: 'medal', badge: '' },
                { title: 'Riwayat Unit Kerja', component: __WEBPACK_IMPORTED_MODULE_7__pages_riwayat_unit_kerja_riwayat_unit_kerja__["a" /* RiwayatUnitKerjaPage */], icon: 'ribbon', badge: '' },
                { title: 'Riwayat Pendidikan', component: __WEBPACK_IMPORTED_MODULE_8__pages_riwayat_pendidikan_riwayat_pendidikan__["a" /* RiwayatPendidikanPage */], icon: 'school', badge: '' },
            ];
        }
        console.log("Berhasil load menu...");
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
        //this.getCountRiwayat();
    };
    MyApp.prototype.refreshBadge = function () {
        //console.log("Hello")
        this.getCountRiwayat();
    };
    MyApp.prototype.doLogout = function () {
        this.authService.logout().then(function (result) {
        }, function (err) {
        });
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_signin_signin__["a" /* SigninPage */]);
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/app/app.html"*/'<ion-menu [content]="content" id="myMenu">\n\n  <ion-header>\n    <ion-toolbar no-padding color="primary">\n    <ion-card *ngFor="let dataHeaders of dataHeader">\n      <ion-grid>\n      <ion-row>\n        <ion-col  no-padding col-12 text-center style="font-size: 18px;"> {{ dataHeaders.nama_lengkap }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col  no-padding col-12 text-center>{{ dataHeaders.riwayat_jabatan }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col  no-padding col-12 text-center>{{ dataHeaders.riwayat_golongan }}\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col  no-padding col-12 text-center>{{ dataHeaders.riwayat_unit_kerja }}\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n  </ion-card>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n\n    <ion-list>\n      <ion-item-group>\n        <ion-item-divider color="light" text-center>Main Menu</ion-item-divider>        \n          <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n            <ion-icon name="{{ p.icon }}" item-start color="primary"></ion-icon>\n            <p style="font-size: 15px;">{{p.title}}</p>\n            <ion-badge *ngIf=" groups == \'Operator\' " color="primary" item-end>{{p.badge}}</ion-badge>\n            <ion-badge *ngIf=" groups == \'Verifikator\' " color="danger" item-end>{{p.badge}}</ion-badge>\n          </button>          \n      </ion-item-group>\n    </ion-list>\n\n    <ion-list>\n      <ion-item-group>\n        <ion-item-divider color="light" text-center>Lain-lain</ion-item-divider>\n      <button menuClose ion-item *ngFor="let p of pagesLain_lain" (click)="openPage(p)" >\n        <ion-icon name="{{ p.icon }}" item-start color="primary"></ion-icon>\n      <p style="font-size: 15px;" color="primary">{{p.title}}</p>\n      </button>\n      <button menuClose ion-item (click)="doLogout()">\n        <ion-icon name="log-out" item-start color="danger"></ion-icon>\n      <p style="font-size: 15px;" color="danger">Logout</p>\n      </button>\n      </ion-item-group>\n    </ion-list>\n\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false" (click)="refreshBadge()" (swipe)="refreshBadge()"></ion-nav>'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_13__providers_auth_service_auth_service__["a" /* AuthServiceProvider */], __WEBPACK_IMPORTED_MODULE_12__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiwayatUnitKerjaNonPegawaiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the RiwayatUnitKerjaNonPegawaiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RiwayatUnitKerjaNonPegawaiPage = (function () {
    function RiwayatUnitKerjaNonPegawaiPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RiwayatUnitKerjaNonPegawaiPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatUnitKerjaNonPegawaiPage');
    };
    RiwayatUnitKerjaNonPegawaiPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    return RiwayatUnitKerjaNonPegawaiPage;
}());
RiwayatUnitKerjaNonPegawaiPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-riwayat-unit-kerja-non-pegawai',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-unit-kerja-non-pegawai/riwayat-unit-kerja-non-pegawai.html"*/'<!--\n  Generated template for the RiwayatUnitKerjaNonPegawaiPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n  	<button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Riwayat Unit Kerja</ion-title>\n    <ion-buttons end style="width: 10%">\n      <a (click)="GoToDashboard()">\n      <img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n    </ion-buttons>   \n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<ion-searchbar name="search" placeholder="Cari Pegawai..."></ion-searchbar>\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-unit-kerja-non-pegawai/riwayat-unit-kerja-non-pegawai.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], RiwayatUnitKerjaNonPegawaiPage);

//# sourceMappingURL=riwayat-unit-kerja-non-pegawai.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiwayatUnitKerjaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_detail_riwayat_unit_kerja_detail_riwayat_unit_kerja__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_riwayat_unit_kerja_add_riwayat_unit_kerja_add__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_data_service_riwayat_data_service_riwayat__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the RiwayatUnitKerjaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RiwayatUnitKerjaPage = (function () {
    function RiwayatUnitKerjaPage(navCtrl, navParams, dataService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.loadingCtrl = loadingCtrl;
        this.form_operator = { operator_cari_pegawai: '' };
        this.form_verifikator = { verifikator_cari_pegawai: '' };
        this.foto = localStorage.getItem('bsw-foto');
        this.getDataRiwayatUnitKerja();
        this.groups = localStorage.getItem('bsw-groups');
    }
    RiwayatUnitKerjaPage.prototype.doRefresh = function (refresh) {
        this.getDataRiwayatUnitKerja();
        refresh.complete();
    };
    RiwayatUnitKerjaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RiwayatUnitKerjaPage');
    };
    RiwayatUnitKerjaPage.prototype.getDataRiwayatUnitKerja = function () {
        var _this = this;
        this.showLoader();
        this.dataService.getListRiwayatUnitKerja().subscribe(function (data) {
            _this.loading.dismiss();
            //this.lists_riwayat_unit_kerja = data;
            _this.lists_riwayat_unit_kerja = data.results;
        });
    };
    RiwayatUnitKerjaPage.prototype.operator_getDataRiwayatUnitKerja = function () {
        var _this = this;
        var param;
        if (this.form_operator.operator_cari_pegawai == "" || this.form_operator.operator_cari_pegawai == null) {
            this.getDataRiwayatUnitKerja();
        }
        else {
            param = this.form_operator.operator_cari_pegawai;
            this.showLoader();
            this.dataService.non_pegawai_getDataRiwayatUnitKerja(param).subscribe(function (data) {
                _this.loading.dismiss();
                //this.lists_riwayat_unit_kerja = data;
                _this.lists_riwayat_unit_kerja = data.results;
                console.log(data.results);
            });
        }
    };
    RiwayatUnitKerjaPage.prototype.verifikator_getDataRiwayatUnitKerja = function () {
        var _this = this;
        var param;
        if (this.form_verifikator.verifikator_cari_pegawai == "" || this.form_verifikator.verifikator_cari_pegawai == null) {
            this.getDataRiwayatUnitKerja();
        }
        else {
            param = this.form_verifikator.verifikator_cari_pegawai;
            this.showLoader();
            this.dataService.non_pegawai_getDataRiwayatUnitKerja(param).subscribe(function (data) {
                _this.loading.dismiss();
                //this.lists_riwayat_unit_kerja = data;
                _this.lists_riwayat_unit_kerja = data.results;
                console.log(data.results);
            });
        }
    };
    RiwayatUnitKerjaPage.prototype.GoToDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    RiwayatUnitKerjaPage.prototype.GoToDetailRiwayatUnitKerja = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_detail_riwayat_unit_kerja_detail_riwayat_unit_kerja__["a" /* DetailRiwayatUnitKerjaPage */], { id: id });
    };
    RiwayatUnitKerjaPage.prototype.GoToAddRiwayatUnitKerja = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_riwayat_unit_kerja_add_riwayat_unit_kerja_add__["a" /* RiwayatUnitKerjaAddPage */]);
    };
    RiwayatUnitKerjaPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Mengambil data dari server...'
        });
        this.loading.present();
    };
    return RiwayatUnitKerjaPage;
}());
RiwayatUnitKerjaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-riwayat-unit-kerja',template:/*ion-inline-start:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-unit-kerja/riwayat-unit-kerja.html"*/'<!--\n  Generated template for the RiwayatUnitKerjaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n  	<button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Riwayat Unit Kerja</ion-title>\n    <ion-buttons end style="width: 10%">\n      <a (click)="GoToDashboard()">\n      <img style="border-radius: 50%" src="{{ foto}}" width="100%" height="100%"></a>\n    </ion-buttons>   \n  </ion-navbar>\n  \n</ion-header>\n\n\n<ion-content>\n<ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingText="Pull to refresh..."\n      refreshingText="Refreshing...">\n    </ion-refresher-content>\n  </ion-refresher>\n<ion-fab right bottom  *ngIf=" groups == \'Operator\' ">\n    <button ion-fab color="primary" (click)="GoToAddRiwayatUnitKerja()"><ion-icon name="add"></ion-icon></button>\n</ion-fab>\n\n<div *ngIf=" groups == \'Operator\' ">\n  <form (submit)="operator_getDataRiwayatUnitKerja()">\n    <ion-searchbar placeholder="Cari Pegawai...." [(ngModel)]="form_operator.operator_cari_pegawai" name="operator_cari_pegawai"></ion-searchbar>\n  </form>\n</div>\n<div *ngIf=" groups == \'Verifikator\' ">\n  <form (submit)="verifikator_getDataRiwayatUnitKerja()">\n    <ion-searchbar placeholder="Cari Pegawai...." [(ngModel)]="form_verifikator.verifikator_cari_pegawai" name="verifikator_cari_pegawai"></ion-searchbar>\n  </form>\n</div>\n\n<ion-list no-lines>\n<ion-item-sliding *ngFor="let list_riwayat_unit_kerja of lists_riwayat_unit_kerja">\n    <ion-item style="color: #f53d3d" *ngIf=" list_riwayat_unit_kerja.status == 6">\n      <ion-avatar item-start>\n        <img src="{{ list_riwayat_unit_kerja.foto_url }}">\n      </ion-avatar>\n    <b>Pegawai</b> : {{ list_riwayat_unit_kerja.pegawai }} <br>\n    <b>{{ list_riwayat_unit_kerja.username }}</b> <br>\n    <b>Nama Unit Kerja</b> : {{ list_riwayat_unit_kerja.unit_kerja }} <br>\n    <b>Tanggal Mulai</b> : {{ list_riwayat_unit_kerja.tmt }}\n    </ion-item>\n    <ion-item-options side="right">\n    <button ion-button small color="danger" (click)="GoToDetailRiwayatUnitKerja(list_riwayat_unit_kerja.id)">\n    <ion-icon name="text"></ion-icon>Detail</button>\n    </ion-item-options>\n  </ion-item-sliding>\n\n  <ion-item-sliding *ngFor="let list_riwayat_unit_kerja of lists_riwayat_unit_kerja">\n    <ion-item  style="color: #4caf50;" *ngIf=" list_riwayat_unit_kerja.status == 2">\n      <ion-avatar item-start>\n        <img src="{{ list_riwayat_unit_kerja.foto_url }}">\n      </ion-avatar>\n    <b>Pegawai</b> : {{ list_riwayat_unit_kerja.pegawai }} <br>\n    <b>{{ list_riwayat_unit_kerja.username }}</b> <br>\n    <b>Nama Unit Kerja</b> : {{ list_riwayat_unit_kerja.unit_kerja }} <br>\n    <b>Tanggal Mulai</b> : {{ list_riwayat_unit_kerja.tmt }}\n    </ion-item>\n    <ion-item-options side="right">\n    <button ion-button small color="primary" (click)="GoToDetailRiwayatUnitKerja(list_riwayat_unit_kerja.id)">\n    <ion-icon name="text"></ion-icon>Detail</button>\n    </ion-item-options>\n  </ion-item-sliding>\n\n  <ion-item-sliding *ngFor="let list_riwayat_unit_kerja of lists_riwayat_unit_kerja" no-lines>\n    <ion-item style="color:black;" *ngIf=" list_riwayat_unit_kerja.status == 1">\n      <ion-avatar item-start>\n        <img src="{{ list_riwayat_unit_kerja.foto_url }}">\n      </ion-avatar>\n    <b>Pegawai</b> : {{ list_riwayat_unit_kerja.pegawai }} <br>\n    <b>{{ list_riwayat_unit_kerja.username }}</b> <br>\n    <b>Nama Unit Kerja</b> : {{ list_riwayat_unit_kerja.unit_kerja }} <br>\n    <b>Tanggal Mulai</b> : {{ list_riwayat_unit_kerja.tmt }}\n    </ion-item>\n    <ion-item-options side="right">\n    <button ion-button small color="secondary" (click)="GoToDetailRiwayatUnitKerja(list_riwayat_unit_kerja.id)">\n        <ion-icon name="text"></ion-icon>Detail</button>\n    </ion-item-options>\n  </ion-item-sliding>\n</ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/media/foolish/Foolish/Development/ADISATYA/Project/bsw/Sources/Mockup-BSW/src/pages/riwayat-unit-kerja/riwayat-unit-kerja.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__providers_data_service_riwayat_data_service_riwayat__["a" /* DataServiceRiwayatProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
], RiwayatUnitKerjaPage);

//# sourceMappingURL=riwayat-unit-kerja.js.map

/***/ }),

/***/ 9:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataServiceRiwayatProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
Generated class for the DataServiceRiwayatProvider provider.

See https://angular.io/docs/ts/latest/guide/dependency-injection.html
for more info on providers and Angular DI.
    */
//let base_url = "http://192.168.200.88:8899/api-mobile/";
var DataServiceRiwayatProvider = (function () {
    function DataServiceRiwayatProvider(http) {
        this.http = http;
        this.base_url = "http://simita.aitc.co.id/api-mobile/";
        this.site_url = "http://simita.aitc.co.id";
        console.log('Hello DataServiceRiwayatProvider Provider');
    }
    //==================================================//
    //====================== GET DATA =====================//
    //==================================================//
    DataServiceRiwayatProvider.prototype.getListPegawaiDetail = function () {
        console.log(this.http.get(this.base_url + 'pegawai-detail/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'pegawai-detail/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); });
    };
    //============
    DataServiceRiwayatProvider.prototype.getListRiwayatJabatan = function () {
        console.log(this.http.get(this.base_url + 'riwayat-jabatan-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'riwayat-jabatan-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); });
    };
    DataServiceRiwayatProvider.prototype.getDetailRiwayatJabatan = function (id) {
        console.log(this.http.get(this.base_url + 'detail-riwayat-jabatan/' + id).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'detail-riwayat-jabatan/' + id).map(function (res) { return res.json(); });
    };
    DataServiceRiwayatProvider.prototype.non_pegawai_getDetailRiwayatJabatan = function (param) {
        console.log(this.http.get(this.base_url + 'riwayat-jabatan-list/' + localStorage.getItem('bsw-token') + '/?q=' + param).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'riwayat-jabatan-list/' + localStorage.getItem('bsw-token') + '/?q=' + param).map(function (res) { return res.json(); });
    };
    //======================================================================//
    DataServiceRiwayatProvider.prototype.getListRiwayatUnitKerja = function () {
        console.log(this.http.get(this.base_url + 'riwayat-unit-kerja-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'riwayat-unit-kerja-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); });
    };
    DataServiceRiwayatProvider.prototype.getDetailRiwayatUnitKerja = function (id) {
        console.log(this.http.get(this.base_url + 'detail-riwayat-unit-kerja/' + id).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'detail-riwayat-unit-kerja/' + id).map(function (res) { return res.json(); });
    };
    DataServiceRiwayatProvider.prototype.non_pegawai_getDataRiwayatUnitKerja = function (param) {
        console.log(this.http.get(this.base_url + 'riwayat-unit-kerja-list/' + localStorage.getItem('bsw-token') + '/?q=' + param).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'riwayat-unit-kerja-list/' + localStorage.getItem('bsw-token') + '/?q=' + param).map(function (res) { return res.json(); });
    };
    //======================================================================//
    DataServiceRiwayatProvider.prototype.getListRiwayatKenaikanPangkat = function () {
        console.log(this.http.get(this.base_url + 'riwayat-golongan-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'riwayat-golongan-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); });
    };
    DataServiceRiwayatProvider.prototype.getDetailRiwayatKenaikanPangkat = function (id) {
        console.log(this.http.get(this.base_url + 'detail-riwayat-golongan/' + id).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'detail-riwayat-golongan/' + id).map(function (res) { return res.json(); });
    };
    DataServiceRiwayatProvider.prototype.non_pegawai_getDetailRiwayatKenaikanPangkat = function (param) {
        console.log(this.http.get(this.base_url + 'riwayat-golongan-list/' + localStorage.getItem('bsw-token') + '/?q=' + param).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'riwayat-golongan-list/' + localStorage.getItem('bsw-token') + '/?q=' + param).map(function (res) { return res.json(); });
    };
    //============
    DataServiceRiwayatProvider.prototype.getListRiwayatPendidikan = function () {
        console.log(this.http.get(this.base_url + 'riwayat-pendidikan-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'riwayat-pendidikan-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); });
    };
    DataServiceRiwayatProvider.prototype.getDetailRiwayatPendidikan = function (id) {
        console.log(this.http.get(this.base_url + 'detail-riwayat-pendidikan/' + id).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'detail-riwayat-pendidikan/' + id).map(function (res) { return res.json(); });
    };
    DataServiceRiwayatProvider.prototype.non_pegawai_getDetailRiwayatRiwayatPendidikan = function (param) {
        console.log(this.http.get(this.base_url + 'riwayat-pendidikan-list/' + localStorage.getItem('bsw-token') + '/?q=' + param).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'riwayat-pendidikan-list/' + localStorage.getItem('bsw-token') + '/?q=' + param).map(function (res) { return res.json(); });
    };
    //============
    DataServiceRiwayatProvider.prototype.getListDataPerkawinan = function () {
        console.log(this.http.get(this.base_url + 'data-perkawinan-detail/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'data-perkawinan-detail/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); });
    };
    //============
    DataServiceRiwayatProvider.prototype.getListRiwayatCuti = function () {
        console.log(this.http.get(this.base_url + 'cuti-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'cuti-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); });
    };
    DataServiceRiwayatProvider.prototype.getDetailRiwayatCuti = function (id) {
        console.log(this.http.get(this.base_url + 'detail-cuti/' + id).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'detail-cuti/' + id).map(function (res) { return res.json(); });
    };
    //============
    DataServiceRiwayatProvider.prototype.getListRiwayatHukuman = function () {
        console.log(this.http.get(this.base_url + 'hukuman-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'hukuman-list/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); });
    };
    DataServiceRiwayatProvider.prototype.getDetailRiwayatHukuman = function (id) {
        console.log(this.http.get(this.base_url + 'detail-hukuman/' + id).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'detail-hukuman/' + id).map(function (res) { return res.json(); });
    };
    //============
    DataServiceRiwayatProvider.prototype.getCountRiwayat = function () {
        console.log(this.http.get(this.base_url + 'count-riwayat/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); }));
        return this.http.get(this.base_url + 'count-riwayat/' + localStorage.getItem('bsw-token')).map(function (res) { return res.json(); });
    };
    //==================================================//
    //====================== GET DATA =====================//
    //==================================================//
    //==================================================//
    //====================== POST DATA =====================//
    //==================================================//
    DataServiceRiwayatProvider.prototype.postRiwayatUnitKerjaBaru = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            _this.http.post(_this.base_url + 'riwayat-unit-kerja/post/' + localStorage.getItem('bsw-token') + '/', JSON.stringify(data), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    DataServiceRiwayatProvider.prototype.postStatusDetailUnitKerja = function (id, data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            _this.http.post(_this.base_url + 'riwayat-unit-kerja/status-post/' + localStorage.getItem('bsw-token') + '/' + id + '/', JSON.stringify(data), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    DataServiceRiwayatProvider.prototype.postStatusDetailJabatan = function (id, data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            _this.http.post(_this.base_url + 'riwayat-jabatan/status-post/' + localStorage.getItem('bsw-token') + '/' + id + '/', JSON.stringify(data), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    DataServiceRiwayatProvider.prototype.postStatusDetailKenaikanPangkat = function (id, data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            _this.http.post(_this.base_url + 'riwayat-golongan/status-post/' + localStorage.getItem('bsw-token') + '/' + id + '/', JSON.stringify(data), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    DataServiceRiwayatProvider.prototype.postStatusDetailPendidikan = function (id, data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Content-Type', 'application/json');
            _this.http.post(_this.base_url + 'riwayat-pendidikan/status-post/' + localStorage.getItem('bsw-token') + '/' + id + '/', JSON.stringify(data), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    return DataServiceRiwayatProvider;
}());
DataServiceRiwayatProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], DataServiceRiwayatProvider);

//# sourceMappingURL=data-service-riwayat.js.map

/***/ })

},[220]);
//# sourceMappingURL=main.js.map